# CLEAN
# NIY

# INSTALL
install-venv:
	virtualenv --python=python3 venv

install-dependencies:
	venv/bin/pip install -r requirements.txt

# RUN
run-migration:
	venv/bin/python src/manage.py migrate

run-autopep8:
	venv/bin/autopep8 -i -r -j 0 src/

run-tests:
	venv/bin/python src/manage.py test tests

run-functional-tests:
	venv/bin/python src/manage.py test functional-tests

run-flake8:
	venv/bin/flake8 src/

run-server:
	venv/bin/python src/manage.py runserver

run-gitlab-ci-simulator: run-flake8 run-tests

run-code-coverage:
	venv/bin/coverage run --source='./src' --omit='./src/functional-tests/*,*/migrations/*' src/manage.py test tests
	venv/bin/coverage html

run-sass:
	venv/bin/python tools/scripts/compile_scss.py

run-seeder:
	venv/bin/python src/manage.py authentication_model_seeder $(number_of_seeds)
	#venv/bin/python src/manage.py prerequisite_model_seeder $(number_of_seeds)
	venv/bin/python src/manage.py course_model_seeder $(number_of_seeds)
	#venv/bin/python src/manage.py course_instructor_participant_seeder $(number_of_seeds)
	venv/bin/python src/manage.py task_model_seeder $(number_of_seeds)

# UPDATE
update-dependencies:
	venv/bin/pip freeze > requirements.txt

.PHONY: clean install-venv install-dependencies run-gitlab-ci-simulator
