Lernplattform Webseite (LMS)
=====================
[![pipeline status](https://gitlab.cs.fau.de/Lernplattform/Lernplattform_Webseite/badges/master/pipeline.svg)](https://gitlab.cs.fau.de/Lernplattform/Lernplattform_Webseite/commits/master)
[![coverage report](https://gitlab.cs.fau.de/Lernplattform/Lernplattform_Webseite/badges/master/coverage.svg)]()

Der Code für die Lernplattform Webseite

Install
-----
Python3 is required. <br />
Use **make all** to install pyhton, virtualenv and the dependencies and run the mirgration, tests and the server.

Usage
-----
Run the tests: **make run-tests** <br />
Run the browser tests: **make run-functional-tests** <br />
Run the django server: **make run-server** <br />
Compile the .scss files **make run-sass**

Licenses
---
| package  | version  | license |   
|---|---|---|
| autopep8 | 1.3.3 | Expat License |
| certifi | 2018.1.18 | MPL-2.0 |
| chardet | 3.0.4 | LGPL |
| chromedriver | 2.24.1 | Apache 2.0 |
| defusedxml | 0.5.0 | PSFL |
| Django | 2.0.1 | BSD |
| django-avatar | 4.1.0 | BSD |
| django-debug-toolbar | 1.9.1 | BSD |
| faker | 0.8.10 | MIT |
| flake8 | 3.5.0 | MIT |
| idna | 2.6 | BSD-like |
| libsass | 0.13.6 | MIT |
| Markdown | 2.6.11 | BSD |
| martor | 1.2.5 | GNUGPL-v3 |
| mccabe | 0.6.1 | Expat license |
| model-mommy | 1.5.1 | Apache license |
| oauthlib | 2.0.6 | BSD |
| olefile | 0.44 | BSD |
| Pillow | 5.0.0 | Standard PIL License |
| pycodestyle | 2.3.1 | Expat license |
| pyflakes | 1.6.0 | MIT |
| PyJWT | 1.5.3 | MIT |
| python3-openid | 3.1.0 | Apache 2.0 |
| pytz | 2017.3 | MIT |
| requests | 2.18.4 | Apache 2.0 |
| requests-oauthlib | 0.8.0 | ISC |
| selenium | 3.8.1 | Apache 2.0  |
| six | 1.11.0 | MIT |
| social-auth-app-django | 2.1.0 | BSD |
| social-auth-core | 1.6.0 | BSD |
| splinter | 0.7.7 | BSD |
| sqlparse | 0.2.4 | BSD |
| urllib3 | 1.22 | MIT |
