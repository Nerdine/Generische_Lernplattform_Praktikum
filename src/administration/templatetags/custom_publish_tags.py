from django import template
from django.utils.translation import ugettext as _

register = template.Library()


@register.simple_tag
def get_action_name(course):
    if course.published:
        return _('Unpublish')
    else:
        return _('Publish')
