from django.urls import path

from .views import course as course_views
from .views import lesson as lesson_views
from .views import tasks as tasks_views
from .views import solution as solution_views

app_name = 'administration'
urlpatterns = [
    # course
    path('course/create',
         course_views.CourseCreateView.as_view(), name='course_create'),
    path('course/<slug:course_slug>/update',
         course_views.CourseUpdateView.as_view(), name='course_update'),
    path('course/<slug:course_slug>/update/structure',
         course_views.CourseUpdateStructureView.as_view(), name='course_update_structure'),
    path('course/<slug:course_slug>/',
         course_views.CourseDetailView.as_view(), name='course_detail'),
    path('courses',
         course_views.CoursesView.as_view(), name='courses'),
    path('course/<slug:course_slug>/publish',
         course_views.CoursePublishUnpublishView.as_view(), name='course_publish'),
    path('<slug:course_slug>/instructor-details/<slug:instructor_slug>',
         course_views.CourseInstructorDetailView.as_view(), name='instructor_detail'),

    # lesson
    path('course/<slug:course_slug>/lesson/create',
         lesson_views.LessonCreateView.as_view(), name='lesson_create'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/update',
         lesson_views.LessonUpdateView.as_view(), name='lesson_update'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/delete',
         lesson_views.LessonDeleteView.as_view(), name='lesson_delete'),

    # all tasks
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/task/create',
         tasks_views.TaskCreateView.as_view(),
         name='task_create'),
    path('course/<slug:course_slug>/task/<slug:task_slug>/delete',
         tasks_views.TaskDeleteView.as_view(),
         name='task_delete'),

    # task text
    path('course/<slug:course_slug>/text/<slug:task_slug>/detail',
         tasks_views.TextTaskAdminDetailView.as_view(),
         name='text_admin_detail'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/text/create',
         tasks_views.TextTaskCreateView.as_view(),
         name='text_create'),
    path('course/<slug:course_slug>/text/<slug:task_slug>/update',
         tasks_views.TextTaskUpdateView.as_view(),
         name='text_update'),

    # task exercise
    path('course/<slug:course_slug>/detail_exercise/<slug:task_slug>',
         tasks_views.ExerciseTaskAdminDetailView.as_view(),
         name='exercise_admin_detail'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/exercise/create',
         tasks_views.ExerciseTaskCreateView.as_view(),
         name='exercise_create'),
    path('course/<slug:course_slug>/exercise/<slug:task_slug>/update',
         tasks_views.ExerciseTaskUpdateView.as_view(),
         name='exercise_update'),

    # task programming exercise
    path('course/<slug:course_slug>/programmingexercise/<slug:task_slug>/detail',
         tasks_views.ProgrammingExerciseTaskAdminDetailView.as_view(),
         name='programmingexercise_admin_detail'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/programmingexercise/create',
         tasks_views.ProgrammingExerciseTaskCreateView.as_view(),
         name='programmingexercise_create'),
    path('course/<slug:course_slug>/programmingexercise/<slug:task_slug>/update',
         tasks_views.ProgrammingExerciseTaskUpdateView.as_view(),
         name='programmingexercise_update'),

    # task quiz
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/detail',
         tasks_views.QuizTaskAdminDetailView.as_view(),
         name='quiz_admin_detail'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/quiz/create',
         tasks_views.QuizTaskCreateView.as_view(),
         name='quiz_create'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/update',
         tasks_views.QuizTaskUpdateView.as_view(),
         name='quiz_update'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/update/structure',
         tasks_views.QuizUpdateStructureView.as_view(),
         name='quiz_update_structure'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/update_content',
         tasks_views.QuizTaskContentUpdateView.as_view(),
         name='quiz_content_update'),

    # quiz question
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/question/create',
         tasks_views.QuestionCreateView.as_view(),
         name='quiz_question_create'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/question/<slug:question_slug>/update',
         tasks_views.QuestionUpdateView.as_view(),
         name='quiz_question_update'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/question/<slug:question_slug>/delete',
         tasks_views.QuestionDeleteView.as_view(),
         name='quiz_question_delete'),

    # quiz answer
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/question/<slug:question_slug>/create',
         tasks_views.AnswerCreateView.as_view(),
         name='quiz_answer_create'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/answer/<slug:answer_slug>/update',
         tasks_views.AnswerUpdateView.as_view(),
         name='quiz_answer_update'),
    path('course/<slug:course_slug>/quiz/<slug:task_slug>/answer/<slug:answer_slug>/delete',
         tasks_views.AnswerDeleteView.as_view(),
         name='quiz_answer_delete'),

    # task video
    path('course/<slug:course_slug>/video/<slug:task_slug>/detail',
         tasks_views.VideoTaskAdminDetailView.as_view(),
         name='video_admin_detail'),
    path('course/<slug:course_slug>/lesson/<slug:lesson_slug>/video/create',
         tasks_views.VideoTaskCreateView.as_view(),
         name='video_create'),
    path('course/<slug:course_slug>/video/<slug:task_slug>/update',
         tasks_views.VideoTaskUpdateView.as_view(),
         name='video_update'),

    # Solution
    path('solution/<slug:course_slug>/',
         solution_views.SolutionDetailView.as_view(), name='solution_detail'),
    path('solution/pass/<slug:course_slug>/',
         solution_views.SolutionPassView.as_view(), name='solution_pass'),
]
