import json

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.generic import DetailView, CreateView, UpdateView, ListView
from django.views.generic.base import View

from authentication.models import User
from course.forms import CourseCreateForm
from course.models import Course, Instructor
from course.views import InstructorRequiredMixin
from prerequisite.forms import CategoryAjaxCreateForm
from task.models import TaskBase


class CourseDetailView(LoginRequiredMixin, InstructorRequiredMixin, DetailView):
    model = Course
    template_name = 'administration/course/detail.html'
    slug_url_kwarg = 'course_slug'
    context_object_name = 'course'


class CoursesView(LoginRequiredMixin, InstructorRequiredMixin, ListView):
    model = Course
    template_name = 'administration/course/courses.html'

    def get_queryset(self):

        instructors = Instructor.objects.filter(user=self.request.user)
        course_pks = []
        for instructor in instructors:
            course_pks.append(instructor.course.pk)

        return Course.objects.filter(pk__in=course_pks)


class CourseCreateView(InstructorRequiredMixin, LoginRequiredMixin, CreateView):
    form_class = CourseCreateForm
    template_name = 'administration/course/create.html'

    def get_context_data(self, **kwargs):
        context = super(CourseCreateView, self).get_context_data()
        context['category_form'] = CategoryAjaxCreateForm()
        return context

    def form_valid(self, form):
        response = super(CourseCreateView, self).form_valid(form)

        categories = form.cleaned_data['categories']
        if categories.count() > 0:
            self.object.categories.set(categories)

        prerequisites = form.cleaned_data['prerequisites']
        if prerequisites.count() > 0:
            self.object.prerequisites.set(prerequisites)

        new_instructors = form.cleaned_data['instructors']
        if new_instructors.count() > 0:
            self.object.instructors.set(new_instructors)

        messages.success(
            self.request,
            _("%(course)s created successfully") % {'course': self.object}
        )
        return response

    def get_success_url(self):
        return reverse('administration:course_detail', kwargs={'course_slug': self.object.slug})


class CourseUpdateView(LoginRequiredMixin, InstructorRequiredMixin, UpdateView):
    model = Course
    form_class = CourseCreateForm
    template_name = 'administration/course/update.html'

    def get_context_data(self, **kwargs):
        context = super(CourseUpdateView, self).get_context_data()
        context['category_form'] = CategoryAjaxCreateForm()
        return context

    def form_valid(self, form):
        response = super(CourseUpdateView, self).form_valid(form)

        new_cats = form.cleaned_data['categories']
        if new_cats.count() > 0:
            self.object.categories.set(new_cats)

        new_prerequisites = form.cleaned_data['prerequisites']
        if new_prerequisites.count() > 0:
            self.object.prerequisites.set(new_prerequisites)

        new_instructors = form.cleaned_data['instructors']
        if new_instructors.count() > 0:
            self.object.instructors.set(new_instructors)

        messages.success(
            self.request,
            _("%(course)s updated successfully") % {'course': self.object}
        )
        return response

    def get_object(self):
        object = get_object_or_404(Course, slug=self.kwargs['course_slug'])
        return object

    def get_success_url(self):
        return reverse('administration:course_detail', kwargs={'course_slug': self.object.slug})


class CourseInstructorDetailView(LoginRequiredMixin, DetailView):
    context_object_name = 'course_list'
    template_name = 'administration/course/instructor_detail.html'
    queryset = Course.objects.all()
    slug_url_kwarg = 'course_slug'

    def get_context_data(self, **kwargs):
        context = super(CourseInstructorDetailView, self).get_context_data(**kwargs)
        user = User.objects.filter(username=self.kwargs['instructor_slug']).first()
        recent_course = Course.objects.filter(slug=self.kwargs['course_slug']).first()

        context['user'] = user
        context['events'] = user.events.all()
        context['specialties'] = user.specialties.order_by('-knowledge_level').all()
        related_instructors = Instructor.objects.filter(user=user.id).all()

        courses = list()
        for related in related_instructors:
            courses.append(Course.objects.filter(id=related.course_id).first())
        context['courses'] = courses
        context['recent_course'] = recent_course
        return context


class CoursePublishUnpublishView(LoginRequiredMixin, InstructorRequiredMixin, View):
    model = Course

    def post(self, request, course_slug):
        course = get_object_or_404(Course, slug=course_slug)

        if course.published:  # unpublish
            course.published = False
            self.__add_message(_('The course %(name)s was successfully unpublished'), course)
        else:  # publish
            course.published = True
            self.__add_message(_('The course %(name)s was successfully published'), course)
        course.save()

        return redirect('administration:courses')

    def __add_message(self, message, course):
        messages.add_message(self.request, messages.SUCCESS, _(
            message % {'name': course.name}))


class CourseUpdateStructureView(LoginRequiredMixin, InstructorRequiredMixin, View):
    def post(self, request, course_slug):
        course = get_object_or_404(Course, slug=course_slug)
        json_as_string = request.POST.get("structure", "")
        if json_as_string.strip() == "":
            return HttpResponse(status=201)
        data = json.loads(json_as_string)

        lessons = data[0]

        lesson_counter = 0
        task_counter = 0

        for lesson in lessons:
            lesson_entity = course.lessons.filter(id=lesson["id"]).first()
            if lesson_entity is None:
                return HttpResponseServerError()
            lesson_entity.position = lesson_counter
            lesson_counter = lesson_counter + 1
            lesson_entity.save()

            for task in lesson["children"][0]:
                task = TaskBase.objects.filter(id=task["id"]).first()
                if task is None:
                    return HttpResponseServerError()
                task.lesson = lesson_entity
                task.position = task_counter
                task_counter = task_counter + 1
                task.save()
            task_counter = 0

        return HttpResponse(_("The update of the course structure was successfully"), status=200)
