from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic import CreateView, UpdateView, DeleteView

from course.forms import LessonCreateForm
from course.models import Lesson, Course
from course.views import InstructorRequiredMixin


class LessonBaseView(InstructorRequiredMixin, LoginRequiredMixin, View):
    slug_url_kwarg = 'lesson_slug'
    model = Lesson

    def get_success_url(self):
        course_slug = self.kwargs['course_slug']
        return reverse('administration:course_detail', kwargs={'course_slug': course_slug})


class LessonCreateView(LessonBaseView, CreateView):
    form_class = LessonCreateForm
    template_name = 'administration/lesson/create.html'

    def form_valid(self, form):
        self.object = form.save(False)
        course = get_object_or_404(Course, slug=self.kwargs['course_slug'])
        self.object.course = course

        response = super(LessonCreateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(lesson)s created successfully") % {'lesson': self.object}
        )
        return response


class LessonUpdateView(LessonBaseView, UpdateView):
    form_class = LessonCreateForm
    template_name = 'administration/lesson/update.html'

    def form_valid(self, form):
        response = super(LessonUpdateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(lesson)s updated successfully") % {'lesson': self.object}
        )
        return response

    def get_object(self):
        object = get_object_or_404(Lesson, slug=self.kwargs['lesson_slug'])
        return object


class LessonDeleteView(LessonBaseView, DeleteView):
    template_name = 'administration/lesson/delete.html'

    def delete(self, request, *args, **kwargs):
        response = super(LessonDeleteView, self).delete(self, request, *args, **kwargs)
        messages.success(
            self.request,
            _("%(lesson)s deleted successfully") % {'lesson': self.object}
        )
        return response

    def get_object(self):
        object = get_object_or_404(Lesson, slug=self.kwargs['lesson_slug'])
        return object
