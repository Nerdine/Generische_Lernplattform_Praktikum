from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView
from django.views import View
from django.shortcuts import redirect, get_object_or_404
from course.views import InstructorRequiredMixin
from course.models import Course
from task.models import TaskBase, UserExerciseSolution
from authentication.models import User


class SolutionDetailView(InstructorRequiredMixin, LoginRequiredMixin, DetailView):
    model = Course
    slug_url_kwarg = 'course_slug'
    template_name = 'administration/solution/detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tasks = TaskBase.objects.filter(
            lesson__course__slug=self.kwargs['course_slug'], derived_type="exercise")
        sols = []
        for task in tasks:
            sols.append((task, UserExerciseSolution.objects.filter(
                exercise=task.exercise, is_corrected=False)))

        context['solution_list'] = sols
        return context


class SolutionPassView(LoginRequiredMixin, View):

    def post(self, request, course_slug):
        user = get_object_or_404(User, username=request.POST.get('user', ""))
        exercise = get_object_or_404(TaskBase, name=request.POST.get('exercise', ""))
        is_correct_answered = False
        if('TRUE' in request.POST):
            is_correct_answered = True
        solution = UserExerciseSolution.objects.get(user=user, exercise=exercise)
        solution.is_correct_answered = is_correct_answered
        solution.is_corrected = True
        solution.save()
        return redirect('administration:solution_detail', course_slug=course_slug)
