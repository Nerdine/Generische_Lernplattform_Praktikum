import json

from django.contrib import messages
from django.http.response import HttpResponseRedirect, HttpResponse, HttpResponseServerError
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from course.models import Lesson
from course.views import LoginRequiredMixin, InstructorRequiredMixin
from task.forms import AnswerForm, QuestionForm, TaskBaseCreateForm, VideoTaskCreateForm, \
    TextTaskCreateForm, QuizTaskCreateForm, ExerciseTaskCreateForm, ExerciseSolutionCreateForm
from task.forms import ProgrammingexerciseTaskCreateForm
from task.models import ProgrammingExercise, TaskBase, Text, Quiz, Exercise, Video
from task.models import Question, Answer


class TaskAdminDetailView(InstructorRequiredMixin, LoginRequiredMixin, DetailView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['course_slug'] = self.kwargs['course_slug']
        context['task_slug'] = self.kwargs['task_slug']
        return context


class TaskCreateView(InstructorRequiredMixin, LoginRequiredMixin, CreateView):
    model = TaskBase
    form_class = TaskBaseCreateForm
    template_name = 'administration/tasks/task_create.html'
    selected_task_type = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        lesson = get_object_or_404(Lesson, slug=self.kwargs['lesson_slug'])
        if lesson is not None:
            context['lesson_name'] = lesson.name

        if self.selected_task_type is not None:
            context['selected_task_url'] = self.selected_task_type

        context['types'] = [
            {'name': '--------', 'url': 'task_create'},
            {'name': 'Exercise', 'url': 'exercise_create'},
            {'name': 'Programming-Exercise', 'url': 'programmingexercise_create'},
            {'name': 'Quiz', 'url': 'quiz_create'},
            {'name': 'Video', 'url': 'video_create'},
            {'name': 'Text', 'url': 'text_create'},
        ]

        return context

    def get_success_url(self):
        course_slug = self.kwargs['course_slug']
        return reverse('administration:course_detail', kwargs={'course_slug': course_slug})

    def form_valid(self, form):
        self.object = form.save(False)
        self.object.lesson = get_object_or_404(Lesson, slug=self.kwargs['lesson_slug'])
        self.object.save()
        response = super(TaskCreateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(task)s created successfully") % {'task': self.object}
        )
        return response

    def post(self, request, **kwargs):
        task_url = request.POST.get('taskTypes')
        if task_url is not None:
            return HttpResponseRedirect(reverse("administration:" + task_url, kwargs=kwargs))

        return super().post(self, request, **kwargs)


class TaskUpdateView(InstructorRequiredMixin, LoginRequiredMixin, UpdateView):
    slug_url_kwarg = 'task_slug'

    def get_success_url(self):
        course_slug = self.kwargs['course_slug']
        return reverse('administration:course_detail', kwargs={'course_slug': course_slug})

    def form_valid(self, form):
        response = super(TaskUpdateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(task)s updated successfully") % {'task': self.object}
        )
        return response


class TaskDeleteView(InstructorRequiredMixin, LoginRequiredMixin, DeleteView):
    slug_url_kwarg = 'task_slug'
    model = TaskBase
    template_name = 'administration/tasks/task_delete.html'

    def get_success_url(self):
        course_slug = self.kwargs['course_slug']
        return reverse('administration:course_detail', kwargs={'course_slug': course_slug})

    def get_object(self):
        object = get_object_or_404(TaskBase, slug=self.kwargs['task_slug'])
        return object

    def delete(self, request, *args, **kwargs):
        response = super(TaskDeleteView, self).delete(self, request, *args, **kwargs)
        messages.success(
            self.request,
            _("%(task)s deleted successfully") % {'task': self.object}
        )
        return response


class TextTaskAdminDetailView(TaskAdminDetailView):
    model = Text
    template_name = 'administration/tasks/text/text_admin_detail.html'

    def get_object(self):
        object = get_object_or_404(Text, slug=self.kwargs['task_slug'])
        return object


class TextTaskCreateView(TaskCreateView):
    form_class = TextTaskCreateForm
    template_name = 'administration/tasks/text/text_create.html'
    selected_task_type = "text_create"


class TextTaskUpdateView(TaskUpdateView):
    model = Text
    form_class = TextTaskCreateForm
    template_name = 'administration/tasks/text/text_update.html'


class QuizTaskAdminDetailView(TaskAdminDetailView):
    model = Quiz
    template_name = 'administration/tasks/quiz/quiz_admin_detail.html'

    def get_object(self):
        object = get_object_or_404(Quiz, slug=self.kwargs['task_slug'])
        return object


class QuizTaskCreateView(TaskCreateView):
    form_class = QuizTaskCreateForm
    template_name = 'administration/tasks/quiz/quiz_create.html'
    selected_task_type = "quiz_create"

    def get_success_url(self):
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.object.slug
        })


class QuizTaskUpdateView(TaskUpdateView):
    model = Quiz
    form_class = QuizTaskCreateForm
    template_name = 'administration/tasks/quiz/quiz_update.html'

    def get_context_data(self, **kwargs):
        context = super(QuizTaskUpdateView, self).get_context_data()
        context['question_form'] = QuestionForm()
        context['course_slug'] = self.kwargs['course_slug']
        context['task_slug'] = self.kwargs['task_slug']
        return context

    def get_success_url(self):
        return reverse('administration:course_detail', kwargs={
            'course_slug': self.kwargs['course_slug'],
        })


class QuizUpdateStructureView(LoginRequiredMixin, InstructorRequiredMixin, View):
    def post(self, request, course_slug, task_slug):
        quiz = get_object_or_404(Quiz, slug=task_slug)
        json_as_string = request.POST.get("structure", "")
        if json_as_string.strip() == "":
            return HttpResponse(status=201)
        data = json.loads(json_as_string)

        questions = data[0]

        question_counter = 0
        answer_counter = 0

        for question in questions:
            question_entity = quiz.get_questions().filter(id=question['id']).first()
            if question_entity is None:
                return HttpResponseServerError()
            question_entity.position = question_counter
            question_counter = question_counter + 1
            question_entity.save()

            for answer in question["children"][0]:
                answer = question_entity.get_answers().filter(id=answer["id"]).first()
                if answer is None:
                    return HttpResponseServerError()
                answer.question = question_entity
                answer.position = answer_counter
                answer_counter = answer_counter + 1
                answer.save()
            answer_counter = 0

        return HttpResponse(_("The update of the quiz structure was successfully"), status=200)


class QuizTaskDeleteView(TaskDeleteView):
    model = Quiz
    template_name = 'administration/tasks/task_delete.html'


class QuizTaskContentUpdateView(TaskAdminDetailView):
    model = Quiz
    template_name = 'administration/tasks/quiz/quiz_content_create_update.html'

    def get_object(self):
        object = get_object_or_404(Quiz, slug=self.kwargs['task_slug'])
        return object


class QuestionCreateView(InstructorRequiredMixin, LoginRequiredMixin, CreateView):
    model = Question
    form_class = QuestionForm
    template_name = 'administration/tasks/quiz/quiz_content_create_update.html'

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data()
        context['task'] = Quiz.objects.get(slug=self.kwargs["task_slug"])
        context['course_slug'] = self.kwargs['course_slug']
        context['task_slug'] = self.kwargs['task_slug']

        context['formHeadline'] = _('Create question')
        context['formButtonLabel'] = _('Create question')
        return context

    def get_success_url(self):
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.kwargs['task_slug']
        })

    def form_valid(self, form):
        self.object = form.save(False)
        self.object.quiz = get_object_or_404(Quiz, slug=self.kwargs['task_slug'])
        self.object.save()
        response = super(QuestionCreateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(object)s created successfully") % {'object': self.object}
        )
        return response


class QuestionUpdateView(InstructorRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Question
    form_class = QuestionForm
    template_name = 'administration/tasks/quiz/quiz_content_create_update.html'

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data()
        context['task'] = Quiz.objects.get(slug=self.kwargs["task_slug"])
        context['course_slug'] = self.kwargs['course_slug']
        context['task_slug'] = self.kwargs['task_slug']
        context['question_slug'] = self.kwargs['question_slug']

        context['formHeadline'] = _('Update question')
        context['formButtonLabel'] = _('Update question')
        return context

    def get_success_url(self):
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.kwargs['task_slug']
        })

    def form_valid(self, form):
        response = super(QuestionUpdateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(object)s updated successfully") % {'object': self.object}
        )
        return response

    def get_object(self):
        object = get_object_or_404(Question, slug=self.kwargs['question_slug'])
        return object


class QuestionDeleteView(InstructorRequiredMixin, LoginRequiredMixin, DeleteView):
    model = Question
    template_name = 'administration/tasks/task_delete.html'

    def get_success_url(self):
        messages.success(self.request, _("%(object)s deleted successfully") %
                         {'object': self.object}
                         )
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.kwargs['task_slug']
        })

    def get_object(self):
        object = get_object_or_404(Question, slug=self.kwargs['question_slug'])
        return object


class AnswerCreateView(InstructorRequiredMixin, LoginRequiredMixin, CreateView):
    model = Answer
    form_class = AnswerForm
    template_name = 'administration/tasks/quiz/quiz_content_create_update.html'

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data()
        context['task'] = Quiz.objects.get(slug=self.kwargs["task_slug"])
        context['course_slug'] = self.kwargs['course_slug']
        context['task_slug'] = self.kwargs['task_slug']
        context['question_slug'] = self.kwargs['question_slug']

        context['formHeadline'] = _(
            "Create answer for %(question)s"
        ) % {'question': Question.objects.get(slug=self.kwargs['question_slug'])
             }
        context['formButtonLabel'] = _("Create answer")
        return context

    def get_success_url(self):
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.kwargs['task_slug']
        })

    def form_valid(self, form):
        question = get_object_or_404(Question, slug=self.kwargs['question_slug'])
        self.object = form.save(False)
        self.object.question = question
        self.object.save()
        response = super(AnswerCreateView, self).form_valid(form)
        messages.success(
            self.request,
            _("%(object)s created successfully") % {'object': self.object}
        )
        if not questionHasCorrectAnswer(self.object.question.id):
            messages.warning(
                self.request,
                _("%(object)s has no correct answer.") % {'object': self.object.question}
            )
        return response


class AnswerUpdateView(InstructorRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Answer
    form_class = AnswerForm
    template_name = 'administration/tasks/quiz/quiz_content_create_update.html'

    def get_context_data(self, **kwargs):
        context = super(UpdateView, self).get_context_data()
        context['task'] = Quiz.objects.get(slug=self.kwargs["task_slug"])
        context['course_slug'] = self.kwargs['course_slug']
        context['task_slug'] = self.kwargs['task_slug']
        context['answer_slug'] = self.kwargs['answer_slug']

        context['formHeadline'] = _(
            "Update answer for %(question)s."
        ) % {'question': Answer.objects.get(slug=self.kwargs['answer_slug']
                                            ).question
             }
        context['formButtonLabel'] = _("Update answer")
        return context

    def get_success_url(self):
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.kwargs['task_slug']
        })

    def form_valid(self, form):
        response = super(AnswerUpdateView, self).form_valid(form)

        messages.success(
            self.request,
            _("%(object)s updated successfully") % {'object': self.object}
        )
        if not questionHasCorrectAnswer(self.object.question.id):
            messages.warning(
                self.request,
                _("%(object)s has no correct answer.") % {'object': self.object.question}
            )
        return response

    def get_object(self):
        object = get_object_or_404(Answer, slug=self.kwargs['answer_slug'])
        return object


class AnswerDeleteView(InstructorRequiredMixin, LoginRequiredMixin, DeleteView):
    model = Answer
    template_name = 'administration/tasks/task_delete.html'

    def get_success_url(self):
        messages.success(
            self.request,
            _("%(object)s deleted successfully") % {'object': self.object}
        )
        if not questionHasCorrectAnswer(self.object.question.id):
            messages.warning(
                self.request,
                _("%(object)s has no correct answer.") % {'object': self.object.question}
            )
        return reverse('administration:quiz_update', kwargs={
            'course_slug': self.kwargs['course_slug'],
            'task_slug': self.kwargs['task_slug']
        })

    def get_object(self):
        object = get_object_or_404(Answer, slug=self.kwargs['answer_slug'])
        return object


def questionHasCorrectAnswer(questionId):
    answers = Question.objects.filter(id=questionId).first().get_answers()
    for answer in answers:
        if answer.is_correct:
            return True
    return False


class ExerciseTaskAdminDetailView(TaskAdminDetailView):
    model = Exercise
    template_name = 'administration/tasks/exercise/exercise_admin_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = ExerciseSolutionCreateForm()
        return context

    def get_object(self):
        object = get_object_or_404(Exercise, slug=self.kwargs['task_slug'])
        return object


class ExerciseTaskCreateView(TaskCreateView):
    form_class = ExerciseTaskCreateForm
    template_name = 'administration/tasks/exercise/exercise_create.html'
    selected_task_type = "exercise_create"


class ExerciseTaskUpdateView(TaskUpdateView):
    model = Exercise
    form_class = ExerciseTaskCreateForm
    template_name = 'administration/tasks/exercise/exercise_update.html'


class ProgrammingExerciseTaskAdminDetailView(TaskAdminDetailView):
    model = ProgrammingExercise
    template_name = 'administration/tasks/programmingexercise/exercise_admin_detail.html'

    def get_object(self):
        object = get_object_or_404(ProgrammingExercise, slug=self.kwargs['task_slug'])
        return object


class ProgrammingExerciseTaskCreateView(TaskCreateView):
    form_class = ProgrammingexerciseTaskCreateForm
    template_name = 'administration/tasks/programmingexercise/exercise_create.html'
    selected_task_type = "programmingexercise_create"


class ProgrammingExerciseTaskUpdateView(TaskUpdateView):
    model = ProgrammingExercise
    form_class = ProgrammingexerciseTaskCreateForm
    template_name = 'administration/tasks/programmingexercise/exercise_update.html'


class VideoTaskCreateView(TaskCreateView):
    form_class = VideoTaskCreateForm
    template_name = 'administration/tasks/video/video_create.html'
    selected_task_type = "video_create"


class VideoTaskUpdateView(TaskUpdateView):
    model = Video
    form_class = VideoTaskCreateForm
    template_name = 'administration/tasks/video/video_update.html'


class VideoTaskAdminDetailView(TaskAdminDetailView):
    model = Video
    template_name = 'administration/tasks/video/video_admin_detail.html'

    def get_object(self):
        object = get_object_or_404(Video, slug=self.kwargs['task_slug'])
        return object
