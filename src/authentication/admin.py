from django.contrib import admin

# Register your models here.
from .models import User, Specialty

admin.site.register(User)
admin.site.register(Specialty)
