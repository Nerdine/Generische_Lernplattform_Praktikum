from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import EmailField, ValidationError


User = get_user_model()


class UniqueEmailField(EmailField):
    def validate(self, value):
        super(EmailField, self).validate(value)
        if User.check_email_already_registered(value):
            raise ValidationError("Email already exists!")


class RegisterForm(UserCreationForm):
    email = UniqueEmailField(required=True, label="Email address")

    class Meta(UserCreationForm.Meta):
        model = get_user_model()
        fields = ['username', 'email']
