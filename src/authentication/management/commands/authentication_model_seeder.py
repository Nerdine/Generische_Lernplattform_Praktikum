from django.core.management.base import BaseCommand
from authentication.models import Event, Specialty, User
from faker import Faker
import datetime
from django.utils import timezone
from django.contrib.auth.hashers import make_password


class Command(BaseCommand):
    help = 'Seeds authentification models'

    def add_arguments(self, parser):
        parser.add_argument('number_of_seeds', nargs='+', type=int)

    used_usernames = list()
    used_position_institution_user_events = list()

    def unique_usernames(self):
        faker = Faker()
        current_username = faker.first_name()
        if current_username in self.used_usernames:
            return self.unique_usernames()
        else:
            if current_username is None or current_username == "":
                return self.unique_usernames()
            else:
                self.used_usernames.append(current_username)
                return current_username

    def unique_position_institution_user(self, current_position, current_institution, user):
        faker = Faker()
        if (current_position, current_institution, user) \
                in self.used_position_institution_user_events:
            current_institution = faker.company()
            return self.unique_position_institution_user(
                self, (current_position, current_institution, user))
        else:
            self.used_position_institution_user_events.append(
                (current_position, current_institution, user))
            return (current_position, current_institution, user)

    def handle(self, *args, **options):
        faker = Faker()
        # Convert first command line argument to integer in order
        # to create the postulated   number of seeds
        expire_date = datetime.timedelta(days=10) + timezone.now()
        mail = faker.ascii_free_email()
        super_user = User(password=make_password("test1234"), email=mail,
                          username="admin", first_name=faker.first_name(),
                          last_name=faker.last_name(), verifyEmailToken=""
                          .replace(" ", ""),
                          verifyEmailTokenExpireDate=expire_date,
                          can_be_instructor=1,
                          is_superuser=1, is_staff=1, is_active=1)
        super_user.save()

        for i in range(0, int(options['number_of_seeds'][0])):
            start_date_event = faker.date_this_decade(before_today=True, after_today=False)
            expire_date = datetime.timedelta(days=10) + timezone.now()
            mail = faker.ascii_free_email()

            username = self.unique_usernames()

            user = User(password=make_password('test1234'), email=mail,
                        username=username, first_name=faker.first_name(),
                        last_name=faker.last_name(), verifyEmailToken=""
                        .replace(" ", ""),
                        verifyEmailTokenExpireDate=expire_date, can_be_instructor=faker.boolean(),
                        is_active=1)
            user.save()

            for i in range(0, 3):

                current_position = faker.job()
                current_institution = faker.company()

                position_institution_user_key = self.unique_position_institution_user(
                    current_position, current_institution, user)

                event = Event.objects.create(position=position_institution_user_key[0],
                                             institution=position_institution_user_key[1],
                                             start_date=start_date_event,
                                             end_date=faker.date_between_dates(
                                                 date_start=start_date_event,
                                                 date_end=start_date_event +
                    datetime.timedelta(days=10)), user=user)
                event.save()

                specialty_titles = ["Zeichnen", "CSS", "HTML",
                                    "Python", "JavaScrips",
                                    "C++-Entwicklung", "C-Programmierung",
                                    "VHDL", "Visual Basic", "ObjectiveC",
                                    "Swift", "C#-Entwicklung",
                                    "Django", "Laravel", "NodeJs"]

                specialty = Specialty.objects.create(
                    title=specialty_titles[faker.random.randint(0, len(specialty_titles) - 1)],
                    knowledge_level=faker.random.randint(0, 2),
                    user=user)

                specialty.save()
