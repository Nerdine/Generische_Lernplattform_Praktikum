# Generated by Django 2.0.1 on 2018-03-29 20:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0006_auto_20180218_1558'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='has_email_notifications_activated',
            field=models.BooleanField(default=True, verbose_name='has email notifications activated'),
        ),
    ]
