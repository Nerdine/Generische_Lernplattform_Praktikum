from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import itertools
from django.template.defaultfilters import slugify


class User(AbstractUser):
    verifyEmailToken = models.CharField(max_length=100, null=True)
    verifyEmailTokenExpireDate = models.DateTimeField(null=True)
    can_be_instructor = models.BooleanField(
        _('Instructor'), default=settings.USER_ABLE_TO_BECOME_INSTRUCTOR)
    has_email_notifications_activated = models.BooleanField(_('Email notifications'),
                                                            default=True)

    @classmethod
    def get_user_by_email(cls, email):
        for user in User.objects.filter(email=email):
            return user
        raise ObjectDoesNotExist

    @classmethod
    def get_user_by_token(cls, token):
        for user in User.objects.filter(verifyEmailToken=token):
            return user
        raise ObjectDoesNotExist

    @classmethod
    def check_email_already_registered(cls, email):
        tmp = False
        for user in User.objects.filter(email=email):
            if user.email == email:
                tmp = True
        return tmp


class Event(models.Model):
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    institution = models.CharField(max_length=200, null=True)
    position = models.CharField(max_length=200, null=True)
    user = models.ForeignKey(User, verbose_name=_('user'),
                             related_name='events', on_delete=models.CASCADE, null=True)
    slug = models.SlugField(max_length=140, unique=True)

    # user + position + institution is unique to avoid duplicates
    class Meta:
        unique_together = ('user', 'institution', 'position',)

    def __str__(self):
        return '{0} - {1} - {2}'.format(self.user.username, self.institution, self.position)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify('%s %s %s' % (
                self.user.username, self.institution, self.position))

            for x in itertools.count(1):
                if not Event.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Event, self).save(*args, **kwargs)


class Specialty(models.Model):
    title = models.CharField(max_length=200, null=False)

    KNOWLEDGE_LEVEL_CHOICES = (
        ('1', _('Novice')),
        ('2', _('Adept')),
        ('3', _('Master')),
    )
    knowledge_level = models.CharField(
        _('Knowledge level'), max_length=1, choices=KNOWLEDGE_LEVEL_CHOICES)

    user = models.ForeignKey(User, verbose_name=_('user'),
                             related_name='specialties',
                             on_delete=models.CASCADE, null=True)
    slug = models.SlugField(max_length=140, unique=True)

    class Meta:
        verbose_name_plural = "specialties"

    def __str__(self):
        return '{}'.format(self.title)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify(self.title)

            for x in itertools.count(1):
                if not Specialty.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Specialty, self).save(*args, **kwargs)
