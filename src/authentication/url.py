from django.conf.urls import url

from . import views

app_name = 'auth'
urlpatterns = [
    # Password Reset
    url(r'password_reset/$', views.PasswordResetView.as_view(), name='password_reset'),
    url(r'password_reset/done/$', views.PasswordResetDoneView.as_view(),
        name='password_reset_done'),
    url(r'reset/done/$', views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete'),
    url(r'reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),

    # Registration
    url(r'register$', views.RegistrationView.as_view(), name="register"),
    url(r'register/sent$',
        views.EmailVerificationResetSentView.as_view(),
        name="email_verification_sent"),
]
