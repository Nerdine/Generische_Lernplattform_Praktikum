from celery import shared_task
import logging
import uuid
from datetime import timedelta

from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth import login
from django.contrib.auth import views
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.views import View
from django.views.generic import FormView, TemplateView

from .forms import RegisterForm

User = get_user_model()
logger = logging.getLogger(__name__)


class LoginView(views.LoginView):
    form_class = AuthenticationForm
    template_name = 'authentication/login.html'
    redirect_authenticated_user = True

    def get(self, request):
        if not request.GET.get('token') is None:
            user = User.get_user_by_token(request.GET.get('token'))
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            user.is_active = True
            user.verifyEmailToken = None
            user.save()
            messages.add_message(request, messages.SUCCESS, _('Email was activated successfully!'))
            return redirect('home')
        form = AuthenticationForm()
        return render(request, 'authentication/login.html', {'fail': False, 'form': form})


class LogoutView(views.LogoutView):
    next_page = 'login'


class HomeView(TemplateView):
    login_url = '/login'
    redirect_field_name = ''
    template_name = 'authentication/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['user_logged_in'] = True
        else:
            context['user_logged_in'] = False
        return context


class PasswordResetView(views.PasswordResetView):
    template_name = 'authentication/password_reset_form.html'
    email_template_name = 'authentication/password_reset_email.html'
    subject_template_name = 'authentication/password_reset_subject.txt'
    success_url = reverse_lazy('auth:password_reset_done')


class PasswordResetDoneView(views.PasswordResetDoneView):
    template_name = 'authentication/password_reset_done.html'


class PasswordResetConfirmView(views.PasswordResetConfirmView):
    template_name = 'authentication/password_reset_confirm.html'
    success_url = reverse_lazy('auth:password_reset_complete')


class PasswordResetCompleteView(views.PasswordResetCompleteView):
    template_name = 'authentication/password_reset_complete.html'


class RegistrationView(FormView):
    form_class = RegisterForm
    template_name = 'authentication/register.html'
    success_url = reverse_lazy('auth:email_verification_sent')

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('home')
        return super(RegistrationView, self).get(request)

    def form_valid(self, form):
        form.save()
        messages.success(self.request, _("Registration successful! Please verify E-Mail Token!"))
        user = User.get_user_by_email(form.cleaned_data['email'])
        token = str(uuid.uuid4())
        user.verifyEmailToken = token
        user.verifyEmailTokenExpireDate = timezone.now() + timedelta(
            hours=2)
        user.is_active = False
        user.save()

        email_verification_link = self.request.build_absolute_uri(
            reverse('login')) + '?token=' + token

        send_email_async.delay(email_verification_link, [form.cleaned_data['email']])

        return super(RegistrationView, self).form_valid(form)


@shared_task()
def send_email_async(email_verification_link, adress):
        send_mail(
            _('Verify Account'),
            email_verification_link,
            'admin@authentication.com',
            adress,
            fail_silently=False,
        )


class EmailVerificationResetSentView(View):
    def get(self, request):
        return render(request, 'authentication/email_verification_sent.html')


class EmailVerificationDoneView(View):
    def get(self, request):
        return render(request, 'authentication/email_verification_done.html')
