from django import forms
from prerequisite.models import Category
from django.utils.translation import ugettext as _


class SearchForm(forms.Form):
    q = forms.CharField(label=_('Text:'), required=False, max_length=100)

    categories = forms.ModelChoiceField(label=_('Category:'),
                                        queryset=Category.objects.all().order_by('type'),
                                        to_field_name='type',
                                        required=False
                                        )

    COURSE_LANGUAGE_CHOICES = (
        ('no', '--------'),
        ('en', 'English'),
        ('de', 'Deutsch'),
        ('fr', 'Français'),
    )

    languages = forms.TypedChoiceField(
        label=_('Language:'), choices=COURSE_LANGUAGE_CHOICES, initial='no', required=False)
