from django import template
from django.utils.translation import ugettext as _

from catalog.views import ACTION_LEAVE, ACTION_ENTER

register = template.Library()


@register.simple_tag
def get_action(course, participants):
    result = participants.get(course.slug)

    if result is None:
        return ACTION_ENTER
    else:
        return ACTION_LEAVE


@register.simple_tag
def get_action_name(course, participants):
    result = participants.get(course.slug)

    if result is None:
        return _('Enter')
    else:
        return _('Leave')
