from django.urls import path

from . import views

app_name = 'catalog'
urlpatterns = [
    path('courses', views.CoursesView.as_view(), name='courses'),
    path('<slug:course_slug>/', views.CourseDetailView.as_view(), name='course_detail'),

    path('<slug:course_slug>/instructor-details/<slug:instructor_slug>',
         views.CourseInstructorDetailView.as_view(), name='instructor_detail'),
]
