from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils.translation import ugettext as _
from django.views.generic import DetailView, ListView

from authentication.models import User
from catalog.form import SearchForm
from course.models import Course, Participant, Instructor

LANGUAGES = (
    ('', 'All'),
    ('en', 'English'),
    ('de', 'German'),
    ('fr', 'French'),
)

ACTION_LEAVE = 'leave'
ACTION_ENTER = 'enter'


class CoursesView(ListView):
    model = Course
    template_name = 'catalog/courses.html'

    def get_queryset(self):
        queryset = super(CoursesView, self).get_queryset()
        queryset = queryset.filter(published=True)

        if 'languages' in self.request.GET and self.request.GET['languages'] != 'no':
            queryset = queryset.filter(course_language__contains=self.request.GET['languages'])
        if 'q' in self.request.GET and self.request.GET['q'] != '':
            queryset = queryset.filter(Q(name__icontains=self.request.GET['q']) | Q(
                short_description__icontains=self.request.GET['q']) | Q(
                long_description__icontains=self.request.GET['q']))
        if 'categories' in self.request.GET and self.request.GET['categories'] != '':
            queryset = queryset.filter(categories__type__contains=self.request.GET['categories'])
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        initial_data = {}
        self.__add_query_param(initial_data, 'languages')
        self.__add_query_param(initial_data, 'q')
        self.__add_query_param(initial_data, 'categories')
        context['form'] = SearchForm(initial=initial_data)
        self.__add_course_data(context)
        return context

    def post(self, request):
        if request.user.is_authenticated is False:
            return redirect('login')

        course_slug = self.request.POST['course_slug']
        course = get_object_or_404(Course, slug=course_slug)
        participant = Participant.objects.filter(user=request.user, course=course).first()

        action = self.request.POST['action']

        if action == ACTION_ENTER:
            self.__handle_enter_course(course, participant)
            return redirect('course:continue', course_slug=course_slug)
        elif action == ACTION_LEAVE:
            self.__handle_leave_course(course, participant)
            return redirect('catalog:courses')
        else:
            return HttpResponse(status=500, content='the action ' + action + 'is invalid')

    def __add_query_param(self, initial_data, name):
        if name in self.request.GET:
            initial_data[name] = self.request.GET[name]

    def __add_course_data(self, context):
        participants = {}
        if self.request.user.is_authenticated:
            for participant in Participant.objects.filter(user=self.request.user):
                course = participant.course
                participants[course.slug] = 'True'
        context['participants'] = participants

    def __handle_enter_course(self, course, participant):
        if participant is None:
            Participant.objects.create(course=course, user=self.request.user, rating=0)
            self.__add_message('You were successfully added to the course %(name)s', course)
        else:
            self.__add_message('You are already a member of the course %(name)s', course)

    def __handle_leave_course(self, course, participant):
        if participant is None:
            self.__add_message('You are\'t a member of the course %(name)s', course)
        else:
            participant.delete()
            self.__add_message('You were successfully removed from the course %(name)s', course)

    def __add_message(self, message, course):
        messages.add_message(self.request, messages.SUCCESS, _(
            message % {'name': course.name}))


class CourseDetailView(DetailView):
    model = Course
    template_name = 'catalog/course_detail.html'
    slug_url_kwarg = 'course_slug'
    context_object_name = 'course'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_authenticated is False:
            context['authenticated'] = False
            context['participate'] = False
        else:
            context['authenticated'] = True
            participant = Participant.objects.filter(
                user=self.request.user, course=context['course']).first()
            if participant is None:
                context['participate'] = False
            else:
                context['participate'] = True

        return context

    def post(self, request, course_slug):
        if request.user.is_authenticated is False:
            return redirect('login')

        course = get_object_or_404(Course, slug=course_slug)
        participant = Participant.objects.filter(user=request.user, course=course).first()

        if participant is None:
            messages.add_message(request, messages.SUCCESS, _(
                'You were successfully added to the course %(name)s' % {'name': course.name}))
            Participant.objects.create(course=course, user=request.user, rating=0)
        else:
            messages.add_message(request, messages.SUCCESS, _(
                'You are already a member of the course %(name)s' % {'name': course.name}))

        return redirect('course:continue', course_slug=course_slug)


class CourseInstructorDetailView(LoginRequiredMixin, DetailView):
    context_object_name = 'course_list'
    template_name = 'catalog/instructor_detail.html'
    queryset = Course.objects.all()
    slug_url_kwarg = 'course_slug'

    def get_context_data(self, **kwargs):
        context = super(CourseInstructorDetailView, self).get_context_data(**kwargs)
        user = User.objects.filter(username=self.kwargs['instructor_slug']).first()
        recent_course = Course.objects.filter(slug=self.kwargs['course_slug']).first()

        context['instructor'] = user
        related_instructors = Instructor.objects.filter(user=user.id).all()

        courses = list()
        for related in related_instructors:
            courses.append(Course.objects.filter(id=related.course_id).first())
        context['courses'] = courses
        context['recent_course'] = recent_course
        return context
