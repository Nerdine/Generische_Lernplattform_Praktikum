from django import forms
from martor.fields import MartorFormField

from chat.models import Chat, Message, ChatStatus
from course.models import Course


class ConversationCreateForm(forms.Form):

    subject = forms.CharField()
    courses = forms.ModelMultipleChoiceField(
        queryset=Course.objects.none(),
        to_field_name='name',
        required=True
    )
    message = MartorFormField()

    def save_conversation(self, user):
        chat = Chat.objects.create(
            subject=self.cleaned_data['subject'], course=self.cleaned_data['courses'].first())
        message = Message.objects.create(
            content=self.cleaned_data['message'], chat=chat, author=user)
        ChatStatus.objects.create(user=user, chat=chat, isRead=True)
        return message

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(ConversationCreateForm, self).__init__(*args, **kwargs)
        self.fields['courses'].queryset = Course.objects.filter(instructor__user=user)


class MessageCreateForm(forms.Form):

    message = MartorFormField()

    def save_message(self, user, chat):
        message = Message.objects.create(
            content=self.cleaned_data['message'], chat=chat, author=user)
        chat.last_message_date_time = message.created_at
        chat.save()

        for status in ChatStatus.objects.filter(chat=chat):
            status.isRead = False
            status.save()

        return message
