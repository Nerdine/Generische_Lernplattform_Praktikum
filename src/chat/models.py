import itertools
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from course.models import Course

User = get_user_model()


class Chat(models.Model):
    subject = models.CharField(_('Subject'), max_length=80)
    course = models.ForeignKey(Course, verbose_name=_('Course'),
                               related_name='chats', on_delete=models.DO_NOTHING)
    last_message_date_time = models.DateTimeField(_('Last Message Datetime'), auto_now_add=True)
    slug = models.SlugField(max_length=140, unique=True)

    def __str__(self):
        return '{} - {}'.format(self.subject, self.course)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify(self.subject)

            for x in itertools.count(1):
                if not Chat.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Chat, self).save(*args, **kwargs)


class Message(models.Model):
    content = models.TextField(_("Content"))
    chat = models.ForeignKey(Chat, verbose_name=_('Chat'), related_name='messages',
                             on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)

    def __str__(self):
        return '{} - {} - {}'.format(self.content, self.chat, self.author)


class ChatStatus(models.Model):

    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    chat = models.ForeignKey(Chat, verbose_name=_(
        'Chat'), related_name='status', on_delete=models.CASCADE)
    isRead = models.BooleanField(_("isRead"))

    def __str__(self):
        return '{} - {} - {}'.format(self.user, self.chat, self.isRead)

    class Meta:
        unique_together = ('user', 'chat')
