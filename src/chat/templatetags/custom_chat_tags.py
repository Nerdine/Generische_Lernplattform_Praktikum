from django import template
from django.db.models import Q

from chat.models import ChatStatus, Chat
from course.models import Course

register = template.Library()


@register.simple_tag
def has_unread_messages_by_chat(chat, user):
    chat_status = ChatStatus.objects.filter(chat=chat, user=user).first()

    if chat_status is None:
        return "unread"

    if chat_status.isRead:
        return ""
    else:
        return "unread"


@register.simple_tag
def has_unread_messages(user):
    query = Q(participant__user=user)
    query.add(Q(instructor__user=user), Q.OR)
    courses = Course.objects.filter(query).distinct()
    # courses = Course.objects.filter(participant__user=user)
    for course in courses:
        for chat in Chat.objects.filter(course=course):
            if ChatStatus.objects.filter(chat=chat, user=user, isRead=True).first() is None:
                return "visible"

    return "invisible"
