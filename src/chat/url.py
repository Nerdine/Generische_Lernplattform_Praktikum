from django.urls import path

from . import views

app_name = 'chat'
urlpatterns = [

    path('', views.ConversationsView.as_view(), name='conversations'),
    path('create', views.ConversationCreateView.as_view(),
         name='conversation_create'),
    path('<slug:chat_slug>/delete', views.ConversationDeleteView.as_view(),
         name='conversation_delete'),
    path('<slug:chat_slug>/messages', views.MessageView.as_view(), name='messages'),
    path('<slug:chat_slug>/message/create', views.MessageCreateView.as_view(),
         name='message_create'),

]
