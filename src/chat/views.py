from celery import shared_task
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.template import loader
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _
from django.views.generic import ListView, FormView, DeleteView

from authentication.models import User
from chat.form import ConversationCreateForm, MessageCreateForm
from chat.models import Chat, Message, ChatStatus
from course.models import Course
from course.views import InstructorRequiredMixin


class ChatRequiredMixin(UserPassesTestMixin):
    # If you want to redirect to another page than login
    login_url = '/'

    def test_func(self):
        if 'chat_slug' in self.kwargs:
            chat_slug = self.kwargs['chat_slug']
            chat = get_object_or_404(Chat, slug=chat_slug)
            return self.request.user in chat.course.participants.all() or \
                self.request.user in chat.course.instructors.all()
        else:
            return False


class ConversationsView(LoginRequiredMixin, ListView):
    model = Chat
    template_name = 'chat/conversations.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current_user'] = self.request.user
        context['can_create'] = Course.objects.filter(
            instructor__user=self.request.user).first() is not None
        return context

    def get_queryset(self):
        queryset = super(ConversationsView, self).get_queryset()
        query = Q(course__participant__user=self.request.user)
        query.add(Q(course__instructor__user=self.request.user), Q.OR)
        return queryset.filter(query).distinct().order_by(
            '-last_message_date_time')


class MessageView(LoginRequiredMixin, ChatRequiredMixin, ListView):
    model = Message
    template_name = 'chat/messages.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['chat'] = self.chat
        context['form'] = MessageCreateForm()
        context['can_create'] = Course.objects.filter(
            instructor__user=self.request.user).first() is not None
        return context

    def get_queryset(self):
        queryset = super(MessageView, self).get_queryset()
        return queryset.filter(chat=self.chat)

    def get(self, request, *args, **kwargs):
        self.chat = get_object_or_404(Chat, slug=self.kwargs['chat_slug'])
        result = super().get(self, request, args, kwargs)

        status = ChatStatus.objects.filter(user=self.request.user, chat=self.chat).first()
        if status is not None:
            status.isRead = True
            status.save()
        else:
            ChatStatus.objects.create(user=self.request.user, chat=self.chat, isRead=True)
        return result


class ConversationCreateView(LoginRequiredMixin, InstructorRequiredMixin, FormView):
    template_name = 'chat/conversation_create.html'
    form_class = ConversationCreateForm
    success_url = reverse_lazy('chat:conversations')

    def get_form_kwargs(self):
        kwargs = super(ConversationCreateView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        message = form.save_conversation(self.request.user)
        notify_user_about_new_message(message, self.request)
        return super().form_valid(form)


class MessageCreateView(LoginRequiredMixin, ChatRequiredMixin, FormView):
    form_class = MessageCreateForm

    def get_success_url(self):
        return reverse('chat:messages', kwargs={'chat_slug': self.kwargs['chat_slug']})

    def form_valid(self, form):
        chat = get_object_or_404(Chat, slug=self.kwargs['chat_slug'])
        message = form.save_message(self.request.user, chat)
        notify_user_about_new_message(message, self.request)
        return super().form_valid(form)


class ConversationDeleteView(InstructorRequiredMixin, DeleteView):
    template_name = 'chat/conversation_delete.html'
    success_url = reverse_lazy('chat:conversations')

    def delete(self, request, *args, **kwargs):
        response = super(ConversationDeleteView, self).delete(self, request, *args, **kwargs)
        messages.success(
            self.request,
            _("%(chat)s deleted successfully") % {'chat': self.object.subject}
        )
        return response

    def get_object(self):
        object = get_object_or_404(Chat, slug=self.kwargs['chat_slug'])
        return object


def notify_user_about_new_message(message, request):
    chat = message.chat
    course = message.chat.course

    context = {
        'message_content': message.content,
        'link_to_the_message': request.build_absolute_uri(
            reverse('chat:messages', kwargs={'chat_slug': chat.slug})),
        'link_to_the_settings': request.build_absolute_uri(
            reverse('user_profile:edit_profile'))
    }

    body_as_html = loader.render_to_string('chat/email_body.html', context)
    body_as_txt = loader.render_to_string('chat/email_body.txt', context)

    for user in User.objects.filter(
            Q(instructor__course=course, has_email_notifications_activated=True) |
            Q(participant__course=course, has_email_notifications_activated=True)).distinct():

        if user == message.author:
            continue

        subject = course.name + ': ' + chat.subject
        from_email = 'lernplattform.webdev@gmail.com'
        recipient_list = [user.email]
        send_email_async.delay(subject, body_as_txt, from_email, recipient_list, body_as_html)


@shared_task()
def send_email_async(subject, body_as_txt, from_email, recipient_list, body_as_html):
    msg = EmailMultiAlternatives(subject, body_as_txt, from_email, recipient_list)
    msg.attach_alternative(body_as_html, 'text/html')
    msg.send()
