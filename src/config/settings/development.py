from config.settings.common import *

DEBUG = True
# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '6_xe0lr@gejcx+_&fuofvhj#tll8em**k=xcbq8pmxy%ql6cd='

ALLOWED_HOSTS = ['127.0.0.1',
                 'localhost']

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# EMAIL CONFIGURATION
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

STATIC_URL = '/static/'

# API KEYS
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = '927271581568-b4d7n9rj9nm6qih4b2fdfp3h6itbqoa8.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'rYUrvtbpO1Dd0HDJ7o78R4Gf'
SOCIAL_AUTH_GITHUB_KEY = '51db515973cea250f848'
SOCIAL_AUTH_GITHUB_SECRET = 'a3b978cada4c2d42873ccb7afaa638b70c36e15d'


# FUNCTIONAL TESTS
CHROMEDRIVER_PATH = os.path.abspath(os.path.join(os.path.dirname(BASE_DIR), 'venv/chromedriver-Linux64'))

# CELERY
CELERY_TASK_ALWAYS_EAGER = True