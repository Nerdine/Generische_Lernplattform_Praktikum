from config.settings.common import *
import os

DEBUG = True 

with open('/home/lernplattform/env/secret_key') as f:
        SECRET_KEY = f.read().strip()

ALLOWED_HOSTS = ['lernplattform.me', 'www.lernplattform.me']

CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

with open('/home/lernplattform/env/django_db_pw') as f:
    DJANGO_DB_PW = f.read().strip()
with open('/home/lernplattform/env/django_db_user') as f:
    DJANGO_DB_USER = f.read().strip()
with open('/home/lernplattform/env/django_db_name') as f:
    DJANGO_DB_NAME = f.read().strip()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DJANGO_DB_NAME,
        'USER': DJANGO_DB_USER,
        'PASSWORD': DJANGO_DB_PW,
        'HOST': 'localhost',
        'PORT': '',
    }
}

# EMAIL CONFIGURATION
EMAIL_BACKEND='django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS=True
EMAIL_HOST='smtp.gmail.com'
EMAIL_PORT=587
EMAIL_HOST_USER='lernplattform.webdev@gmail.com'
EMAIL_HOST_PASSWORD='formquickdelay'
DEFAULT_EMAIL_FROM='lernplattform.webdev@gmail.com'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

STATIC_URL = '/static/'
STATIC_ROOT = '/home/lernplattform/static/'


# API KEYS
with open('/home/lernplattform/env/github_key') as f:
        SOCIAL_AUTH_GITHUB_KEY = f.read().strip()
with open('/home/lernplattform/env/github_secret') as f:
        SOCIAL_AUTH_GITHUB_SECRET = f.read().strip()

# Celery
CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'
