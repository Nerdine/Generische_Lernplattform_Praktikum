"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin

from authentication import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.HomeView.as_view(), name="home"),
    url(r'^login$', views.LoginView.as_view(), name="login"),
    url(r'^logout$', views.LogoutView.as_view(), name="logout"),

    # Social Auth
    url(r'', include('social_django.urls', namespace='social')),

    url(r'^martor/', include('martor.urls')),

    # define sub module url routing
    url(r'^auth/', include('authentication.url', namespace="auth")),
    url(r'^course/', include('course.url', namespace="course")),
    url(r'^task/', include('task.url', namespace="task")),
    url(r'^category/', include('prerequisite.url', namespace="prerequisite")),
    url(r'^profile/', include('user_profile.url', namespace="profile")),
    url(r'^desk/', include('desk.url', namespace="desk")),
    url(r'^catalog/', include('catalog.url', namespace="catalog")),
    url(r'^administration/', include('administration.url', namespace="administration")),
    url(r'^chat/', include('chat.url', namespace="chat")),

    # Avatar urls
    url(r'^avatar/', include('avatar.urls')),
    url(r'^media/avatars/', include('avatar.urls')),
]


if settings.DEBUG:  # pragma: no cover
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

    # this is needed to load static files during development
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    # this is needed to load the media files during development
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
