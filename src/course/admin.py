from django.contrib import admin

# Register your models here.
from .models import Course, Participant, Instructor, Lesson

admin.site.register(Course)
admin.site.register(Participant)
admin.site.register(Instructor)
admin.site.register(Lesson)
