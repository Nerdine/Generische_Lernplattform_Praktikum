from django import forms
from .models import Course, Lesson
from prerequisite.models import Category, Prerequisite
from django.utils.translation import ugettext as _
from django.core.files.images import get_image_dimensions
from authentication.models import User


class BaseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        field_list = list(self.fields.values())

        if len(field_list) > 0:
            field_list[0].widget.attrs["autofocus"] = True


class CourseCreateForm(BaseForm):
    categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all().order_by('type'),
        to_field_name='type',
        required=False
    )
    prerequisites = forms.ModelMultipleChoiceField(
        queryset=Prerequisite.objects.all().order_by('-category'),
        to_field_name='slug',
        required=False
    )
    instructors = forms.ModelMultipleChoiceField(
        queryset=User.objects.filter(can_be_instructor=True),
        to_field_name='username',
        required=True,
        label='Instructors'
    )
    picture = forms.ImageField(required=False,
                               label=_('Picture (ideal picture size h/w 150px and 350px)'))

    def clean_picture(self):
        image = self.cleaned_data.get('picture', False)

        if image:
            w, h = get_image_dimensions(image)
            if image.size > 3 * 1024 * 1024:
                raise forms.ValidationError('Image file is too large ( > 3mb).')
            if w < 50 or w > 500:  # TODO discusse final values
                raise forms.ValidationError(
                    'Image file has wrong format ( recommend width between 50 px and 500 px )')
            if h < 10 or h > 300:
                raise forms.ValidationError(
                    'Image file has wrong format ( recommend height between 10 px and 300 px )')
        return image

    class Meta:
        model = Course
        fields = [
            'name',
            'short_description',
            'long_description',
            'course_language',
            'effort',
            'picture',
        ]

    def __init__(self, *args, **kwargs):
        super(CourseCreateForm, self).__init__(*args, **kwargs)
        self.fields['effort'].widget.attrs.update({
            'placeholder': _('Effort in minutes')
        })
        if kwargs['instance'] is not None:
            self.fields['categories'].initial = (
                Category.objects.filter(courses=kwargs['instance'])
            )
            self.fields['prerequisites'].initial = (
                Prerequisite.objects.filter(courses=kwargs['instance'])
            )
            self.fields['instructors'].initial = (
                Course.objects.filter(slug=kwargs['instance'].slug).first().instructors.all()
            )


class LessonCreateForm(BaseForm):
    class Meta:
        model = Lesson
        fields = ['name']
