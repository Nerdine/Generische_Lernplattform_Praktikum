from django.core.management.base import BaseCommand
from model_mommy.recipe import Recipe, foreign_key, seq
from course.models import Course, Instructor, Participant
from faker import Faker
from authentication.models import User
import datetime
from django.utils import timezone
from django.contrib.auth.hashers import make_password


class Command(BaseCommand):
    help = 'Seeds course models'
    used_usernames = list()
    used_usernames_db = list()

    def add_arguments(self, parser):
        parser.add_argument('number_of_seeds', nargs='+', type=int)

    def handle(self, *args, **options):
        faker = Faker()
        for i in range(options['number_of_seeds'][0]):
            expire_date = datetime.timedelta(days=10) + timezone.now()
            user = Recipe(User, password=make_password("test1234"),
                          username=seq(self.unique_usernames()),
                          first_name=faker.first_name(),
                          last_name=faker.last_name(), verifyEmailToken=''
                          .replace(" ", ""),
                          verifyEmailTokenExpireDate=expire_date,
                          can_be_instructor=faker.boolean())

            user2 = Recipe(User, password=make_password("test1234"),
                           username=seq(self.unique_usernames()),
                           first_name=faker.first_name(),
                           last_name=faker.last_name(), verifyEmailToken=''
                           .replace(" ", ""),
                           verifyEmailTokenExpireDate=expire_date,
                           can_be_instructor=faker.boolean())

            faker_sentence_length = faker.random.randint(3, 10)
            course_name = faker.sentence(nb_words=faker_sentence_length,
                                         variable_nb_words=True,
                                         ext_word_list=None)
            course = Recipe(Course,
                            name=course_name,
                            short_description=faker.word(),
                            long_description=faker.paragraph(
                                nb_sentences=3, variable_nb_sentences=True, ext_word_list=None),
                            course_language="en", effort=faker.random.randint(0, 50),
                            published=faker.boolean(), slug=course_name)

            instructor = Recipe(Instructor, user=foreign_key(
                user), make_m2m=True, course=foreign_key(course))
            participant = Recipe(Participant, rating=faker.random.randint(0, 50),
                                 user=foreign_key(user2),
                                 course=foreign_key(course), make_m2m=True)

            instructor.make()
            participant.make()
