from django.core.management.base import BaseCommand
from model_mommy.recipe import Recipe, foreign_key
from course.models import Course, Lesson
from faker import Faker


class Command(BaseCommand):
    help = 'Seeds course models'

    def add_arguments(self, parser):
        parser.add_argument('number_of_seeds', nargs='+', type=int)

    def handle(self, *args, **options):
        faker = Faker()
        for i in range(options['number_of_seeds'][0]):

            faker_sentence_length = faker.random.randint(3, 10)
            course_name = faker.sentence(nb_words=faker_sentence_length,
                                         variable_nb_words=True,
                                         ext_word_list=None)
            course = Recipe(Course,
                            name=course_name,
                            short_description=faker.word(),
                            long_description=faker.paragraph(
                                nb_sentences=3, variable_nb_sentences=True, ext_word_list=None),
                            course_language="en", effort=faker.random.randint(0, 50),
                            published=faker.boolean(),
                            picture=faker.image_url(width=None, height=None),
                            slug=course_name)

            faker_sentence_length = faker.random.randint(2, 13)
            lesson_name = faker.sentence(nb_words=faker_sentence_length, variable_nb_words=True,
                                         ext_word_list=None)
            lesson = Recipe(Lesson,
                            name=lesson_name,
                            position=faker.random.randint(0, 10),
                            course=foreign_key(course), slug=lesson_name)

            lesson.make()
