# Generated by Django 2.0.1 on 2018-03-04 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0005_auto_20180301_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='picture',
            field=models.ImageField(default=None, upload_to='course_images/', verbose_name='Picture'),
            preserve_default=False,
        ),
    ]
