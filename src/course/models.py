import itertools
from django.template.defaultfilters import slugify
from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxValueValidator, MinValueValidator

from task.models import TaskBase

User = get_user_model()


class Course(models.Model):
    name = models.CharField(_('Name'), max_length=55)
    short_description = models.CharField(_('Short description'), max_length=166)
    long_description = models.TextField(_('Long description'))
    COURSE_LANGUAGE_CHOICES = (
        ('no', '--------'),
        ('en', 'English'),
        ('de', 'Deutsch'),
        ('fr', 'Français'),
    )
    course_language = models.CharField(
        _('Course language'), max_length=2, choices=COURSE_LANGUAGE_CHOICES, default='no')
    # needed effort in minutes
    effort = models.PositiveIntegerField(_('Effort'), )
    # flag that says whether or not the course is available for users
    published = models.BooleanField(_('Published'), default=False)
    participants = models.ManyToManyField(User, through='Participant', related_name='participants')
    instructors = models.ManyToManyField(User, through='Instructor', related_name='instructors')
    picture = models.ImageField(_('Picture'), upload_to='course_images/',
                                blank=True)
    slug = models.SlugField(max_length=140, unique=True)

    def getPicture(self):
        if not self.picture:
            # depending on your template
            self.picture = '../static/img/Course.jpg'
        return self.picture.url

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify(self.name)

            for x in itertools.count(1):
                if not Course.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Course, self).save(*args, **kwargs)


class Instructor(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING)

    # user + course is unique to avoid duplicates
    class Meta:
        unique_together = ('user', 'course',)
        auto_created = True

    def __str__(self):
        return '{} - {}'.format(self.course, self.user)


class Participant(models.Model):
    # a user can rate a course only once
    # rating = 0 -> user hasn't rated yet for that course
    # rating > 0 -> user rated for that course
    rating = models.PositiveIntegerField(
        _('Rating'),
        validators=[MaxValueValidator(5), MinValueValidator(0)]
    )
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING)

    # useris + course is unique so that the user can only rate and join the course once
    class Meta:
        unique_together = ('user', 'course',)

    def __str__(self):
        return '{} - {}'.format(self.course, self.user)


class Lesson(models.Model):
    # name of the lesson
    name = models.CharField(_('Name'), max_length=100)
    # the position of the lesson amongst the other lessons
    position = models.PositiveIntegerField(_('Position'))
    # FK to course
    course = models.ForeignKey(Course, verbose_name=_('Course'),
                               related_name='lessons', on_delete=models.CASCADE)
    slug = models.SlugField(max_length=140, unique=True)

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify(self.name)

            for x in itertools.count(1):
                if not Lesson.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        if self.position is None:
            last_lesson = Lesson.objects.filter(
                course_id=self.course.id).order_by('-position').first()
            if last_lesson is None:
                self.position = 0
            else:
                self.position = last_lesson.position + 1

        super(Lesson, self).save(*args, **kwargs)

    @classmethod
    def get_all_lessons(cls):
        return cls.objects.all()

    def get_related_tasks(self):
        tasks = TaskBase.objects.select_related(
            'programmingexercise', 'exercise', 'text', 'video', 'quiz').filter(lesson=self)
        return tasks
