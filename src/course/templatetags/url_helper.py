from django import template
from django.urls import reverse

from task.models import Text, Quiz, Exercise, Video, ProgrammingExercise

register = template.Library()


@register.simple_tag
def get_task_update_url(task_type, task_slug, course_slug):
    args = {'course_slug': course_slug, 'task_slug': task_slug}
    if task_type == Text.type:
        return reverse('administration:text_update', kwargs=args)
    if task_type == Quiz.type:
        return reverse('administration:quiz_update', kwargs=args)
    if task_type == Exercise.type:
        return reverse('administration:exercise_update', kwargs=args)
    if task_type == ProgrammingExercise.type:
        return reverse('administration:programmingexercise_update', kwargs=args)
    if task_type == Video.type:
        return reverse('administration:video_update', kwargs=args)
    else:
        return reverse('home')


@register.simple_tag
def get_task_admin_detail_url(task_type, task_slug, course_slug):
    args = {'course_slug': course_slug, 'task_slug': task_slug}
    if task_type == Text.type or task_type == Quiz.type \
            or task_type == Exercise.type or task_type == ProgrammingExercise.type \
            or task_type == Video.type:
        return reverse('administration:' + task_type + '_admin_detail', kwargs=args)
    else:
        return reverse('home')
