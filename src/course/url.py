from django.urls import path

from . import views

app_name = 'course'
urlpatterns = [

    path('<slug:course_slug>/continue',
         views.CourseContinueView.as_view(), name='continue'),
]
