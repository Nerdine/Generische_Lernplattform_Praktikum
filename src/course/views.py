from django import template
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.shortcuts import get_object_or_404, redirect
from django.views import View
from django.utils.translation import ugettext as _

from desk.templatetags.desk_url_helper import get_task_detail_url
from task.models import TaskBase, UserSuccess
from .models import Course, Lesson

register = template.Library()


class InstructorRequiredMixin(UserPassesTestMixin):
    # If you want to redirect to another page than login
    login_url = '/'

    def test_func(self):
        if 'course_slug' in self.kwargs:
            course_slug = self.kwargs['course_slug']
            course = get_object_or_404(Course, slug=course_slug)
            return self.request.user in course.instructors.all()
        elif self.request.user.can_be_instructor:
            return self.request.user
        else:
            return False


class ParticipantRequiredMixin(UserPassesTestMixin):
    # If you want to redirect to another page than login
    login_url = '/'

    def test_func(self):
        if 'course_slug' in self.kwargs:
            course_slug = self.kwargs['course_slug']
            course = get_object_or_404(Course, slug=course_slug)
            return self.request.user in course.participants.all()
        elif 'task_slug' in self.kwargs:
            task_slug = self.kwargs['task_slug']
            task = get_object_or_404(TaskBase, slug=task_slug)
            return self.request.user in task.lesson.course.participants.all()
        else:
            return False


class CourseContinueView(LoginRequiredMixin, ParticipantRequiredMixin, View):

    def get(self, request, course_slug):
        course = get_object_or_404(Course, slug=course_slug)
        next_task = self.__calculate_next_task(course, request)

        if next_task is None:
            messages.warning(
                self.request,
                _("The course %(course)s has no content (Task).") % {'course': course}
            )
            return redirect('desk:courses')
        else:
            return redirect(get_task_detail_url(next_task.derived_type, next_task.slug))

    def __calculate_next_task(self, course, request):
        last_successful_task = self.__last_successful_task(course, request.user)

        if last_successful_task is None:
            return self.__first_course_task(course)

        next_task = self.next_task_of_equal_lesson(course, last_successful_task)

        if next_task is not None:
            return next_task

        next_task = self.__task_of_next_lesson(course, last_successful_task)

        if next_task is not None:
            return next_task

        return self.__first_course_task(course)

    @staticmethod
    def next_task_of_equal_lesson(course, last_successful_task):
        return TaskBase.objects.filter(
            lesson__course_id=course.id,
            lesson_id=last_successful_task.taskbase.lesson_id,
            position__gt=last_successful_task.taskbase.position). \
            order_by('position').first()

    @staticmethod
    def __first_course_task(course):
        return TaskBase.objects.filter(
            lesson__course_id=course.id).order_by(
            'lesson__position', 'position').first()

    @staticmethod
    def __task_of_next_lesson(course, user_success):
        next_lesson = Lesson.objects.filter(
            course_id=course.id,
            position__gt=user_success.taskbase.
            lesson.position).order_by('position').first()

        if next_lesson is None:
            return None

        return TaskBase.objects.filter(
            lesson__course_id=course.id,
            lesson_id=next_lesson.id).order_by(
            'position').first()

    @staticmethod
    def __last_successful_task(course, user):
        return UserSuccess.objects.filter(
            taskbase__lesson__course_id=course.id,
            successful=True,
            user=user).order_by(
            '-taskbase__lesson__position', '-taskbase__position').select_related().first()
