from celery import shared_task
import docker


# shared_task requires BROKER to perform correctly
@shared_task
def stop_docker_container(cont_id, img_name):

    client = docker.from_env()
    # stop container
    con = client.containers.get(cont_id)
    if con.status == "running":
        con.stop()

    try:
        # cleanup stopped container
        client.containers.prune()
        # cleanup image
        client.images.remove(img_name)
    except Exception:
        pass
