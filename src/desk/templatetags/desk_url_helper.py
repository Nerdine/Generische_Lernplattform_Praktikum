from django import template
from django.urls import reverse

from task.models import Text, Quiz, Exercise, Video, ProgrammingExercise

register = template.Library()


@register.simple_tag
def get_task_detail_url(task_type, task_slug):
    args = {'task_slug': task_slug}
    if task_type == Text.type:
        return reverse('desk:text_detail', kwargs=args)
    if task_type == Quiz.type:
        return reverse('desk:quiz_detail', kwargs=args)
    if task_type == Exercise.type:
        return reverse('desk:exercise_detail', kwargs=args)
    if task_type == ProgrammingExercise.type:
        return reverse('desk:programmingexercise_detail', kwargs=args)
    if task_type == Video.type:
        return reverse('desk:video_detail', kwargs=args)
    else:
        return reverse('home')
