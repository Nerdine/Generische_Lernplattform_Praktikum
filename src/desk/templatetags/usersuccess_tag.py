from django import template
from task.models import UserSuccess


register = template.Library()


@register.simple_tag
def get_user_success(task, user):
    user_success = UserSuccess.objects.filter(taskbase=task, user=user).first()
    if user_success is not None:
        return user_success.successful
    return None
