from django.urls import path

from . import views

app_name = 'desk'
urlpatterns = [
    path('courses', views.CoursesView.as_view(), name='courses'),

    path('task/<slug:task_slug>/previous', views.PreviousTaskView.as_view(),
         name='previous'),
    path('task/<slug:task_slug>/next', views.NextTaskView.as_view(),
         name='next'),
    path('task/text/<slug:task_slug>', views.TextTaskDetailView.as_view(),
         name='text_detail'),
    path('task/exercise/<slug:task_slug>', views.ExerciseTaskDetailView.as_view(),
         name='exercise_detail'),
    path('task/exercise/<slug:task_slug>/pass', views.ExerciseTaskPassView.as_view(),
         name='exercise_pass'),
    path('task/programmingexercise/<slug:task_slug>',
         views.ProgrammingExerciseTaskDetailView.as_view(),
         name='programmingexercise_detail'),
    path('task/quiz/<slug:task_slug>', views.QuizTaskDetailView.as_view(),
         name='quiz_detail'),
    path('task/quiz/<slug:task_slug>/pass', views.QuizTaskPassView.as_view(),
         name='quiz_pass'),
    path('task/video/<slug:task_slug>', views.VideoTaskDetailView.as_view(),
         name='video_detail'),
    path('<slug:course_slug>/', views.CourseDetailView.as_view(), name='course_detail'),

]
