from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView
from course.views import ParticipantRequiredMixin
from django.utils.translation import ugettext as _
import os

from course.models import Course, Participant
from desk.templatetags.desk_url_helper import get_task_detail_url
from task.docker.docker_helper import manage_docker_container
from task.forms import ExerciseSolutionCreateForm
from task.managers import CourseProgressManager
from task.models import TaskBase, Text, Quiz, UserAnswerSuccess, Exercise, \
    ProgrammingExercise, UserExerciseSolution, UserSuccess, Video


class CoursesView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'desk/courses.html'

    def get_queryset(self):
        queryset = super(CoursesView, self).get_queryset()

        participants = Participant.objects.filter(user=self.request.user)
        course_pks = []
        for participant in participants:
            course_pks.append(participant.course.pk)

        return queryset.filter(pk__in=course_pks, published=True)


class TaskDetailView(LoginRequiredMixin, ParticipantRequiredMixin, DetailView):
    slug_url_kwarg = 'task_slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        task = TaskBase.objects.select_related(
            'lesson', 'exercise', 'programmingexercise', 'text', 'video', 'quiz'
        ).get(slug=self.kwargs['task_slug'])

        context['task'] = task

        return context


class TextTaskDetailView(TaskDetailView):
    model = Text
    template_name = 'desk/tasks/text_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['text'] = context['task'].text
        return context


class QuizTaskDetailView(TaskDetailView):
    model = Quiz
    template_name = 'desk/tasks/quiz_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['quiz'] = context['task'].quiz
        user_answers = UserAnswerSuccess.objects.filter(
            user=self.request.user, quiz=context['task'].quiz)
        context['readonly'] = user_answers.count() != 0
        context['user_answers'] = user_answers
        return context


class QuizTaskPassView(LoginRequiredMixin, ParticipantRequiredMixin, View):

    def post(self, request, task_slug):
        quiz = get_object_or_404(Quiz, slug=task_slug)
        for question in quiz.questions.all():
            checked_answers = request.POST.getlist('question-' + str(question.id))
            for answer in question.answers.all():
                if answer.is_correct:
                    is_correct_answered = str(answer.id) in checked_answers
                else:
                    is_correct_answered = str(answer.id) not in checked_answers
                UserAnswerSuccess.objects.create(
                    user=request.user, quiz=quiz, answer=answer,
                    is_correct_answered=is_correct_answered)

        if UserSuccess.objects.filter(user=request.user, taskbase=quiz).first() is None:
            UserSuccess.objects.create(user=request.user, taskbase=quiz, successful=True)

        return redirect('desk:quiz_detail', task_slug=task_slug)


class ExerciseTaskDetailView(TaskDetailView):
    model = Exercise
    form_class = ExerciseSolutionCreateForm
    template_name = 'desk/tasks/exercise_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['exercise'] = context['task'].exercise
        user_solution = UserExerciseSolution.objects.filter(
            user=self.request.user, exercise=context['task'].exercise)
        context['readonly'] = user_solution.count() != 0
        context['user_solution'] = user_solution
        context['form'] = ExerciseSolutionCreateForm
        return context


class ExerciseTaskPassView(LoginRequiredMixin, ParticipantRequiredMixin, View):

    def post(self, request, task_slug):
        exercise = get_object_or_404(Exercise, slug=task_slug)
        solution = request.POST.get('solution', "")
        if solution == '' and len(request.FILES) > 0:
            file = request.FILES['file']
            file_type_ok = False
            if file is not None:
                # check file type
                ext = os.path.splitext(file.name)[1]
                valid_extensions = ['.txt']
                if not ext.lower() in valid_extensions:
                    messages.error(
                        self.request,
                        _("Unsupported file extension.")
                    )
                else:
                    file_type_ok = True

            if file_type_ok:
                if file.multiple_chunks():
                    messages.error(
                        self.request,
                        _("Uploaded file is too big (%(size)s.2f MB).") % {'size': file.size}
                    )
                else:
                    for chunks in file.chunks():
                        solution += chunks.decode('utf-8', errors="replace")

        if solution != '':
            UserExerciseSolution.objects.create(
                user=request.user, exercise=exercise, solution=solution,
                is_correct_answered=False, is_corrected=False)

            if UserSuccess.objects.filter(user=request.user, taskbase=exercise).first() is None:
                UserSuccess.objects.create(user=request.user, taskbase=exercise, successful=True)
        else:
            messages.error(
                self.request,
                _("You need to enter a solution (text or file).")
            )

        return redirect('desk:exercise_detail', task_slug=task_slug)


class ProgrammingExerciseTaskDetailView(TaskDetailView):
    model = ProgrammingExercise
    template_name = 'desk/tasks/programming_exercise_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['programmingexercise'] = context['task'].programmingexercise
        return context

    def post(self, request, task_slug):
        if request.is_ajax() is False or request.method != "POST":
            return HttpResponse('only POST.', status=500)

        programming_exercise = get_object_or_404(ProgrammingExercise, slug=task_slug)
        solution = request.POST.get('solution')
        out_buffer = manage_docker_container(programming_exercise.language, solution)

        if UserSuccess.objects.filter(user=request.user,
                                      taskbase=programming_exercise).first() is None:
            UserSuccess.objects.create(
                user=request.user, taskbase=programming_exercise, successful=True)

        return HttpResponse(out_buffer, task_slug, status=200)


class VideoTaskDetailView(TaskDetailView):
    model = Video
    template_name = 'desk/tasks/video_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['video'] = context['task'].video
        return context


class NextTaskView(LoginRequiredMixin, ParticipantRequiredMixin, View):

    def post(self, request, task_slug):
        task = get_object_or_404(TaskBase, slug=task_slug)
        course_slug = task.lesson.course.slug

        next_task = TaskBase.objects.filter(
            lesson=task.lesson, position__gt=task.position).order_by('position').first()

        # no task anymore in the current lesson
        if next_task is None:
            result = redirect('desk:course_detail', course_slug=course_slug)

            # take the next lesson and its first task
            next_lesson = task.lesson.course.lessons.all().\
                filter(position__gt=task.lesson.position).order_by('position').first()
            while next_lesson is not None and next_task is None:
                next_task = TaskBase.objects.filter(
                    lesson=next_lesson).order_by('position').first()

                # in case there is a lesson without any tasks, take the next lesson
                if next_task is None:
                    next_lesson = task.lesson.course.lessons.all().\
                        filter(position__gt=next_lesson.position).order_by('position').first()
                else:
                    result = redirect(get_task_detail_url(next_task.derived_type, next_task.slug))
        # there is still a task in the current lesson
        else:
            result = redirect(get_task_detail_url(next_task.derived_type, next_task.slug))

        if task.derived_type == Quiz.type or task.derived_type == Exercise.type or \
                task.derived_type == ProgrammingExercise.type:
            return result

        if UserSuccess.objects.filter(user=request.user, taskbase=task).first() is None:
            UserSuccess.objects.create(user=request.user, taskbase=task, successful=True)

        return result


class PreviousTaskView(LoginRequiredMixin, ParticipantRequiredMixin, View):

    def post(self, request, task_slug):
        task = get_object_or_404(TaskBase, slug=task_slug)
        course_slug = task.lesson.course.slug

        previous_task = TaskBase.objects.filter(
            lesson=task.lesson, position__lt=task.position).order_by('-position').first()

        # no task anymore in the current lesson
        if previous_task is None:
            result = redirect('desk:course_detail', course_slug=course_slug)

            # take the previous lesson and its last task
            previous_lesson = task.lesson.course.lessons.all().\
                filter(position__lt=task.lesson.position).order_by('-position').first()
            while previous_lesson is not None and previous_task is None:
                previous_task = TaskBase.objects.filter(
                    lesson=previous_lesson).order_by('position').last()

                # in case there is a lesson without any tasks, take the lesson before that
                if previous_task is None:
                    previous_lesson = task.lesson.course.lessons.all().\
                        filter(position__lt=previous_lesson.position).order_by('-position').first()
                else:
                    result = redirect(
                        get_task_detail_url(previous_task.derived_type, previous_task.slug))

            return result
        # there is still a task in the current lesson
        else:
            return redirect(get_task_detail_url(previous_task.derived_type, previous_task.slug))


class CourseDetailView(LoginRequiredMixin, ParticipantRequiredMixin, DetailView):
    model = Course
    template_name = 'desk/course_detail.html'
    slug_url_kwarg = 'course_slug'
    context_object_name = 'course'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        course = get_object_or_404(Course, slug=self.kwargs['course_slug'])
        context['progress'] = int(
            (CourseProgressManager.get_course_progress(self, course, self.request.user)) * 100)
        return context
