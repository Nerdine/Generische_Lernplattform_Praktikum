from django.test import LiveServerTestCase
from django.urls import reverse

from authentication.forms import User
from course.models import Course, Instructor

from .. import browser
from ..authentication import auth_functions


class AuthenticationTest(LiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')

    def tearDown(self):
        self.browser.quit()

    def test_course_registration(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)
        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])

        course1 = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )
        course2 = Course.objects.create(
            name='CourseB',
            short_description='DescriptionB',
            long_description='DescriptionBB',
            course_language='de',
            effort=2,
            published=False
        )
        Instructor.objects.create(
            user=user,
            course=course1
        )
        Instructor.objects.create(
            user=user,
            course=course2
        )

        self.browser.visit(self.front_page)
        self.browser.click_link_by_href(reverse('administration:course_overview'))
        if self.browser.is_element_not_present_by_text('CourseA'):
            self.fail("The courseA does not exist")
        if self.browser.is_element_not_present_by_text('CourseB'):
            self.fail("The courseB does not exist")

        self.browser.find_by_xpath(
            "//div[@class='course']/form/a[@class='details btn btn-primary']")[0].click()

        if self.browser.is_element_not_present_by_text('Course CourseA'):
            self.fail("The detail page of course A was not found")
