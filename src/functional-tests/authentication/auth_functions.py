from django.urls import reverse
from django.core import mail


DEFAULT_USER = {'email': 'lennon@thbeatles.com',
                'name': 'john', 'password': '<password>'}


def create_user(self, user):
    # Register the new user
    self.browser.visit(self.live_server_url + reverse('auth:register'))
    self.browser.find_by_id('id_username').fill(user['name'])
    self.browser.find_by_id('id_email').fill(user['email'])
    self.browser.find_by_id('id_password1').fill(user['password'])
    self.browser.find_by_id('id_password2').fill(user['password'])
    # click register button
    self.browser.find_by_xpath("//input[@type='submit']")[0].click()

    self.assertEquals(self.live_server_url +
                      reverse('auth:email_verification_sent'), self.browser.url)
    # execute the token link of the email
    self.assertEqual(len(mail.outbox), 1)
    self.browser.visit(mail.outbox[0].body)

    # logout
    self.browser.visit(self.live_server_url + reverse('logout'))
    self.assertEquals(self.live_server_url + reverse('login'), self.browser.url)


def login_user(self, user):
    self.browser.visit(self.live_server_url + reverse('login'))
    self.browser.find_by_id('id_username').fill(user['name'])
    self.browser.find_by_id('id_password').fill(user['password'])
    # click login button
    self.browser.find_by_xpath("//input[@type='submit']")[0].click()
    self.assertEquals(self.front_page, self.browser.url)
