import re
from django.test import LiveServerTestCase
from django.urls import reverse
from django.core import mail

from . import auth_functions
from .. import browser


class AuthenticationTest(LiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')

    def tearDown(self):
        self.browser.quit()

    def test_register_and_login_and_logout(self):
        """tests that a user can be registered, logged in and logged out"""
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)

        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        # click logout button
        self.browser.find_link_by_href("/logout")[0].click()
        self.assertEquals(self.live_server_url +
                          reverse('login'), self.browser.url)

    def test_authentication(self):
        """tests the redirects with/without authentication"""
        # not logged and access /desk
        self.browser.visit(self.live_server_url + reverse('desk:courses'))
        self.assertEquals(self.live_server_url + reverse('login') + '?next=' +
                          reverse('desk:courses'), self.browser.url)
        # login and access /login
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)
        self.browser.visit(self.live_server_url + reverse('login'))
        self.assertEquals(self.front_page, self.browser.url)

    def test_login_with_wrong_input(self):
        """test trys to login with wrong name and/or pw"""
        user = auth_functions.DEFAULT_USER
        login_url = self.live_server_url + reverse('login')
        # name wrong - pw true
        self.browser.visit(login_url)
        self.browser.find_by_id('id_username').fill("wrong")
        self.browser.find_by_id('id_password').fill(user['password'])
        self.browser.find_by_xpath("//input[@type='submit']")[0].click()
        self.assertEquals(login_url, self.browser.url)
        self.assertIn("Please enter a correct username and password.", self.browser.html)
        # name true - pw wrong
        self.browser.visit(login_url)
        self.browser.find_by_id('id_username').fill(user['name'])
        self.browser.find_by_id('id_password').fill("wrong")
        self.browser.find_by_xpath("//input[@type='submit']")[0].click()
        self.assertEquals(login_url, self.browser.url)
        self.assertIn("Please enter a correct username and password.", self.browser.html)
        # name wrong - pw wrong
        self.browser.visit(login_url)
        self.browser.find_by_id('id_username').fill("wrong")
        self.browser.find_by_id('id_password').fill("wrong")
        self.browser.find_by_xpath("//input[@type='submit']")[0].click()
        self.assertEquals(login_url, self.browser.url)
        # name empty - pw true
        self.browser.visit(login_url)
        self.browser.find_by_id('id_password').fill(user['password'])
        self.browser.find_by_xpath("//input[@type='submit']")[0].click()
        self.assertEquals(login_url, self.browser.url)
        # name true - pw empty
        self.browser.visit(login_url)
        self.browser.find_by_id('id_username').fill(user['name'])
        self.browser.find_by_xpath("//input[@type='submit']")[0].click()
        self.assertEquals(login_url, self.browser.url)
        # name empty - pw empty
        self.browser.visit(login_url)
        self.browser.find_by_xpath("//input[@type='submit']")[0].click()
        self.assertEquals(login_url, self.browser.url)

    def test_password_reset(self):
        """tests the entire password rest worflow """
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)

        # start the password request workflow for a specific email address
        self.browser.visit(self.live_server_url + reverse('auth:password_reset'))
        self.browser.find_by_id('id_email').fill(
            auth_functions.DEFAULT_USER['email'])
        self.browser.find_by_xpath("//button[@type='submit']")[0].click()

        self.assertEquals(self.live_server_url +
                          reverse('auth:password_reset_done'), self.browser.url)

        # check that the token link was sent per email
        self.assertEqual(len(mail.outbox), 2)

        url_match = re.search(
            r"https?://[^/]*(/.*reset/\S*)", mail.outbox[1].body)
        self.assertIsNotNone(url_match, "No URL found in sent email")
        path = url_match.groups()[0]

        new_password = 'Utjd#kls4'

        # call the token link
        self.browser.visit(self.live_server_url + path)

        # set the new password
        self.browser.find_by_id('id_new_password1').fill(new_password)
        self.browser.find_by_id('id_new_password2').fill(new_password)
        self.browser.find_by_xpath("//button[@type='submit']")[0].click()

        self.assertEquals(self.live_server_url + reverse('auth:password_reset_complete'),
                          self.browser.url)

        # check that the new password works
        user_with_new_password = auth_functions.DEFAULT_USER
        user_with_new_password['password'] = new_password
        auth_functions.login_user(self, user_with_new_password)
