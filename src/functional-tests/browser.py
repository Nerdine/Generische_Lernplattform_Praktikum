import os
from splinter import Browser
from selenium import webdriver
from django.conf import settings


def setup(self):
    if os.environ.get('CI') is not None:  # CI --> headless chrome
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        return Browser('chrome', chrome_options)
    else:
        if os.environ.get('FF')is not None:  # Wenn Firefox verwendet werden soll
            return Browser('firefox', capabilities={"javascriptEnabled": True})
        else:  # Wenn normaler Chrome verwendet werden soll
            executable_path = {'executable_path': settings.CHROMEDRIVER_PATH}
            return Browser('chrome', **executable_path)
