from django.contrib.auth import get_user_model
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

from course.models import Course, Participant
from .. import browser
from ..authentication import auth_functions

User = get_user_model()


class CourseCatalogTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')
        self.course_catalog_page = self.live_server_url + reverse('catalog:courses')
        self.__create_courses()

    def tearDown(self):
        # self.browser.quit()
        pass

    def test_language_search(self):
        self.browser.visit(self.course_catalog_page)

        self.browser.select('languages', 'en')
        self.browser.find_by_value('Search').click()

        if self.browser.is_text_not_present('CourseEnPublished'):
            self.fail('CourseEnPublished was not found')

        if self.browser.is_text_present('CourseDePublished'):
            self.fail('CourseDePublished was found')
        if self.browser.is_text_present('CourseEnNotPublished'):
            self.fail('CourseEnNotPublished was found')

    def __create_courses(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        self.user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])

        self.course_en_published = Course.objects.create(
            name='CourseEnPublished',
            short_description='ShortEnPublished',
            long_description='LongEnPublished',
            course_language='en',
            effort=1,
            published=True
        )
        Course.objects.create(
            name='CourseEnNotPublished',
            short_description='ShortEnNotPublished',
            long_description='LongEnNotPublished',
            course_language='en',
            effort=2,
            published=False
        )
        self.course_de_published = Course.objects.create(
            name='CourseDePublished',
            short_description='ShortDePublished',
            long_description='LongDePublished',
            course_language='de',
            effort=2,
            published=True
        )
        Participant.objects.create(course=self.course_de_published, user=self.user, rating=0)
