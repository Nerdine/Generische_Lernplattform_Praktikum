from django.test import LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

from course.models import Course, Instructor, Lesson

from .. import browser
from ..authentication import auth_functions
from django.contrib.auth import get_user_model


User = get_user_model()


class TestCourseCrud(LiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')

    def tearDown(self):
        self.browser.quit()

    def test_create(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        self.user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        self.user.can_be_instructor = True
        self.user.save()

        self.browser.visit(self.front_page)
        self.browser.click_link_by_href(reverse('administration:courses'))

        self.browser.find_link_by_text('Create a new course').first.click()
        if self.browser.is_text_not_present('Create course'):
            self.fail("The 'Create' link did not work")

        self.browser.find_by_id('id_name').fill('CourseA')
        self.browser.find_by_id('id_short_description').fill('CourseADesc')
        self.browser.find_by_id('id_long_description').fill('CourseADescription')
        self.browser.select_by_text('course_language', 'English')
        self.browser.find_by_id('id_effort').fill('5')

        self.browser.find_by_value('Create').first.click()

        if self.browser.is_text_not_present('CourseA'):
            self.fail("The creation failed")

    def test_update(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        course = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )
        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        Instructor.objects.create(user=user, course=course)

        self.browser.visit(self.front_page)
        self.browser.click_link_by_href(reverse('administration:courses'))

        self.browser.find_link_by_text('Edit').first.click()
        if self.browser.is_text_not_present('Course CourseA'):
            self.fail("The 'Edit' link did not work")

        self.browser.find_link_by_text('Edit the course details').first.click()
        if self.browser.is_text_not_present('Update course'):
            self.fail("The 'Edit the course details' link did not work")

        self.browser.find_by_id('id_name').fill('NameChangedForA')
        self.browser.find_by_value('Update').first.click()

        if self.browser.is_text_not_present('NameChangedForA'):
            self.fail("The update failed")


class TestLessonCrud(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')

    def tearDown(self):
        self.browser.quit()

    def test_create(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        course = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )
        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        Instructor.objects.create(user=user, course=course)

        self.browser.visit(self.live_server_url + reverse('administration:courses'))

        self.browser.find_link_by_text('Edit').first.click()
        if self.browser.is_text_not_present('Course CourseA'):
            self.fail("The 'Edit' link did not work")

        self.browser.find_link_by_text('Create a new lesson').first.click()
        if self.browser.is_text_not_present('Create lesson'):
            self.fail("The 'Create a new lesson' link did not work")

        self.browser.find_by_id('id_name').fill('Lesson1')

        self.browser.find_by_value('Create').first.click()

        if self.browser.is_text_not_present('Lesson1'):
            self.fail("The creation failed")

    def test_update(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        course = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )
        Lesson.objects.create(
            name='Lesson1',
            position=1,
            course=course
        )
        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        Instructor.objects.create(user=user, course=course)

        self.browser.visit(self.live_server_url + reverse('administration:courses'))

        self.browser.find_link_by_text('Edit').first.click()
        if self.browser.is_text_not_present('Course CourseA'):
            self.fail("The 'Edit' link did not work")

        self.browser.find_by_xpath('//*[@title="Edit the lesson"]').first.click()
        if self.browser.is_text_not_present('Update lesson'):
            self.fail("The 'Edit the lesson' link did not work")

        self.browser.find_by_id('id_name').fill('NameChangedForLesson1')
        self.browser.find_by_value('Update').first.click()

        if self.browser.is_text_not_present('NameChangedForLesson1'):
            self.fail("The update failed")

    def test_delete(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        course = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )
        Lesson.objects.create(
            name='Lesson1',
            position=1,
            course=course
        )
        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        Instructor.objects.create(user=user, course=course)

        self.browser.visit(self.live_server_url + reverse('administration:courses'))

        self.browser.find_link_by_text('Edit').first.click()
        if self.browser.is_text_not_present('Course CourseA'):
            self.fail("The 'Edit' link did not work")

        self.browser.find_by_xpath('//*[@title="Delete the lesson"]').first.click()
        if self.browser.is_text_not_present('Delete lesson'):
            self.fail("The 'Delete the lesson' link did not work")

        self.browser.find_by_value('Confirm').first.click()

        if self.browser.is_text_not_present('Lesson1 deleted successfully'):
            self.fail("The delete failed")
