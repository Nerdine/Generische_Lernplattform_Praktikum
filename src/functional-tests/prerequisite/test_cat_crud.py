import unittest

from django.test import LiveServerTestCase
from django.urls import reverse
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

import tempfile
import os

from .. import browser
from ..authentication import auth_functions
from django.contrib.auth import get_user_model
from config.settings.development import MEDIA_ROOT
from PIL import Image

from prerequisite.models import Category


User = get_user_model()


def get_image():
    f = tempfile.NamedTemporaryFile(suffix='.png', prefix='tmptest', dir=MEDIA_ROOT, delete=True)
    Image.new('RGB', (50, 50), 'grey').save(f, 'PNG')
    return f


@unittest.skip("test_create muss gefixt werden bzgl Javascript")
class TestCategoryCrud(LiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')
        self.image = get_image()

    def tearDown(self):
        self.browser.quit()
        self.image.close()

    def test_create(self):
        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        self.user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        self.user.can_be_instructor = True
        self.user.save()

        self.browser.visit(self.front_page)
        self.browser.click_link_by_href(reverse('administration:course_overview'))

        self.browser.find_link_by_text('Create').first.click()
        if self.browser.is_element_not_present_by_text('Create course'):
            self.fail("The 'Create' link did not work")

        self.browser.find_link_by_text('Create category').first.click()

        wait = WebDriverWait(self.browser.driver, 10)
        element = wait.until(EC.visibility_of_element_located((By.ID, 'category_form')))

        if element is not None:
            self.browser.find_by_id('id_type').fill('CSS')
            self.browser.driver\
                .find_element_by_xpath("//input[@type='file']")\
                .send_keys(self.image.name)

            self.browser.find_by_value('Add new category').first.click()

            if self.browser.is_text_not_present('CSS'):
                self.fail("The creation failed")

            path = MEDIA_ROOT + '/category_images/'
            os.remove(os.path.join(path, self.image.name.split("/").pop()))
        else:
            self.fail("The category form was not visible after 10 seconds.")

    def test_update(self):
        Category.objects.create(type='CSS', picture=self.image.name)

        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        self.user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        self.user.can_be_instructor = True
        self.user.save()

        self.browser.visit(self.front_page)
        self.browser.click_link_by_href(reverse('prerequisite:category_overview'))

        create_link = self.browser.find_link_by_text('Update').first
        create_link.click()
        if self.browser.is_element_not_present_by_text('Update category'):
            self.fail("The 'Update' link did not work")

        self.browser.find_by_id('id_type').fill('HTML')

        update_button = self.browser.find_by_value('Update').first
        update_button.click()

        if self.browser.is_text_not_present('HTML'):
            self.fail("The update failed")

    def test_delete(self):
        Category.objects.create(type='CSS', picture=self.image.name)

        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        self.user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])
        self.user.can_be_instructor = True
        self.user.save()

        self.browser.visit(self.front_page)
        self.browser.click_link_by_href(reverse('prerequisite:category_overview'))

        delete_link = self.browser.find_link_by_text('Delete').first
        delete_link.click()
        if self.browser.is_element_not_present_by_text('Delete category'):
            self.fail("The 'Delete' link did not work")

        confirm_button = self.browser.find_by_value('Confirm').first
        confirm_button.click()

        if self.browser.is_text_not_present('CSS deleted successfully'):
            self.fail("The delete failed")
