import unittest
from time import sleep

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

from course.models import Course, Participant, Lesson
from task.models import ProgrammingExercise
from .. import browser
from ..authentication import auth_functions
from django.contrib.auth import get_user_model

User = get_user_model()


# fixme: nach LPF-116 nicht mehr skippen
@unittest.skip("Skipping Hello Docker until Docker Fix LPF-116")
class ProgrammingexerciseTaskTest(StaticLiveServerTestCase):
    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')

    def tearDown(self):
        self.browser.quit()

    def test_docker_hello_world(self):
        course = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )

        lesson = Lesson.objects.create(
            course=course,
            name='Lesson',
            description='Description',
            position=1,
        )

        programmingExercise = ProgrammingExercise.objects.create(
            task="Schreibe Hello World in Python",
            name="Python Übung",
            position=0,
            derived_type="programmingexercise",
            lesson=lesson
        )

        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])

        Participant.objects.create(rating=0, user=user, course=course)

        self.browser.visit(self.live_server_url +
                           reverse('administration:programmingexercise_detail', kwargs={
                               'task_slug': programmingExercise.slug
                           }))
        self.browser.fill('solution', 'print("Hello World")')

        self.browser.find_by_id('sendSolution').first.click()
        sleep(3)

        rows = self.browser.driver.find_element_by_id(
            'messageContainer').find_elements_by_tag_name('li')
        self.assertEquals(0, len(rows), "Es sind gab unerwartete Messages")

        text = self.browser.driver.find_element_by_id('output').get_attribute('value')
        self.assertIn("Hello World", text)
