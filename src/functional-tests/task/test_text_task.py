from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse

from course.models import Course, Instructor, Participant, Lesson
from .. import browser
from ..authentication import auth_functions
from django.contrib.auth import get_user_model

User = get_user_model()


class TextTaskTest(StaticLiveServerTestCase):

    def setUp(self):
        self.browser = browser.setup(self)
        self.front_page = self.live_server_url + reverse('home')

    def tearDown(self):
        self.browser.quit()

    def test_markdown_preview(self):
        course = Course.objects.create(
            name='CourseA',
            short_description='DescriptionA',
            long_description='DescriptionAA',
            course_language='de',
            effort=1,
            published=True
        )

        lesson = Lesson.objects.create(
            course=course,
            name='Lesson',
            position=1,
        )

        auth_functions.create_user(self, auth_functions.DEFAULT_USER)
        auth_functions.login_user(self, auth_functions.DEFAULT_USER)

        user = User.objects.get(email=auth_functions.DEFAULT_USER['email'])

        Instructor.objects.create(user=user, course=course)
        Participant.objects.create(rating=0, user=user, course=course)

        self.browser.visit(self.live_server_url + reverse('administration:text_create', kwargs={
            'course_slug': course.slug, 'lesson_slug': lesson.slug}))
        self.browser.fill('name', 'TaskName')

        # set the focus to the markdown input field
        self.browser.find_by_xpath('//textarea').first.clear()

        active_web_element = self.browser.driver.switch_to_active_element()
        active_web_element.send_keys('**Foo**')

        # assert the markdown preview
        self.browser.find_by_xpath("//div[contains(@class, 'tab-martor-menu')]/a")[1].click()
        if self.browser.is_element_not_present_by_xpath("//strong[text()='Foo']"):
            self.fail('the preview text is not present')

        self.browser.find_by_value('Create').first.click()
        self.assertEquals(self.live_server_url + reverse('administration:course_detail', kwargs={
            'course_slug': course.slug}), self.browser.url)
