from django.contrib import admin

# Register your models here.
from .models import Prerequisite, Category

admin.site.register(Prerequisite)
admin.site.register(Category)
