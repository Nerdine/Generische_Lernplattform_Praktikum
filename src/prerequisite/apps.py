from django.apps import AppConfig


class PrerequisiteConfig(AppConfig):
    name = 'prerequisite'
