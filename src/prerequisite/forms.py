from django import forms
from django.core.files.images import get_image_dimensions
from .models import Category
from course.forms import BaseForm
from django.utils.translation import ugettext_lazy as _


def clean_cat(self, image, cat_type):
    if cat_type is None or cat_type is '':
        raise forms.ValidationError(_('Invalid category type'))

    if image:
        w, h = get_image_dimensions(image)
        if image.size > 3 * 1024 * 1024:
            raise forms.ValidationError(_('Image file is too large ( > 3mb).'))
        if w < 50 or w > 500:  # TODO discusse final values
            raise forms.ValidationError(_(
                'Image file has wrong format ( recommend width between 50 px and 500 px )'))
        if h < 10 or h > 300:
            raise forms.ValidationError(_(
                'Image file has wrong format ( recommend height between 10 px and 300 px )'))

    existing_cat = Category.objects.filter(type=cat_type).first()
    if existing_cat is not None and existing_cat.pk != self.instance.pk:
        raise forms.ValidationError(_('A category with that name already exists.'))


class CategoryForm(BaseForm):
    type = forms.CharField(required=False)
    picture = forms.ImageField(required=False)

    def clean(self):
        image = self.cleaned_data['picture']
        cat_type = self.cleaned_data['type']
        clean_cat(self, image, cat_type)

    class Meta:
        model = Category
        fields = [
            'type',
            'picture'
        ]


class CategoryAjaxCreateForm(BaseForm):
    type = forms.CharField(required=False)
    cat_picture = forms.ImageField(required=False, label=_("Picture"))

    def clean(self):
        image = self.cleaned_data['cat_picture']
        cat_type = self.cleaned_data['type']
        clean_cat(self, image, cat_type)

    class Meta:
        model = Category
        fields = [
            'type'
        ]
