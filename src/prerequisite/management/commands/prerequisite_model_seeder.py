from django.core.management.base import BaseCommand
from model_mommy.recipe import Recipe, foreign_key, related
from prerequisite.models import Category, Prerequisite
from course.models import Course
from faker import Faker


class Command(BaseCommand):
    help = 'Seeds course models'

    def add_arguments(self, parser):
        parser.add_argument('number_of_seeds', nargs='+', type=int)

    def handle(self, *args, **options):
        faker = Faker()

        for i in range(0, int(options['number_of_seeds'][0])):
            faker_sentence_length = faker.random.randint(2, 10)

            course = Recipe(Course,
                            name=faker.sentence(nb_words=faker_sentence_length,
                                                variable_nb_words=True,
                                                ext_word_list=None),
                            short_description=faker.word(),
                            long_description=faker.paragraph(
                                nb_sentences=3, variable_nb_sentences=True, ext_word_list=None),
                            course_language="en", effort=faker.random.randint(0, 50),
                            published=faker.boolean(), slug=faker.word())

            category = Recipe(Category, type=faker.word(),
                              picture=faker.image_url(width=None, height=None),
                              slug=faker.word(), courses=related(course), make_m2m=True)

            prerequisite = Recipe(Prerequisite, slug=faker.word(),
                                  category=foreign_key(category),
                                  courses=related(course),
                                  skill_level=faker.random.randint(1, 3), make_m2m=True)
            prerequisite.make()
