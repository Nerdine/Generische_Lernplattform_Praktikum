from django.db import models
from django.utils.translation import ugettext_lazy as _
import itertools
from django.template.defaultfilters import slugify


class Category(models.Model):
    # type like: CSS, JQuery
    type = models.CharField(_('Type'), max_length=80)
    # picture
    picture = models.ImageField(_('Picture'), upload_to='category_images/', blank=True)
    # FK to course
    courses = models.ManyToManyField('course.Course', verbose_name=_('Courses'),
                                     related_name='categories', blank=True)
    slug = models.SlugField(max_length=140, unique=True)

    def getPicture(self):
        if not self.picture:
            # depending on your template
            self.picture = '../static/img/category-default-image.png'
        return self.picture.url

    def __str__(self):
        return '{}'.format(self.type)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify(self.type)

            for x in itertools.count(1):
                if not Category.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Category, self).save(*args, **kwargs)


class Prerequisite(models.Model):
    SKILL_LEVEL_CHOICES = (
        ('1', _('Novice')),
        ('2', _('Adept')),
        ('3', _('Master')),
    )
    skill_level = models.CharField(_('Skill level'), max_length=1, choices=SKILL_LEVEL_CHOICES)
    category = models.ForeignKey(Category, verbose_name=_('Category'),
                                 related_name='prerequisites', on_delete=models.CASCADE)
    courses = models.ManyToManyField('course.Course', verbose_name=_('Courses'),
                                     related_name='prerequisites', blank=True)
    slug = models.SlugField(max_length=140, unique=True)

    # sill level + category is unique to avoid duplicates
    class Meta:
        unique_together = ('skill_level', 'category',)

    def __str__(self):
        return '{0} - {1}'.format(self.get_skill_level_display(), self.category.type)

    def save(self, *args, **kwargs):
        if not self.id or self.slug is None:
            self.slug = orig = slugify('%s %s' % (self.category.type, self.skill_level))

            for x in itertools.count(1):
                if not Prerequisite.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        super(Prerequisite, self).save(*args, **kwargs)
