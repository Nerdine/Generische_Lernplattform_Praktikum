from django.urls import path

from . import views

app_name = 'prerequisite'
urlpatterns = [
    # category
    path('create_category',
         views.create_category_ajax, name='create_category_ajax'),
    path('create_category/',
         views.CategoryCreateView.as_view(), name='category_create'),
    path('update_category/<slug:category_slug>/',
         views.CategoryUpdateView.as_view(), name='category_update'),
    path('delete_category/<slug:category_slug>/',
         views.CategoryDeleteView.as_view(), name='category_delete'),
    path('overview/categories',
         views.CategoryOverviewView.as_view(), name='category_overview'),
]
