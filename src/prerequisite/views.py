import json
from functools import reduce

from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy, reverse
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.views.generic import UpdateView, DeleteView, ListView, CreateView, View

from .models import Category, Prerequisite
from .forms import CategoryForm, CategoryAjaxCreateForm
from course.views import InstructorRequiredMixin


class CategoryOverviewView(InstructorRequiredMixin, LoginRequiredMixin, ListView):
    model = Category
    template_name = 'prerequisite/category_overview.html'

    def get_queryset(self):
        return Category.objects.all().order_by('type')


class CategoryBaseView(InstructorRequiredMixin, LoginRequiredMixin, View):
    model = Category
    success_url = reverse_lazy('prerequisite:category_overview')

    def get_object(self):
        object = get_object_or_404(Category, slug=self.kwargs['category_slug'])
        return object


class CategoryCreateView(CategoryBaseView, CreateView):
    form_class = CategoryForm
    template_name = 'prerequisite/category_create.html'

    def form_valid(self, form):
        response = super(CategoryCreateView, self).form_valid(form)
        for level in Prerequisite.SKILL_LEVEL_CHOICES:
            Prerequisite.objects.create(skill_level=level[0], category=self.object)
        messages.success(
            self.request,
            _("'%(category)s' created successfully") % {'category': self.object}
        )
        return response


class CategoryUpdateView(CategoryBaseView, UpdateView):
    form_class = CategoryForm
    template_name = 'prerequisite/category_update.html'

    def form_valid(self, form):
        self.object = form.save(False)

        # used to put the old name in the error messages
        not_yet_updated_object = get_object_or_404(Category, pk=self.object.pk)

        update_failed = 0
        if self.object.courses.all().count() > 0:
            messages.error(
                self.request,
                _("'%(category)s' cannot be updated. It is used for a course.")
                % {'category': not_yet_updated_object}
            )
            update_failed = 1
        if reduce(lambda x, y: x + y,
                  map(lambda x: x.courses.all().count(),
                      Prerequisite.objects.filter(category=self.object)
                      )
                  ) > 0:
            messages.error(
                self.request,
                _("'%(category)s' cannot be updated. It is used as a prerequisite for a course.")
                % {'category': not_yet_updated_object}
            )
            update_failed = 1

        if update_failed is 0:
            self.object.save()
            messages.success(
                self.request,
                _("'%(category)s' updated successfully") % {'category': self.object}
            )

        return HttpResponseRedirect(
            reverse('prerequisite:category_overview')
        )


class CategoryDeleteView(CategoryBaseView, DeleteView):
    template_name = 'prerequisite/category_delete.html'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        category = get_object_or_404(Category, slug=self.kwargs['category_slug'])
        deletionFailed = 0
        if category.courses.all().count() > 0:
            messages.error(
                self.request,
                _("'%(category)s' cannot be deleted. It is used for a course.")
                % {'category': self.object}
            )
            deletionFailed = 1
        if reduce(lambda x, y: x + y,
                  map(lambda x: x.courses.all().count(),
                      Prerequisite.objects.filter(category=category)
                      )
                  ) > 0:
            messages.error(
                self.request,
                _("'%(category)s' cannot be deleted. It is used as a prerequisite for a course.")
                % {'category': self.object}
            )
            deletionFailed = 1
        if deletionFailed is 0:
            self.object.delete()
            messages.success(
                self.request,
                _("'%(category)s' deleted successfully") % {'category': self.object}
            )

        return HttpResponseRedirect(
            reverse('prerequisite:category_overview')
        )


# Ajax calls
def create_category_ajax(request):
    if request.is_ajax() and request.method == 'POST':
        form = CategoryAjaxCreateForm(request.POST, request.FILES)
        if form.is_valid():
            cat_type = form.cleaned_data['type']
            cat_picture = form.cleaned_data['cat_picture']

            new_cat = Category.objects.create(
                type=cat_type,
                picture=cat_picture
            )
            data = {'category': new_cat.__str__(), 'id': new_cat.id}

            # add prerequisites
            data.update({'prerequisites': []})
            for level in Prerequisite.SKILL_LEVEL_CHOICES:
                prerequisite = Prerequisite.objects.create(skill_level=level[0], category=new_cat)
                data['prerequisites'].append(
                    {
                        'slug': prerequisite.slug,
                        'value': prerequisite.__str__()
                    }
                )
            return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        error_list = list(form.errors.values())
        if len(error_list) > 0:
            return HttpResponse(error_list[0], status=500)
        return HttpResponse('The entered data is not valid.', status=500)
    return HttpResponse('only POST.', status=500)
