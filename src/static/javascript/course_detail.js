$(function () {

    var structureIsEdited = false;

    $('#btn-edit-course-structure').click(function (e) {

        $(this).parent().parent().toggleClass("structureUpdate");
        if (is_touch_device()) {
            $(this).parent().parent().toggleClass("touch");
        }

        if ($(this).hasClass("enabled")) {
            $(this).removeClass("enabled")

            if (is_touch_device()) {
            }
            else {
                $("ol.course-structure").sortable("disable");
            }
            updateCourseStructure();
        }
        else {
            $(this).addClass("enabled")
            if (is_touch_device()) {
            }
            else {
                $("ol.course-structure").sortable("enable");
            }
        }
    });

    $('.lesson.up').click(function (e) {
        var lesson = $(this).parent().parent();
        lesson.insertBefore(lesson.prev());
        structureIsEdited = true;
    });

    $('.lesson.down').click(function (e) {
        var lesson = $(this).parent().parent();
        lesson.next().insertBefore(lesson);
        structureIsEdited = true;
    });

    $('.task.up').click(function (e) {
        var task = $(this).parent().parent();
        task.insertBefore(task.prev());
        structureIsEdited = true;
    });

    $('.task.down').click(function (e) {
        var task = $(this).parent().parent();
        task.next().insertBefore(task);
        structureIsEdited = true;
    });

    var group = $("ol.course-structure").sortable({
        isValidTarget: function ($item, container) {

            if (container.el.length === 0) {
                return false;

            }

            if ($item.is(".task")) {
                if (container.el[0].classList.contains("task-list")) {
                    return true;
                }
            }

            if ($item.is(".lesson")) {
                if (container.el[0].classList.contains("lesson-list")) {
                    return true;
                }
            }
            return false;
        },
        onDrop: function ($item, container, _super) {
            structureIsEdited = true;
            _super($item, container);
        }
    });
    $("ol.course-structure").sortable("disable");

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function updateCourseStructure() {

        if (structureIsEdited == false) {
            return;
        }

        structureIsEdited = false;

        var data = group.sortable("serialize").get();
        var jsonString = JSON.stringify(data, null, ' ');

        if (jsonString === "") {
            return;
        }

        var form_data = new FormData();
        form_data.append("structure", jsonString);
        form_data.append("csrfmiddlewaretoken", getCookie('csrftoken'));

        var course_slug = document.location.pathname.split("/")[3];

        $.ajax({
            type: 'POST',
            cache: false,
            url: '/administration/course/' + course_slug + '/update/structure',
            data: form_data,
            processData: false,
            contentType: false,
            success: function (data, status, xhr) {
                if (data === "") {
                    return;
                }
                addMessage("The update of the course structure was successfully", "alert-success");
            },
            error: function (data, status, error) {
                var messageType = 'alert-danger';
                if (data.status === 500) {
                    addMessage(data.responseText, messageType);
                } else if (data.status === 0) {
                    addMessage("Timeout, please try again later", messageType);
                } else {
                    addMessage("Unknown error (" + data.status + "). Please contact the admin" , messageType);
                }
            }
        });
    }

    function is_touch_device() {
        return (('ontouchstart' in window)
            || (navigator.MaxTouchPoints > 0)
            || (navigator.msMaxTouchPoints > 0));
    }
});
