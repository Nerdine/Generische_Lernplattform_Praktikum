$("#toggle_category_form").on('click', function(e){
    e.preventDefault();

    $("#category_form").slideToggle(200, function() {
        var form = document.getElementById("category_form");
        if (form.offsetParent !== null) {
            form.scrollIntoView({behavior: "smooth", block: "end", inline: "end"});
    }
  });
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var form_data = new FormData();
$("#id_cat_picture").on('change', function(){
    var file = this.files[0];
    if (form_data) {
        form_data.append("cat_picture", file);
    }
});

$("#create_category_but").on('click', function(e){
    e.preventDefault();

    form_data.append("type", $("#id_type").val());
    form_data.append("csrfmiddlewaretoken", getCookie('csrftoken'));

    var messageContainer = document.getElementById("messageContainer");
    while (messageContainer.firstChild) {
        messageContainer.removeChild(messageContainer.firstChild)
    }

    $.ajax({
        type: 'POST',
        cache: false,
        url: category_url,
        data: form_data,
        processData: false,
        contentType: false,
        success: function (data, status, xhr) {
            $("#id_type").val("");
            $("#id_cat_picture").val("");
            form_data = new FormData();

            $('#id_categories').append('<option value="'+data.category+'">' + data.category + '</option>');
            // add this category to category select for prerequisites
            for (i = 0; i < data.prerequisites.length; i++) {
                $('#id_prerequisites').append('<option value="'+ data.prerequisites[i].slug +'">' + data.prerequisites[i].value + '</option>');
            }
            addMessage("The category was created successfully", "alert-success");

        },
        error: function(data, status, error) {
            // unselect the category picture
            $("#id_cat_picture").val("");
            form_data = new FormData();

            var messageType = 'alert-danger';
            if (data.status === 500) {
                addMessage(data.responseText, messageType);
            } else if (data.status === 0) {
                addMessage("Timeout, please try again later", messageType);
            } else {
                addMessage("Unknown error (" + data.status + "). Please contact the admin" , messageType);
            }
        }
    });
});
