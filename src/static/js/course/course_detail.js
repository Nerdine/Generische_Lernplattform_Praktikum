function toggleDetails(lesson_slug){
  var show_link = document.getElementById("link_" + lesson_slug);
  if(show_link.classList.contains("show_link")){
    show_link.classList.remove("show_link")
    show_link.classList.add("hide_link");
    show_link.text = "Hide details";
  }
  else {
    show_link.classList.add("show_link")
    show_link.classList.remove("hide_link");
    show_link.text = "Show details";
  }

  $("#details_" + lesson_slug).slideToggle();
}