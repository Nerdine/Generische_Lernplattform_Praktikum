/* creates a new message inside the message container and scrolls it into view
    params:
        messageText: Text to be displayed in the message
        messageType: alert-danger, alert-info, alert-warning, alert-error, etc.
*/
function addMessage(messageText, messageType) {

    $('#messageContainer').append(
        $('<div></div>')
        .attr({ "roleName": 'alert' })
        .addClass('alert')
        .addClass(messageType)
        .addClass('alert-dismissible')
        .addClass('fade')
        .addClass('show')
        .append(messageText)
        .append(
            $('<button></button>')
            .attr({ "data-dismiss": 'alert',
                "aria-label": 'Close',
                "type": 'button'})
            .addClass('close')
            .append(
                $('<span></span>')
                .attr({ "aria-hidden": "true" })
                .append("×")
            )
        )
    );

    document.getElementById("messageContainer").firstElementChild.scrollIntoView();
}