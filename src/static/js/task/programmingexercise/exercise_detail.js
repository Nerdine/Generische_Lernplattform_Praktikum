function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function setSpinningWheelVisible(bool) {
    if (bool) {
        $('#send_code_but').prop("disabled", true);
        $('#spinning_wheel').removeClass('invisible');
    } else {
        $('#send_code_but').prop("disabled", false);
        $('#spinning_wheel').addClass('invisible');
    }
}

$("#send_code_but").on('click', function(e){
    setSpinningWheelVisible(true);
    var messageContainer = document.getElementById("messageContainer");
    while (messageContainer.firstChild) {
        messageContainer.removeChild(messageContainer.firstChild)
    }
    var solution = ace.edit("solution");

    $.ajax({
        type: "POST",
        cache: false,
        url: '/desk/task/programmingexercise/' + window.top.location.pathname.split('/').slice(-1)[0],  // desk:programmingexercise_detail
        data: {
            csrfmiddlewaretoken: getCookie('csrftoken'),
            solution: solution.session.getValue(),
        },
        success: function(data, textStatus, xhr){
            $("#output").html(data);
            setSpinningWheelVisible(false);
        },
        error: function(data, textStatus, errorThrown) {
            var messageType = 'alert-danger';
            if (data.status === 500) {
                addMessage(data.responseText, messageType);
            } else if (data.status === 0) {
                addMessage("Timeout, please try again later", messageType);
            } else {
                addMessage("Unknown error (" + data.status + "). Please contact the admin" , messageType);
            }
            setSpinningWheelVisible(false);
        }
    });
});