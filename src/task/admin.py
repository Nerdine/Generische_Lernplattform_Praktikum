from django.contrib import admin
from martor.widgets import AdminMartorWidget

from django.db import models

from authentication.models import Event
from .models import Text, Video, Exercise, Quiz, Answer, Question, UserAnswerSuccess, \
    UserSuccess, ProgrammingExercise, UserExerciseSolution


class MarkdownModelAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminMartorWidget},
    }


admin.site.register(Video)
admin.site.register(Text, MarkdownModelAdmin)
admin.site.register(Exercise)
admin.site.register(ProgrammingExercise)
admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(UserAnswerSuccess)
admin.site.register(UserSuccess)
admin.site.register(UserExerciseSolution)
admin.site.register(Event)
