import datetime

from datetime import timedelta
from django.conf import settings
import docker
import os
import shutil
import tempfile
from django import template
from desk.tasks import stop_docker_container

register = template.Library()

# Prerequesite:
# docker-ce is installed
# docker daemon is running ($ systemctl status docker.service)
# there is a usergroup 'docker' ($ sudo groupadd docker) --> already satisfied with docker-ce
# user is in the docker usergroup ($ sudo gpasswd -a $USER docker)
# optional reboot


def manage_docker_container(lang, code):
    container = start_docker_container(lang, code)
    stop_docker_container.apply_async(
        args=[container.container_id, container.image_name],
        eta=datetime.datetime.now() + timedelta(seconds=settings.MAXIMUM_CONTAINER_LIFETIME)
    )

    outBuffer = ""  # todo realtime response
    for line in container.outStream:
        print(str(line, "UTF8"))
        outBuffer += str(line, "UTF8")

    return outBuffer


def start_docker_container(lang, code):
    with tempfile.TemporaryDirectory() as TMP_DIR:

        shutil.copyfile(settings.DOCKER_PATH + "/dockerfiles/" +
                        lang + "/dockerfile", TMP_DIR + "/dockerfile")
        with open(TMP_DIR + "/main", "w") as f:
            f.write(code)
        os.chmod(TMP_DIR + "/main", 0o555)

        client = docker.from_env()
        tag = os.path.basename(TMP_DIR)
        image = client.images.build(path=TMP_DIR, tag=tag)

        container = client.containers.run(
            image,
            detach=True,
            mem_limit=settings.MAXIMUM_CONTAINER_RAM,  # memory limitation (must have)
            pids_limit=settings.MAXIMUM_CONTAINER_PID_LIMIT,  # pid amount
            cpu_period=50000,
            cpu_quota=int(50000 * settings.MAXIMUM_CONTAINER_CPU_RATIO / 100),
            # cpu limitation (must have)
            network_disabled=True,  # disabled networking
            privileged=False
        )

        def createdContainer(): return None
        createdContainer.outStream = container.logs(stream=True)
        createdContainer.container_id = container.id
        createdContainer.image_name = tag
        return createdContainer
