from django.forms import FileField
from martor.fields import MartorFormField
from django.utils.translation import ugettext as _

from course.forms import BaseForm
from .models import Exercise, UserExerciseSolution
from .models import ProgrammingExercise
from .models import Text, Quiz, Question, Answer, TaskBase, Video


class TaskBaseCreateForm(BaseForm):
    class Meta:
        model = TaskBase
        fields = []


class TextTaskCreateForm(BaseForm):
    content = MartorFormField()

    class Meta:
        model = Text
        fields = ['name', 'content']


class ProgrammingexerciseTaskCreateForm(BaseForm):
    class Meta:
        model = ProgrammingExercise
        fields = ['name', 'task', 'language']


class ExerciseTaskCreateForm(BaseForm):
    task = MartorFormField()
    solution = MartorFormField()

    class Meta:
        model = Exercise
        fields = ['name',  'task', 'solution']


class ExerciseSolutionCreateForm(BaseForm):
    solution = MartorFormField(required=False)
    file = FileField(
        label=_("Choose file (supported type: .txt)"),
        required=False
    )

    class Meta:
        model = UserExerciseSolution
        fields = ['solution']


class VideoTaskCreateForm(BaseForm):
    class Meta:
        model = Video
        fields = ['name', 'upload']


class QuizTaskCreateForm(BaseForm):
    class Meta:
        model = Quiz
        fields = ['name']


class QuestionForm(BaseForm):
    class Meta:
        model = Question
        fields = ['text']


class AnswerForm(BaseForm):
    class Meta:
        model = Answer
        fields = ['text', 'is_correct']
