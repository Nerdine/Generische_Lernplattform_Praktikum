from django.core.management.base import BaseCommand
from task.models import TaskBase, Exercise, ProgrammingExercise, UserExerciseSolution
from task.models import Text, Video, Quiz, Question, Answer, UserAnswerSuccess, UserSuccess
from course.models import Lesson, Course
from faker import Faker
from authentication.models import User


class Command(BaseCommand):
    help = 'Seeds course models'

    def add_arguments(self, parser):
        parser.add_argument('number_of_seeds', nargs='+', type=int)

    used_course_names = list()
    used_lesson_names = list()
    used_question_texts = list()
    used_answer_texts = list()

    def unique_course_names(self, faker_sentence_length):
        faker = Faker()
        current_course_name = faker.sentence(nb_words=faker_sentence_length,
                                             variable_nb_words=True,
                                             ext_word_list=None)
        if current_course_name in self.used_course_names:
            return self.unique_course_names(faker_sentence_length)
        else:
            if current_course_name is None:
                return self.unique_course_names(faker_sentence_length)
            else:
                self.used_course_names.append(current_course_name)
                return current_course_name

    def unique_lesson_names(self):
        faker = Faker()
        faker_sentence_length = faker.random.randint(1, 8)
        current_lesson_name = faker.sentence(
            nb_words=faker_sentence_length, variable_nb_words=True, ext_word_list=None)
        if current_lesson_name in self.used_lesson_names:
            return self.unique_lesson_names()
        else:
            if current_lesson_name is None:
                return self.unique_lesson_names()
            else:
                self.used_lesson_names.append(current_lesson_name)
                return current_lesson_name

    def unique_question_text(self):
        faker = Faker()
        faker_sentence_length = faker.random.randint(1, 8)
        current_question_text = faker.sentence(nb_words=faker_sentence_length,
                                               variable_nb_words=True,
                                               ext_word_list=None)
        if current_question_text in self.used_question_texts:
            return self.unique_question_text(self)
        else:
            if current_question_text is None:
                return self.unique_question_text(self)
            else:
                self.used_question_texts.append(current_question_text)
                return current_question_text

    def unique_answer_text(self):
        faker = Faker()
        faker_sentence_length = faker.random.randint(1, 8)
        current_answer_text = faker.sentence(nb_words=faker_sentence_length,
                                             variable_nb_words=True,
                                             ext_word_list=None)
        if current_answer_text in self.used_answer_texts:
            return self.unique_answer_text(self)
        else:
            if current_answer_text is None:
                return self.unique_answer_text(self)
            else:
                self.used_answer_texts.append(current_answer_text)
                return current_answer_text

    def handle(self, *args, **options):
        faker = Faker()
        for i in range(0, int(options['number_of_seeds'][0])):

            faker_sentence_length = faker.random.randint(3, 10)
            courses = Course.objects.all()

            lesson_name = self.unique_lesson_names()
            lesson = Lesson.objects.create(
                name=lesson_name,
                position=faker.random.randint(1, 10),
                course=courses[faker.random.randint(0, len(courses) - 1)], slug=lesson_name)

            lesson.save()
            task_base_name = faker.word()
            task_base = TaskBase.objects.create(name=task_base_name,
                                                slug=task_base_name,
                                                position=faker.random.randint(0, 50),
                                                derived_type=faker.word(),
                                                lesson=lesson)
            task_base.save()

            for j in range(0, 3):
                faker_sentence_length = faker.random.randint(1, 10)
                video_name = faker.sentence(nb_words=faker_sentence_length, variable_nb_words=True,
                                            ext_word_list=None)
                video = Video.objects.create(upload=faker.word(),
                                             name=video_name, slug=video_name,
                                             position=faker.random.randint(0, 50),
                                             derived_type=faker.word(),
                                             lesson=lesson)
                video.save()
                faker_sentence_length = faker.random.randint(1, 10)
                text_name = faker.sentence(nb_words=faker_sentence_length, variable_nb_words=True,
                                           ext_word_list=None)
                text = Text.objects.create(content=faker.word(),
                                           name=text_name, slug=text_name,
                                           position=faker.random.randint(0, 50),
                                           derived_type=faker.word(),
                                           lesson=lesson)
                faker_sentence_length = faker.random.randint(1, 10)
                quiz_name = faker.sentence(nb_words=faker_sentence_length, variable_nb_words=True,
                                           ext_word_list=None)
                quiz = Quiz.objects.create(name=quiz_name, slug=quiz_name,
                                           position=faker.random.randint(0, 50),
                                           derived_type=faker.word(),
                                           lesson=lesson)
                faker_sentence_length = faker.random.randint(1, 10)
                exercise_name = faker.sentence(nb_words=faker_sentence_length,
                                               variable_nb_words=True,
                                               ext_word_list=None)
                exercise = Exercise.objects.create(task=faker.word(),
                                                   solution=faker.word(), name=exercise_name,
                                                   slug=exercise_name,
                                                   position=faker.random.randint(0, 50),
                                                   derived_type=faker.word(),
                                                   lesson=lesson)
                faker_sentence_length = faker.random.randint(1, 10)
                programming_exercise_name = faker.sentence(nb_words=faker_sentence_length,
                                                           variable_nb_words=True,
                                                           ext_word_list=None)
                programming_exercise = ProgrammingExercise.objects.create(
                    task=faker.sentence(nb_words=faker_sentence_length,
                                        variable_nb_words=True,
                                        ext_word_list=None), name=programming_exercise_name,
                    slug=programming_exercise_name,
                    position=faker.random.randint(0, 50),
                    derived_type=faker.word(),
                    lesson=lesson)

                text.save()
                quiz.save()
                exercise.save()
                programming_exercise.save()

            users = User.objects.all().exclude(id=1)

            user_exercise_solution = UserExerciseSolution.objects.create(
                exercise=exercise,
                user=users[faker.random.randint(0, len(users) - 1)],
                solution=faker.sentence(nb_words=faker_sentence_length,
                                        variable_nb_words=True,
                                        ext_word_list=None),
                is_correct_answered=faker.boolean(),
                is_corrected=faker.boolean()
            )
            user_exercise_solution.save()
            question_text = self.unique_question_text()
            question = Question.objects.create(
                slug=question_text,
                text=question_text,
                position=faker.random.randint(0, 50),
                quiz=quiz
            )
            question.save()

            answer = Answer.objects.create(slug=self.unique_answer_text,
                                           text=faker.sentence(nb_words=6,
                                                               variable_nb_words=True,
                                                               ext_word_list=None),
                                           is_correct=faker.boolean(),
                                           position=faker.random.randint(0, 50),
                                           question=question)
            answer.save()

            useranswersuccess = UserAnswerSuccess.objects.create(
                quiz=quiz,
                answer=answer,
                user=users[faker.random.randint(0, len(users) - 1)],
                is_correct_answered=faker.boolean()
            )
            useranswersuccess.save()

            # Hier sind taskbase und user zusammen unique
            usersuccess = UserSuccess.objects.create(
                user=users[faker.random.randint(0, len(users) - 1)],
                successful=faker.boolean(), taskbase=task_base
            )
            usersuccess.save()
