from django.db import models

from task.models import UserSuccess, TaskBase


class CourseProgressManager(models.Manager):

    def get_course_progress(self, course, user):
        task_count = TaskBase.objects.filter(lesson__course__slug=course.slug).count()
        user_success_count = UserSuccess.objects.filter(
            user=user,
            taskbase__lesson__course__slug=course.slug).count()

        if task_count == 0:
            return 0
        return user_success_count / task_count
