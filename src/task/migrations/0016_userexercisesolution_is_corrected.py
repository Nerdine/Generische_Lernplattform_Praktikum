# Generated by Django 2.0.1 on 2018-03-01 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0015_auto_20180202_1707'),
    ]

    operations = [
        migrations.AddField(
            model_name='userexercisesolution',
            name='is_corrected',
            field=models.BooleanField(default='False', verbose_name='IsCorrected'),
        ),
    ]
