# Generated by Django 2.0.1 on 2018-03-26 09:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0018_programmingexercise_language'),
    ]

    operations = [
        migrations.AlterField(
            model_name='programmingexercise',
            name='language',
            field=models.TextField(choices=[('python', 'Python')], verbose_name='Programming language'),
        ),
    ]
