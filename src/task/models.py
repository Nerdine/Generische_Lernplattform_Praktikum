import itertools

from django.conf import settings
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from martor.models import MartorField

User = get_user_model()


class TaskBase(models.Model):
    # name of the abstract Task
    name = models.CharField(_('Name'), max_length=80)
    # the position of the task amongst the other tasks
    position = models.PositiveIntegerField(_('Position'))

    slug = models.SlugField(max_length=140, unique=True)

    derived_type = models.CharField(_('Derived Type'), max_length=50)

    lesson = models.ForeignKey('course.Lesson', related_name='tasks', on_delete=models.CASCADE)

    type = 'other'

    def save(self, *args, **kwargs):
        if not self.id or not self.slug:
            self.slug = orig = slugify(self.name)

            for x in itertools.count(1):
                if not TaskBase.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        if not self.derived_type:
            self.derived_type = self.type\

        if self.position is None:
            last_task = TaskBase.objects.filter(
                lesson_id=self.lesson.id).order_by('-position').first()
            if last_task is None:
                self.position = 0
            else:
                self.position = last_task.position + 1

        super(TaskBase, self).save(*args, **kwargs)

    def __str__(self):
        return '{}'.format(self.name)


class Exercise(TaskBase):
    # content / question of the exercise
    task = MartorField(_('Task'))
    # possible sample solution of the exercise
    solution = MartorField(_('Solution'))

    type = 'exercise'


def get_programming_languages():
    CHOICES = settings.UNLOCKED_PROGRAMMING_LANGUAGES
    return CHOICES


class ProgrammingExercise(TaskBase):
    # content of the programming exercise
    task = models.TextField(_('Task'))

    language = models.TextField(_("Programming language"), choices=get_programming_languages())

    type = 'programmingexercise'


class UserExerciseSolution(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE)
    solution = MartorField(_('Solution'))
    is_correct_answered = models.BooleanField(_("IsCorrectAnswered"))
    is_corrected = models.BooleanField(_("IsCorrected"), default='False')

    type = 'exercise_solution'

    class Meta:
        unique_together = ('user', 'exercise',)

    def get_solution(self):
        return self.solution


class Text(TaskBase):
    # content of the text
    content = MartorField(_('Content'))

    type = 'text'


class Video(TaskBase):
    upload = models.FileField(upload_to='uploads/videos/')

    type = 'video'


class Quiz(TaskBase):

    def get_questions(self):
        return Question.objects.filter(quiz=self.id).order_by('position')

    type = 'quiz'


class Question(models.Model):

    QUESTION_TYPE = (
        ('0', 'Single-Answer'),
        ('1', 'Multiple-Answer'),
    )

    slug = models.SlugField(max_length=140, unique=True)

    text = models.TextField(_("Text"))
    position = models.PositiveIntegerField(_("Position"))
    type = models.CharField(_("Type"), max_length=1, choices=QUESTION_TYPE)

    quiz = models.ForeignKey(Quiz, related_name='questions',
                             verbose_name=_('Quiz'), on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.text)

    def get_answers(self):
        return Answer.objects.filter(question=self.id).order_by('position')

    def save(self, *args, **kwargs):
        if not self.id or not self.slug:
            self.slug = orig = slugify(self.text)

            for x in itertools.count(1):
                if not Question.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        if self.position is None:
            last_question = Question.objects.filter(
                quiz_id=self.quiz.id).order_by('-position').first()
            if last_question is None:
                self.position = 0
            else:
                self.position = last_question.position + 1

        super(Question, self).save(*args, **kwargs)


class Answer(models.Model):

    slug = models.SlugField(max_length=140, unique=True)

    text = models.TextField(_("Text"))
    is_correct = models.BooleanField(_("IsCorrect"))
    position = models.PositiveIntegerField(_("Position"))

    question = models.ForeignKey(
        Question,
        related_name='answers',
        verbose_name=_('Question'),
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{}'.format(self.text)

    def save(self, *args, **kwargs):
        if not self.id or not self.slug:
            self.slug = orig = slugify(self.text)

            for x in itertools.count(1):
                if not Answer.objects.filter(slug=self.slug).exists():
                    break
                self.slug = '%s-%d' % (orig, x)

        if self.position is None:
            last_answer = Answer.objects.filter(
                question_id=self.question.id).order_by('-position').first()
            if last_answer is None:
                self.position = 0
            else:
                self.position = last_answer.position + 1

        super(Answer, self).save(*args, **kwargs)

        # updating type field in question
        foundOneCorrectAnswer = False
        type = Question.QUESTION_TYPE[0][0]  # single answer
        for answer in self.question.get_answers():
            if answer.is_correct:
                if foundOneCorrectAnswer:
                    type = Question.QUESTION_TYPE[1][0]  # multi answer
                    break
                foundOneCorrectAnswer = True
        Question.objects.filter(id=self.question.id).update(type=type)


class UserAnswerSuccess(models.Model):
    quiz = models.ForeignKey(
        Quiz,
        related_name='%(class)s_quiz',
        verbose_name='Quiz',
        on_delete=models.CASCADE
    )
    answer = models.ForeignKey(
        Answer,
        related_name='%(class)s_answers',
        verbose_name='Answer',
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        User,
        related_name='%(class)s_users',
        verbose_name='User',
        on_delete=models.CASCADE
    )

    is_correct_answered = models.BooleanField(_("IsCorrectAnswered"))

    def __str__(self):
        return '{} {} {} {}'.format(self.quiz, self.answer, self.user, self.is_correct_answered)


class UserSuccess(models.Model):
    taskbase = models.ForeignKey(
        TaskBase, related_name='usersuccess', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='usersuccess', on_delete=models.CASCADE)
    successful = models.BooleanField(_("Successful"), default=False)

    def __str__(self):
        return '{} {} {}'.format(self.taskbase, self.user, self.successful)

    class Meta:
        unique_together = ("taskbase", "user")
