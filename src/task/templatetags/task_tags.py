from django import template

register = template.Library()


@register.simple_tag
def get_answer_validation(answer, user_answers):
    for user_answer in user_answers:
        if answer.id == user_answer.answer.id:
            if user_answer.is_correct_answered:
                return 'is-valid'
            else:
                return 'is-invalid'
    return ''


@register.simple_tag
def get_user_answer(answer, user_answers):
    for user_answer in user_answers:
        if answer.id == user_answer.answer.id:
            if answer.is_correct is True and user_answer.is_correct_answered is True:
                return 'checked=\'checked\''
            if answer.is_correct is False and user_answer.is_correct_answered is False:
                return 'checked=\'checked\''
            return ''
    return ''
