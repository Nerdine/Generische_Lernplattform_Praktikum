import tempfile

from django.test import TestCase
from django.urls import reverse

from authentication.models import User, Event, Specialty
from course.models import Course, Participant, Instructor, Lesson
from prerequisite.models import Category, Prerequisite
from task.models import Text
from ...testing_utilities import populate_test_db, login_client_user, get_user_data, \
    populate_test_db_2, login_client_user_2, TEST_USER_DATA2, populate_instructor_test_db

COURSE_TAG = '<div class="course card text-center">'
COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'


class CoursesViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db_2()
        self.user = User.objects.get(email=TEST_USER_DATA2['email'])

    def test_course_overview_without_course(self):
        login_client_user_2(self)
        response = self.client.get(reverse('administration:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/courses.html')
        self.assertNotContains(response, COURSE_TAG)
        self.assertContains(response, COURSE_NOT_AVAILABLE, count=1)

    def test_course_overview_with_two_courses(self):
        login_client_user_2(self)
        course_a = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        course_b = Course.objects.create(
            name='CourseB',
            short_description='ShortDescriptionB',
            long_description='LongDescriptionB',
            course_language='en',
            effort=2,
            published=False
        )

        Instructor.objects.create(user=self.user, course=course_a)
        Instructor.objects.create(user=self.user, course=course_b)

        response = self.client.get(reverse('administration:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/courses.html')
        self.assertContains(response, COURSE_TAG, count=2)
        self.assertNotContains(response, COURSE_NOT_AVAILABLE, html=True)

    def test_course_overview_with_not_published_course(self):
        login_client_user_2(self)
        Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=False
        )
        response = self.client.get(reverse('administration:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/courses.html')
        self.assertNotContains(response, COURSE_TAG, html=True)
        self.assertContains(response, COURSE_NOT_AVAILABLE, count=1, html=True)

    def test_authentication(self):
        response = self.client.get(reverse('administration:courses'))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse('administration:courses'))
        self.assertEquals(response.status_code, 302)


class CourseDetailViewTest(TestCase):

    def setUp(self):
        populate_test_db()
        self.user = User.objects.get(email=get_user_data()['email'])

    def test_authentication(self):
        response = self.client.get(
            reverse('administration:course_detail', kwargs={'course_slug': 'test'})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('administration:course_detail', kwargs={'course_slug': 'test'})
        )
        self.assertEquals(response.status_code, 302)

    def test_course_detail_get(self):
        login_client_user(self)

        cat = Category.objects.create(type="CSS")
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        prereq = Prerequisite.objects.create(skill_level='1', category=cat)

        course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        course.categories.add(cat)
        course.prerequisites.add(prereq)
        self.instructor = Instructor.objects.create(user=self.user, course=course)

        response = self.client.get(reverse('administration:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        self.assertContains(response, 'Novice - CSS', count=1)
        self.assertContains(response, 'CSS', count=2)


class CourseCreateViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        populate_test_db_2()

    def test_authentication(self):
        login_client_user(self)
        response = self.client.get(reverse('administration:course_create'))
        self.assertEquals(response.status_code, 302)

    def test_course_create_get(self):
        login_client_user_2(self)
        response = self.client.get(reverse('administration:course_create'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/create.html')
        self.assertContains(response, 'Create course', count=1)

    def test_course_create_published_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse('administration:course_create'),
            {
                'name': 'CourseA',
                'short_description': 'ShortDescriptionA',
                'long_description': 'LongDescriptionA',
                'course_language': 'en',
                'effort': '1',
                'published': 'on',
                'instructors': 'john2'
            },
            follow=True
        )

        self.assertRedirects(response, reverse('administration:course_detail',
                                               kwargs={'course_slug': 'coursea'}))
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # title is counted twice: once in messages and once in the overview list
        self.assertContains(response, 'CourseA', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'CourseA created successfully')

    def test_course_create_not_published_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse('administration:course_create'),
            {
                'name': 'CourseA',
                'short_description': 'ShortDescriptionA',
                'long_description': 'LongDescriptionA',
                'course_language': 'en',
                'effort': '1',
                'instructors': 'john2'
            },
            follow=True
        )

        self.assertRedirects(response, reverse('administration:course_detail',
                                               kwargs={'course_slug': 'coursea'}))
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # once in the messages and once in the title
        self.assertContains(response, 'CourseA', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'CourseA created successfully')


class CourseUpdateViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db_2()
        self.instructor_user = populate_instructor_test_db()
        self.course_a = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_b = Course.objects.create(
            name='CourseB',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.instructor = Instructor.objects.create(user=self.user, course=self.course_b)

    def test_authentication(self):
        response = self.client.get(
            reverse('administration:course_update', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('administration:course_update', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)

    def test_no_instructor(self):
        login_client_user_2(self)
        response = self.client.get(
            reverse('administration:course_update', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('administration:course_update', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)

    def test_be_instructor(self):
        login_client_user_2(self)
        response = self.client.get(
            reverse('administration:course_update', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)
        response = self.client.post(
            reverse('administration:course_update', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)

    def test_course_update_get(self):
        login_client_user_2(self)
        response = self.client.get(
            reverse('administration:course_update', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/update.html')
        self.assertContains(response, 'Update course', count=1)

    def test_course_update_change_name_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse('administration:course_update', kwargs={'course_slug': 'courseb'}),
            {
                'name': 'CourseChangedName',
                'short_description': 'ShortDescription',
                'long_description': 'LongDescription',
                'course_language': 'en',
                'effort': '1',
                'published': 'on',
                'instructors': self.user.username
            },
            follow=True
        )

        self.assertRedirects(response, reverse('administration:course_detail',
                                               kwargs={'course_slug': 'courseb'}))
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # title is counted twice: once in messages and once in the overview list
        self.assertContains(response, 'CourseChangedName', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'CourseChangedName updated successfully')

    def test_course_update_remove_published_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse('administration:course_update', kwargs={'course_slug': 'courseb'}),
            {
                'name': 'CourseB',
                'short_description': 'ShortDescription',
                'long_description': 'LongDescription',
                'course_language': 'en',
                'effort': '1',
                'instructors': self.user.username
            },
            follow=True
        )

        self.assertRedirects(response, reverse('administration:course_detail',
                                               kwargs={'course_slug': 'courseb'}))
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # title of course is counted twice: once in messages, once in list
        self.assertContains(response, 'CourseB', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'CourseB updated successfully')

    def test_course_update_change_instructor(self):
        login_client_user_2(self)
        self.client.post(
            reverse('administration:course_update', kwargs={'course_slug': 'courseb'}),
            {
                'name': 'CourseB',
                'short_description': 'ShortDescription',
                'long_description': 'LongDescription',
                'course_language': 'en',
                'effort': '1',
                'instructors': self.instructor_user.username
            }
        )

        self.assertIsNotNone(Instructor.objects.filter(course=self.course_b,
                                                       user=self.instructor_user).first())


class CourseInstructorView(TestCase):

    def setUp(self):
        user1 = populate_test_db()
        user2 = populate_test_db_2()

        self.courseA = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )

        self.courseB = Course.objects.create(
            name='CourseB',
            short_description='CourseB_Short_Description',
            long_description='LongDescription',
            course_language='de',
            effort=1,
            published=True
        )

        self.courseC = Course.objects.create(
            name='CourseC',
            short_description='CourseC_Short_Description',
            long_description='LongDescription',
            course_language='de',
            effort=1,
            published=True
        )

        self.participant = Participant.objects.create(user=user1, course=self.courseA, rating=0)

        self.instructor = Instructor.objects.create(user=user1, course=self.courseA)
        self.instructor = Instructor.objects.create(user=user1, course=self.courseB)

        self.eventA = Event.objects.create(
            start_date='2009-08-14',
            end_date='2017-08-12',
            institution='Siemens',
            position='Entwicklungsingenieur',
            user=user1
        )

        self.eventB = Event.objects.create(
            start_date='2002-12-09',
            end_date='2009-08-04',
            institution='FAU',
            position='Student',
            user=user1
        )

        self.eventC = Event.objects.create(
            start_date='2011-01-19',
            end_date='2013-09-14',
            institution='Aerodynamics',
            position='Vertriebsleiter',
            user=user2
        )

        self.specialtyA = Specialty.objects.create(
            title='CSS',
            user=user1,
            knowledge_level='1'
        )

        self.specialtyB = Specialty.objects.create(
            title='PHP',
            user=user1,
            knowledge_level='1'
        )

        self.specialtyC = Specialty.objects.create(
            title='Python',
            user=user2,
            knowledge_level='2'
        )

    def test_authentication(self):
        response = self.client.get(
            reverse('administration:instructor_detail', kwargs={
                'course_slug': 'coursea', 'instructor_slug': 'john'})
        )
        self.assertEquals(response.status_code, 302)

    # Checks content of response to get request for logged in user
    def test_existing_instructor_detail_get(self):
        login_client_user(self)
        response = self.client.get(
            reverse('administration:instructor_detail', kwargs={
                'course_slug': 'coursea', 'instructor_slug': 'john'})
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/instructor_detail.html')
        self.assertContains(response, 'courseb', count=1)
        self.assertNotContains(response, 'coursec')

        self.assertContains(response, 'Entwicklungsingenieur', count=1)
        self.assertContains(response, 'Student', count=1)
        self.assertNotContains(response, 'Vertriebsleiter')

        self.assertContains(response, 'PHP', count=1)
        self.assertContains(response, 'CSS', count=1)
        self.assertNotContains(response, 'Python')


class CoursePublishUnpublishView(TestCase):

    def setUp(self):
        populate_test_db_2()
        self.user = User.objects.get(email=TEST_USER_DATA2['email'])

    def __create_courses(self):
        login_client_user_2(self)
        self.course_en_published = Course.objects.create(
            name='CourseEnPublished',
            short_description='ShortEnPublished',
            long_description='LongEnPublished',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_en_not_published = Course.objects.create(
            name='CourseEnNotPublished',
            short_description='ShortEnNotPublished',
            long_description='LongEnNotPublished',
            course_language='en',
            effort=2,
            published=False
        )
        self.course_de_published = Course.objects.create(
            name='CourseDePublished',
            short_description='ShortDePublished',
            long_description='LongDePublished',
            course_language='de',
            effort=2,
            published=True
        )
        Participant.objects.create(course=self.course_de_published, user=self.user, rating=0)

    def __become_instructor_for_course(self, course):
        login_client_user(self)
        Instructor.objects.create(
            user=self.user,
            course=course
        )

    def test_publish_course(self):
        self.__create_courses()
        self.__become_instructor_for_course(self.course_en_not_published)
        course_slug = self.course_en_not_published.slug
        response = self.client.post(reverse('administration:course_publish', kwargs={
            'course_slug': course_slug}), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/courses.html')
        course = Course.objects.filter(
            slug=self.course_en_not_published.slug, published=True).first()
        self.assertIsNotNone(course)
        self.__assert_one_message(
            response, 'The course CourseEnNotPublished was successfully published')

    def test_unpublish_course(self):
        self.__create_courses()
        self.__become_instructor_for_course(self.course_en_published)
        course_slug = self.course_en_published.slug
        response = self.client.post(reverse('administration:course_publish', kwargs={
            'course_slug': course_slug}), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/courses.html')
        course = Course.objects.filter(slug=self.course_en_published.slug, published=False).first()
        self.assertIsNotNone(course)
        self.__assert_one_message(
            response, 'The course CourseEnPublished was successfully unpublished')

    def __assert_one_message(self, response, expected_message):
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]), expected_message)


class CourseUpdateStructureViewTest(TestCase):

    def test(self):
        self.user = populate_test_db()
        login_client_user(self)
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )

        self.lesson_a = Lesson.objects.create(
            course=self.course, name='0', position=0)
        self.lesson_b = Lesson.objects.create(
            course=self.course, name='1', position=1)

        Text.objects.create(name="1", position=0, content="1", lesson=self.lesson_a)
        Text.objects.create(name="2", position=1, content="2", lesson=self.lesson_a)
        Text.objects.create(name="3", position=0, content="3", lesson=self.lesson_b)

        self.instructor = Instructor.objects.create(user=self.user, course=self.course)

        content = """[
                     [
                      {
                       "id": 2,
                       "type": "lesson",
                       "children": [
                        [
                         {
                          "id": 3,
                          "type": "task"
                         },
                         {
                          "id": 2,
                          "type": "task"
                         }
                        ]
                       ]
                      },
                      {
                       "id": 1,
                       "type": "lesson",
                       "children": [
                        [
                         {
                          "id": 1,
                          "type": "task"
                         }
                        ]
                       ]
                      }
                     ]
                    ]"""

        response = self.client.post(reverse('administration:course_update_structure', kwargs={
            'course_slug': self.course.slug}),  {
                'structure': content,
        }, follow=True)
        self.assertEquals(response.status_code, 200)

        self.assertEqual(Lesson.objects.get(name="1").position, 0)
        self.assertEqual(Lesson.objects.get(name="0").position, 1)

        self.assertEqual(Text.objects.get(name="1").position, 0)
        self.assertEqual(Text.objects.get(name="2").position, 1)
        self.assertEqual(Text.objects.get(name="3").position, 0)
