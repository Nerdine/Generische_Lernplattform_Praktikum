from django.test import TestCase
from django.urls import reverse

from course.models import Course, Lesson, Instructor
from ...testing_utilities import populate_test_db, login_client_user

COURSE_TAG = '<div class="course">'
COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'


class LessonCreateViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        self.course_a = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_b = Course.objects.create(
            name='CourseB',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.instructor = Instructor.objects.create(user=self.user, course=self.course_b)

    def test_authentication(self):
        response = self.client.get(
            reverse('administration:lesson_create', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('administration:lesson_create', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)

    def test_no_instructor(self):
        login_client_user(self)
        response = self.client.get(
            reverse('administration:lesson_create', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('administration:lesson_create', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)

    def test_be_instructor(self):
        login_client_user(self)
        response = self.client.get(
            reverse('administration:lesson_create', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)
        response = self.client.post(
            reverse('administration:lesson_create', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)

    def test_lesson_create_get(self):
        login_client_user(self)
        response = self.client.get(
            reverse('administration:lesson_create', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/lesson/create.html')
        self.assertContains(response, 'Create lesson', count=1)

    def test_lesson_create_post(self):
        login_client_user(self)
        response = self.client.post(
            reverse('administration:lesson_create', kwargs={'course_slug': 'courseb'}),
            {
                'name': 'NewLesson',
                'description': 'Description',
                'position': '1'
            },
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('administration:course_detail', kwargs={'course_slug': 'courseb'})
        )
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # name of lesson is counted twice: once in messages and once in the lesson list
        self.assertContains(response, 'NewLesson', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'NewLesson created successfully')


class LessonUpdateViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        self.course_a = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_b = Course.objects.create(
            name='CourseB',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.instructor = Instructor.objects.create(user=self.user, course=self.course_b)
        self.lesson_a = Lesson.objects.create(
            name='LessonA',
            position=1,
            course=self.course_a
        )
        self.lesson_b = Lesson.objects.create(
            name='LessonB',
            position=1,
            course=self.course_b
        )

    def test_authentication(self):
        response = self.client.get(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)

    def test_no_instructor(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)

    def test_be_instructor(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
        ))
        self.assertEquals(response.status_code, 200)
        response = self.client.post(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
        ))
        self.assertEquals(response.status_code, 200)

    def test_lesson_update_get(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'administration:lesson_update',
            kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
        ))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/lesson/update.html')
        self.assertContains(response, 'Update lesson', count=1)

    def test_lesson_update_change_name_post(self):
        login_client_user(self)
        response = self.client.post(
            reverse(
                'administration:lesson_update',
                kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
            ),
            {
                'name': 'ChangedNameLessonB',
                'description': 'DescriptionLessonB',
                'position': '1'
            },
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('administration:course_detail', kwargs={'course_slug': 'courseb'})
        )
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # name of lesson is counted twice: once in messages and once in the lesson list
        self.assertContains(response, 'ChangedNameLessonB', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'ChangedNameLessonB updated successfully')


class LessonDeleteViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        self.course_a = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_b = Course.objects.create(
            name='CourseB',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.instructor = Instructor.objects.create(user=self.user, course=self.course_b)
        self.lesson_a = Lesson.objects.create(
            name='LessonA',
            position=1,
            course=self.course_a
        )
        self.lesson_b = Lesson.objects.create(
            name='LessonB',
            position=1,
            course=self.course_b
        )

    def test_authentication(self):
        response = self.client.get(reverse(
            'administration:lesson_delete',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse(
            'administration:lesson_delete',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)

    def test_no_instructor(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'administration:lesson_delete',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse(
            'administration:lesson_delete',
            kwargs={'course_slug': 'coursea', 'lesson_slug': 'lessona'}
        ))
        self.assertEquals(response.status_code, 302)

    def test_be_instructor(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'administration:lesson_delete',
            kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
        ))
        self.assertEquals(response.status_code, 200)
        response = self.client.post(
            reverse(
                'administration:lesson_delete',
                kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
            ),
            follow=True
        )
        self.assertEquals(response.status_code, 200)

    def test_lesson_delete_get(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'administration:lesson_delete',
            kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
        ))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/lesson/delete.html')
        self.assertContains(
            response,
            'Are you sure you want to delete "LessonB"?',
            count=1
        )

    def test_lesson_delete_post(self):
        login_client_user(self)
        response = self.client.post(
            reverse(
                'administration:lesson_delete',
                kwargs={'course_slug': 'courseb', 'lesson_slug': 'lessonb'}
            ),
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('administration:course_detail', kwargs={'course_slug': 'courseb'})
        )
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        # name of lesson is counted once in messages
        self.assertContains(response, 'LessonB', count=1)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'LessonB deleted successfully')
