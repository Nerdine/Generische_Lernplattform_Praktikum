from django.test import TestCase
from django.urls import reverse

from course.models import Course, Instructor, Lesson
from task.models import Exercise, UserExerciseSolution
from ...testing_utilities import populate_test_db, populate_test_db_2, login_client_user


class SolutionDetailTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        self.user_student = populate_test_db_2()
        self.course_a = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_b = Course.objects.create(
            name='CourseB',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course_b)
        self.exercise = Exercise.objects.create(
            name='ExName', position=42, lesson=self.lesson, task="**Foo**", solution="**Bar**")
        self.user_solution = UserExerciseSolution.objects.create(
            user=self.user_student, exercise=self.exercise, solution="**Spritzig**",
            is_correct_answered=False, is_corrected=False)
        self.instructor = Instructor.objects.create(user=self.user, course=self.course_b)

    def test_no_instructor(self):
        login_client_user(self)
        response = self.client.get(
            reverse('administration:solution_detail', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)
        login_client_user(self)
        response = self.client.get(
            reverse('administration:solution_detail', kwargs={'course_slug': 'coursea'})
        )
        self.assertEquals(response.status_code, 302)

    def test_be_instructor_course_with_exercises(self):
        login_client_user(self)
        response = self.client.get(
            reverse('administration:solution_detail', kwargs={'course_slug': 'courseb'})
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/solution/detail.html')
        self.assertContains(response, 'ExName', count=2)
