from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from course.models import Course, Lesson, Instructor
from task.models import Text, Video, Quiz, Exercise, Question, Answer, ProgrammingExercise
from ...testing_utilities import populate_test_db, login_client_user


def set_up_infrastructure(self):
    self.user = populate_test_db()

    self.course = Course.objects.create(
        name='CourseA',
        short_description='ShortDescription',
        long_description='LongDescription',
        course_language='en',
        effort=1,
        published=True
    )
    self.lesson = Lesson.objects.create(name='Name', position=0, course=self.course)


class TaskDeleteViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)
        self.text = Text.objects.create(name='TextName', position=1,
                                        lesson=self.lesson, content='Content')
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)

    def test_delete_post(self):
        response = self.client.post(reverse('administration:task_delete', kwargs={
            'task_slug': self.text.slug, 'course_slug': self.course.slug}),
            follow=True)
        self.assertEquals(response.status_code, 200)
        actual_text = Text.objects.filter(name='TextName').first()
        self.assertIsNone(actual_text)


class TextTaskCreateViewTest(TestCase):
    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)

    def test_create(self):
        response = self.client.post(
            reverse('administration:text_create',
                    kwargs={
                        'course_slug': self.course.slug,
                        'lesson_slug': self.lesson.slug}),
            {'name': "TextName",
             'lesson': self.lesson.id,
             'content': "**Foo**"},
            follow=True)
        self.assertEquals(response.status_code, 200)
        actual_text = Text.objects.filter(name='TextName').first()
        self.assertIsNotNone(actual_text)
        self.assertEqual(actual_text.content, '**Foo**')


class TextTaskUpdateViewTest(TestCase):
    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)
        self.text = Text.objects.create(name='TextName', position=1,
                                        lesson=self.lesson, content='Content')

    def test_update_get(self):
        response = self.client.get(reverse('administration:text_update', kwargs={
            'task_slug': self.text.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/text/text_update.html')
        self.assertContains(response, 'TextName', count=1)
        self.assertContains(response, 'Content', count=2)

    def test_update_post(self):
        response = self.client.post(reverse('administration:text_update', kwargs={
            'task_slug': self.text.slug, 'course_slug': self.course.slug}),
            {'name': "Foo",
             'lesson': self.lesson.id,
             'content': "**Foo**"},
            follow=True)

        self.assertEquals(response.status_code, 200)
        actual_text = Text.objects.filter(name='Foo').first()
        self.assertIsNotNone(actual_text)
        self.assertEqual(actual_text.name, 'Foo')
        self.assertEqual(actual_text.position, 1)
        self.assertEqual(actual_text.content, '**Foo**')


class ExerciseTaskCreateViewTest(TestCase):
    def setUp(self):
        self.user = populate_test_db()

        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)

    def test_create(self):
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)

        response = self.client.post(
            reverse('administration:exercise_create',
                    kwargs={
                        'course_slug': self.course.slug,
                        'lesson_slug': self.lesson.slug}),
            {'name': "ExerciseName",
             'lesson': self.lesson.id,
             'task': "**Foo**",
             'solution': "**Bar**"},
            follow=True)
        self.assertEquals(response.status_code, 200)
        actual_exercise = Exercise.objects.filter(name='ExerciseName').first()
        self.assertIsNotNone(actual_exercise)
        self.assertEqual(actual_exercise.task, '**Foo**')
        self.assertEqual(actual_exercise.solution, '**Bar**')


class ExerciseTaskUpdateViewTest(TestCase):
    def setUp(self):
        self.user = populate_test_db()

        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)

        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)
        self.exercise = Exercise.objects.create(name='ExerciseName', position=1,
                                                lesson=self.lesson, task="Task",
                                                solution='Solution')

    def test_update_get(self):
        response = self.client.get(reverse('administration:exercise_update', kwargs={
            'task_slug': self.exercise.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/exercise/exercise_update.html')
        self.assertContains(response, 'ExerciseName', count=1)
        self.assertContains(response, 'Task', count=2)
        self.assertContains(response, 'Solution', count=2)

    def test_update_post(self):
        response = self.client.post(reverse('administration:exercise_update', kwargs={
            'task_slug': self.exercise.slug, 'course_slug': self.course.slug}),
            {'name': "Foo",
             'lesson': self.lesson.id,
             'task': "**Foo**",
             'solution': "**Bar**"},
            follow=True)

        self.assertEquals(response.status_code, 200)
        actual_exercise = Exercise.objects.filter(name='Foo').first()
        self.assertIsNotNone(actual_exercise)
        self.assertEqual(actual_exercise.name, 'Foo')
        self.assertEqual(actual_exercise.position, 1)
        self.assertEqual(actual_exercise.task, '**Foo**')
        self.assertEqual(actual_exercise.solution, '**Bar**')


class VideoTaskCreateViewTest(TestCase):
    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)

    def test_create_get(self):
        response = self.client.get(reverse('administration:video_create',
                                           kwargs={'course_slug': self.course.slug,
                                                   'lesson_slug': self.lesson.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/video/video_create.html')

    def test_create_post(self):
        video = SimpleUploadedFile("foo.mp4", b"file_content", content_type="video/mp4")
        response = self.client.post(
            reverse('administration:video_create', kwargs={'course_slug': self.course.slug,
                                                           'lesson_slug': self.lesson.slug}),
            {'name': "Foo",
             'lesson': self.lesson.id,
             'upload': video},
            follow=True)

        self.assertEquals(response.status_code, 200)
        actual_text = Video.objects.filter(name='Foo').first()
        self.assertIsNotNone(actual_text)
        self.assertEqual(actual_text.name, 'Foo')
        self.assertEqual(actual_text.position, 0)


class VideoTaskUpdateViewTest(TestCase):
    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)
        self.video = Video.objects.create(name='VideoName', position=1,
                                          lesson=self.lesson, upload='<dummy>')

    def test_update_get(self):
        response = self.client.get(reverse('administration:video_update', kwargs={
            'task_slug': self.video.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/video/video_update.html')

    def test_update_post(self):
        video = SimpleUploadedFile("foo.mp4", b"file_content", content_type="video/mp4")
        response = self.client.post(reverse('administration:video_update', kwargs={
            'task_slug': self.video.slug, 'course_slug': self.course.slug}),
            {'name': "Foo",
             'lesson': self.lesson.id,
             'upload': video},
            follow=True)

        self.assertEquals(response.status_code, 200)
        actual_text = Video.objects.filter(name='Foo').first()
        self.assertIsNotNone(actual_text)
        self.assertEqual(actual_text.name, 'Foo')
        self.assertEqual(actual_text.position, 1)


class TaskAdminViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)

    def test_video_admin_view(self):
        video = Video.objects.create(name='VideoName', position=1, lesson=self.lesson,
                                     upload='<dummy>')
        response = self.client.get(reverse('administration:video_admin_detail', kwargs={
            'task_slug': video.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/video/video_admin_detail.html')

    def test_text_admin_view(self):
        text = Text.objects.create(name='TextName', position=1,
                                   lesson=self.lesson, content='Content')
        response = self.client.get(reverse('administration:text_admin_detail', kwargs={
            'task_slug': text.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/text/text_admin_detail.html')

    def test_quiz_admin_view(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        question = Question.objects.create(
            text="My Question", position=99, type='1', quiz=quiz)
        Answer.objects.create(text="My Answer", position=13, is_correct=True, question=question)

        response = self.client.get(reverse('administration:quiz_admin_detail', kwargs={
            'task_slug': quiz.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/quiz/quiz_admin_detail.html')

    def test_exercise_admin_view(self):
        exercise = Exercise.objects.create(name='ExerciseName', position=1,
                                           lesson=self.lesson, task="Task",
                                           solution='Solution')
        response = self.client.get(reverse('administration:exercise_admin_detail', kwargs={
            'task_slug': exercise.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(
            response, 'administration/tasks/exercise/exercise_admin_detail.html')

    def test_programming_exercise_admin_view(self):
        programming_exercise = ProgrammingExercise.objects.create(
            name='Name', position=42, lesson=self.lesson, task='Task')
        response = self.client.get(
            reverse('administration:programmingexercise_admin_detail',
                    kwargs={
                        'task_slug': programming_exercise.slug, 'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(
            response, 'administration/tasks/programmingexercise/exercise_admin_detail.html')


class QuizQuestionAnswerCreateViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user, course=self.course)

    def test_create_quiz_get(self):
        response = self.client.get(
            reverse('administration:quiz_create',
                    kwargs={
                        'lesson_slug': self.lesson.slug,
                        'course_slug': self.course.slug
                    }))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/quiz/quiz_create.html')

    def test_create_quiz_post(self):
        response = self.client.post(
            reverse('administration:quiz_create',
                    kwargs={
                        'lesson_slug': self.lesson.slug,
                        'course_slug': self.course.slug
                    }),
            {
                'name': 'fooQuiz'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/quiz/quiz_update.html')
        self.assertIsNotNone(Quiz.objects.filter(name='fooQuiz').first())

    def test_create_question_get(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        response = self.client.get(
            reverse('administration:quiz_question_create',
                    kwargs={
                        'task_slug': quiz.slug,
                        'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_content_create_update.html')

    def test_create_question_post(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        response = self.client.post(
            reverse('administration:quiz_question_create',
                    kwargs={
                        'task_slug': quiz.slug,
                        'course_slug': self.course.slug
                    }),
            {
                'text': 'QuestionFoo'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_update.html')
        self.assertIsNotNone(Question.objects.filter(text="QuestionFoo").first())

    def test_create_answer_get(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        question = Question.objects.create(text="My Question", position=99, type='1', quiz=quiz)
        response = self.client.get(
            reverse('administration:quiz_answer_create',
                    kwargs={
                        'task_slug': quiz.slug,
                        'course_slug': self.course.slug,
                        'question_slug': question.slug
                    }))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_content_create_update.html')

    def test_create_answer_post(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        question = Question.objects.create(text="My Question", position=99, type='1', quiz=quiz)
        response = self.client.post(
            reverse('administration:quiz_answer_create',
                    kwargs={
                        'task_slug': quiz.slug,
                        'course_slug': self.course.slug,
                        'question_slug': question.slug}),
            {
                'text': 'AnswerFoo',
                'is_correct': 'on'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_update.html')
        self.assertIsNotNone(Answer.objects.filter(text="AnswerFoo", is_correct=True).first())


class QuizQuestionAnswerUpdateViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user,
                                  course=self.course)

        self.quiz = Quiz.objects.create(name="QuizName",
                                        position=42,
                                        lesson=self.lesson)

        self.question = Question.objects.create(text="My Question",
                                                position=99,
                                                type='1',
                                                quiz=self.quiz)

        self.answer = Answer.objects.create(text="My Answer",
                                            position=13,
                                            is_correct=True,
                                            question=self.question)

    def test_update_quiz_get(self):
        response = self.client.get(
            reverse('administration:quiz_update',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'course_slug': self.course.slug
                    }))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/tasks/quiz/quiz_update.html')

    def test_update_quiz_post(self):
        response = self.client.post(
            reverse('administration:quiz_update',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'course_slug': self.course.slug
                    }),
            {
                'name': 'fooQuiz'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'administration/course/detail.html')
        self.assertIsNotNone(Quiz.objects.filter(name='fooQuiz').first())

    def test_update_question_get(self):
        response = self.client.get(
            reverse('administration:quiz_question_update',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'question_slug': self.question.slug,
                        'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_content_create_update.html')

    def test_update_question_post(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        response = self.client.post(
            reverse('administration:quiz_question_update',
                    kwargs={
                        'task_slug': quiz.slug,
                        'question_slug': self.question.slug,
                        'course_slug': self.course.slug
                    }),
            {
                'text': 'QuestionFoo'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_update.html')
        self.assertIsNotNone(Question.objects.filter(text="QuestionFoo").first())

    def test_update_answer_get(self):
        response = self.client.get(
            reverse('administration:quiz_answer_update',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'course_slug': self.course.slug,
                        'answer_slug': self.answer.slug
                    }))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_content_create_update.html')

    def test_update_answer_post(self):

        response = self.client.post(
            reverse('administration:quiz_answer_update',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'course_slug': self.course.slug,
                        'answer_slug': self.answer.slug
                    }),
            {
                'text': 'AnswerFoo',
                'is_correct': 'on'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_update.html')
        self.assertIsNotNone(Answer.objects.filter(text="AnswerFoo", is_correct=True).first())


class QuizQuestionAnswerDeleteViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)
        login_client_user(self)
        Instructor.objects.create(user=self.user,
                                  course=self.course)

        self.quiz = Quiz.objects.create(name="QuizName",
                                        position=42,
                                        lesson=self.lesson)

        self.question = Question.objects.create(text="My Question",
                                                position=99,
                                                type='1',
                                                quiz=self.quiz)

        self.answer = Answer.objects.create(text="My Answer",
                                            position=13,
                                            is_correct=True,
                                            question=self.question)

    def test_delete_question_get(self):
        response = self.client.get(
            reverse('administration:quiz_question_delete',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'question_slug': self.question.slug,
                        'course_slug': self.course.slug}))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,              'administration/tasks/task_delete.html')

    def test_delete_question_post(self):
        quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        response = self.client.post(
            reverse('administration:quiz_question_delete',
                    kwargs={
                        'task_slug': quiz.slug,
                        'question_slug': self.question.slug,
                        'course_slug': self.course.slug
                    }),
            {
                'text': 'QuestionFoo'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_update.html')
        self.assertIsNone(Question.objects.filter(text="My Question").first())

    def test_delete_answer_get(self):
        response = self.client.get(
            reverse('administration:quiz_answer_delete',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'course_slug': self.course.slug,
                        'answer_slug': self.answer.slug
                    }))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,          'administration/tasks/task_delete.html')

    def test_delete_answer_post(self):

        response = self.client.post(
            reverse('administration:quiz_answer_delete',
                    kwargs={
                        'task_slug': self.quiz.slug,
                        'course_slug': self.course.slug,
                        'answer_slug': self.answer.slug
                    }),
            {
                'text': 'AnswerFoo',
                'is_correct': 'on'
            }, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response,
                                'administration/tasks/quiz/quiz_update.html')
        self.assertIsNone(Answer.objects.filter(text="My Answer", is_correct=True).first())
