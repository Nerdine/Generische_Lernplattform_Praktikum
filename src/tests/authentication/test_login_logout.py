from django.test import TestCase
from django.urls import reverse
from ..testing_utilities import populate_test_db, get_user_data, login_client_user


class LoginLogoutTests(TestCase):
    def setUp(self):
        populate_test_db()

    def test_login_get(self):
        response = self.client.get(reverse('login'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'authentication/login.html')

    def test_login_already_auth(self):
        login_client_user(self)
        response = self.client.get(reverse('login'))
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)

    def test_home_without_auth(self):
        response = self.client.get(reverse('desk:courses'))
        self.assertRedirects(
            response,
            '/login?next=/desk/courses',
            status_code=302,
            target_status_code=200)

    def test_login_post_correct(self):
        name = get_user_data()['name']
        password = get_user_data()['password']

        response = self.client.post(
            reverse('login'), {
                'username': name,
                'password': password
            })
        self.assertRedirects(
            response,
            '/',
            status_code=302,
            target_status_code=200)

    def test_login_post_wrong_name(self):
        name = "wrong"
        password = get_user_data()['password']

        response = self.client.post(
            reverse('login'), {
                'username': name,
                'password': password
            })
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "Please enter a correct username and password")
        self.assertTemplateUsed(response, 'authentication/login.html')

    def test_login_post_wrong_pw(self):
        name = get_user_data()['name']
        password = "wrong"

        response = self.client.post(
            reverse('login'), {
                'username': name,
                'password': password
            })
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, "Please enter a correct username and password")
        self.assertTemplateUsed(response, 'authentication/login.html')

    def test_logout(self):
        login_client_user(self)
        response = self.client.get(reverse('logout'))
        self.assertRedirects(
            response,
            '/login',
            status_code=302,
            target_status_code=200)

        response = self.client.get(reverse('desk:courses'))
        self.assertRedirects(
            response,
            '/login?next=/desk/courses', status_code=302,
            target_status_code=200)

    def test_active_mail_before_login(self):
        response1 = self.client.post(reverse('auth:register'), {'username': "TestuserLogin",
                                                                'email': "testLogin@testuser.de",
                                                                'password1': "test1234",
                                                                'password2': "test1234"},
                                     follow=True)
        self.assertEquals(response1.status_code, 200)

        response2 = self.client.post(reverse('login'), {'username': "TestuserLogin",
                                                        'password': "test1234"})
        self.assertEquals(response2.status_code, 200)
        self.assertTemplateUsed(
            response=response2, template_name='authentication/login.html')
