from django.test import TestCase
import datetime
from authentication.models import Event, Specialty
from ..testing_utilities import populate_test_db
from django.db import IntegrityError


class SpecialtyTests(TestCase):

    def setUp(self):
        self.user = populate_test_db()

        self.specialty = Specialty.objects.create(
            knowledge_level='2',
            title='PHP',
            user=self.user
        )

    def test_specialty(self):
        actual_specialty = Specialty.objects.get(id=self.specialty.id)
        self.assertEquals(actual_specialty.title, 'PHP')
        self.assertEquals(actual_specialty.knowledge_level, '2')
        self.assertEqual(actual_specialty.user.id, self.user.id)
        self.assertEqual(actual_specialty.user.username, self.user.username)
        self.assertEqual(actual_specialty.slug, 'php')

    def test_duplicated_slugs(self):
        first_specialty = Specialty.objects.create(
            knowledge_level=2,
            title='python',
            user=self.user
        )
        second_specialty = Specialty.objects.create(
            knowledge_level=2,
            title='python',
            user=self.user
        )
        self.assertEqual(first_specialty.slug, 'python')
        self.assertEqual(second_specialty.slug, 'python-1')

    def test_specialty_string_representation(self):
        specialty = Specialty.objects.create(knowledge_level=3, title='css', user=self.user)
        self.assertEqual(str(specialty), 'css')


class EventTests(TestCase):

    def setUp(self):
        self.user = populate_test_db()

        self.event = Event.objects.create(
            start_date=datetime.datetime.now().date(),
            end_date=datetime.datetime.now().date() + datetime.timedelta(days=10),
            institution='FAU',
            position='Lehrer',
            user=self.user
        )

    def test_event(self):
        actual_event = Event.objects.get(id=self.event.id)
        self.assertEquals(actual_event.start_date, datetime.datetime.now().date())
        self.assertEquals(actual_event.end_date, datetime.datetime.now().date() +
                          datetime.timedelta(days=10))
        self.assertEqual(actual_event.institution, 'FAU')
        self.assertEqual(actual_event.position, 'Lehrer')
        self.assertEqual(actual_event.user.id, self.user.id)
        self.assertEqual(actual_event.user.username, self.user.username)
        self.assertEqual(actual_event.slug, 'john-fau-lehrer')

    def test_event_string_representation(self):
        event = Event.objects.create(
            start_date=datetime.datetime.now().date(),
            end_date=datetime.datetime.now().date() + datetime.timedelta(days=10),
            institution='Siemens',
            position='Forscher',
            user=self.user)

        self.assertEqual(str(event), 'john - Siemens - Forscher')

    def test_unique_constraint(self):
        first_event = Event.objects.create(
            start_date=datetime.datetime.now().date(),
            end_date=datetime.datetime.now().date() + datetime.timedelta(days=10),
            institution='BMW',
            position='Entwicklungsingenieur',
            user=self.user)
        self.assertEqual(first_event.slug, 'john-bmw-entwicklungsingenieur')
        data = dict(user=self.user, institution='BMW', position='Entwicklungsingenieur')

        second_event = Event(**data)

        with self.assertRaises(Exception) as raised:
            second_event.save()
        self.assertEqual(IntegrityError, type(raised.exception))
