import re
from django.test import TestCase
from django.urls import reverse
from django.core import mail
from django.contrib.auth import get_user_model
from ..testing_utilities import populate_test_db, get_user_data

User = get_user_model()


class PasswordResetTests(TestCase):
    def setUp(self):
        populate_test_db()

    def test_password_reset_get(self):
        response = self.client.get(reverse('auth:password_reset'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'authentication/password_reset_form.html')

    def test_password_reset_done_get(self):
        response = self.client.get(reverse('auth:password_reset_done'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'authentication/password_reset_done.html')

    def test_password_reset_complete_get(self):
        response = self.client.get(reverse('auth:password_reset_complete'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'authentication/password_reset_complete.html')

    def test_password_reset_post(self):
        email = get_user_data()['email']
        response = self.client.post(
            reverse('auth:password_reset'), {
                'email': email
            })

        self.assertRedirects(response, reverse('auth:password_reset_done'))
        self.assertEqual(len(mail.outbox), 1)

    def test_password_reset_with_invalid_email(self):
        response = self.client.post(reverse('auth:password_reset'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'authentication/password_reset_form.html')
        self.assertEqual(len(mail.outbox), 0)

    def test_password_reset_confirm_post(self):
        email = get_user_data()['email']

        # send reset email
        self.client.post(reverse('auth:password_reset'), {'email': email})
        self.assertEqual(len(mail.outbox), 1)

        # extract the token url
        url_match = re.search(r"https?://[^/]*(/.*reset/\S*)", mail.outbox[0].body)
        self.assertIsNotNone(url_match, "No URL found in sent email")
        path = url_match.groups()[0]

        # set the new password
        new_password = 'Yu1#du4s'
        response = self.client.get(path)
        response = self.client.post(response.url, {
            'new_password1': new_password,
            'new_password2': new_password
        })

        # assert
        self.assertRedirects(response, reverse('auth:password_reset_complete'))
        u = User.objects.get(email=email)
        self.assertTrue(u.check_password(new_password))
