from django.test import TestCase
from django.urls import reverse
from ..testing_utilities import populate_test_db, login_client_user
from django.contrib.auth import get_user_model
from authentication import forms
import sys

sys.path.append('../..')

User = get_user_model()


class RegisterTests(TestCase):
    def setUp(self):
        populate_test_db()

    def test_register_get(self):
        response = self.client.get(reverse('auth:register'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'authentication/register.html')

    # Logged-in person tries to register
    def test_register_already_auth(self):
        login_client_user(self)
        response = self.client.get(reverse('auth:register'))
        self.assertRedirects(response, '/', status_code=302, target_status_code=200)

    # Check if correct form is used
    def test_registration_uses_form(self):
        response = self.client.get(reverse('auth:register'))
        self.assertIsInstance(response.context['form'], forms.RegisterForm)

    def test_user_creation_at_registration_works_with_too_short_password(self):
        response = self.client.post(reverse('auth:register'), {'username': "TestuserSP",
                                                               'email': "test@testuser.de",
                                                               'password': "te45"}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(User.objects.filter(username="TestuserSP")), 0)

    def test_user_creation_at_registration_works_with_too_similar_password(self):
        response = self.client.post(reverse('auth:register'), {'username': "TestuserSimP",
                                                               'email': "test@testuser.de",
                                                               'password': "testtest"},
                                    follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(User.objects.filter(username="TestuserSimP")), 0)

    def test_user_creation_at_registration_works(self):
        response = self.client.post(reverse('auth:register'), {'username': "Testuser",
                                                               'email': "test@testuser.de",
                                                               'password1': "test2222",
                                                               'password2': "test2222"},
                                    follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(User.objects.filter(username="Testuser")), 1)

    def test_registration_redirects_to_login(self):
        response = self.client.post(reverse('auth:register'), {'username': "Testuser2",
                                                               'email': "test@testuser.de",
                                                               'password1': "test1234",
                                                               'password2': "test1234"},
                                    follow=True)
        self.assertTemplateUsed(
            response=response, template_name='authentication/email_verification_sent.html')

    def test_user_creation_at_registration_works_if_user_already_exists(self):
        response1 = self.client.post(reverse('auth:register'),
                                     {'username': "Testuser3", 'email': "test@testuser.de",
                                      'password1': "lala1212",
                                      'password2': "lala1212"}, follow=True)
        self.assertEquals(response1.status_code, 200)
        self.assertEquals(len(User.objects.filter(username="Testuser3")), 1)
        self.client.post(reverse('auth:register'),
                         {'username': "Testuser3", 'email': "test4test4@testuser.de",
                                      'password1': "lala1414",
                                      'password2': "lala1414"}, follow=True)
        self.assertEquals(len(User.objects.filter(username="Testuser3")), 1)

        self.client.post(reverse('auth:register'),
                         {'username': "Testtestuser",
                                      'email': "test@testuser.de",
                                      'password1': "lala1515",
                                      'password2': "lala1515"}, follow=True)
        self.assertEquals(len(User.objects.filter(email="test@testuser.de")), 1)

    def test_user_registration_is_active_false(self):
        response = self.client.post(reverse('auth:register'), {'username': "Testuser",
                                                               'email': "test@testuser.de",
                                                               'password': "lala1212"},
                                    follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertEquals(len(User.objects.all()), 1)
        self.assertFalse(User.objects.filter(username="Testuser").values('is_active'))
