from django.apps import apps
from django.test import TestCase

from catalog.apps import CatalogConfig


class CatalogConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(CatalogConfig.name, 'catalog')
        self.assertEqual(apps.get_app_config('catalog').name, 'catalog')
