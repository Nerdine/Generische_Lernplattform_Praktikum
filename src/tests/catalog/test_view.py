import tempfile

from django.test import TestCase
from django.urls import reverse

from authentication.models import User
from course.models import Course, Participant, Instructor, Lesson
from prerequisite.models import Category, Prerequisite
from task.models import Text
from ..testing_utilities import populate_test_db, login_client_user, get_user_data

COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'


class CoursesViewTest(TestCase):

    def setUp(self):
        populate_test_db()
        self.user = User.objects.get(email=get_user_data()['email'])

    def test_without_course(self):
        login_client_user(self)
        response = self.client.get(reverse('catalog:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        self.assertContains(response, COURSE_NOT_AVAILABLE, count=1)

    def __create_courses(self):
        login_client_user(self)
        self.course_en_published = Course.objects.create(
            name='CourseEnPublished',
            short_description='ShortEnPublished',
            long_description='LongEnPublished',
            course_language='en',
            effort=1,
            published=True
        )
        self.course_en_not_published = Course.objects.create(
            name='CourseEnNotPublished',
            short_description='ShortEnNotPublished',
            long_description='LongEnNotPublished',
            course_language='en',
            effort=2,
            published=False
        )
        self.course_de_published = Course.objects.create(
            name='CourseDePublished',
            short_description='ShortDePublished',
            long_description='LongDePublished',
            course_language='de',
            effort=2,
            published=True
        )
        Participant.objects.create(course=self.course_de_published, user=self.user, rating=0)

        self.lesson_en = Lesson.objects.create(name='test', course=self.course_en_published)
        Text.objects.create(lesson=self.lesson_en, name='Text', content='Foo')

        self.lesson_de = Lesson.objects.create(name='test', course=self.course_de_published)
        Text.objects.create(lesson=self.lesson_de, name='Text', content='Foo')

        self.course_de_published.categories.add(Category.objects.create(type='TestCategory'))

    def __become_instructor_for_course(self, course):
        login_client_user(self)
        Instructor.objects.create(
            user=self.user,
            course=course
        )

    def test_with_default_search(self):
        self.__create_courses()
        response = self.client.get(reverse('catalog:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        self.assertContains(response, 'CourseEnPublished', count=1)
        self.assertContains(response, 'CourseDePublished', count=1)
        self.assertNotContains(response, COURSE_NOT_AVAILABLE, html=True)

    def test_with_text_search(self):
        self.__create_courses()
        response = self.client.get(reverse('catalog:courses'), {'q': 'CourseDe', 'languages': ''})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        self.assertContains(response, 'CourseEnPublished', count=0)
        self.assertContains(response, 'CourseDePublished', count=1)
        self.assertNotContains(response, COURSE_NOT_AVAILABLE, html=True)

    def test_with_language_search(self):
        self.__create_courses()
        response = self.client.get(reverse('catalog:courses'), {'q': '', 'languages': 'en'})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        self.assertContains(response, 'CourseEnPublished', count=1)
        self.assertContains(response, 'CourseDePublished', count=0)
        self.assertNotContains(response, COURSE_NOT_AVAILABLE, html=True)

    def test_with_category_search(self):
        self.__create_courses()
        response = self.client.get(reverse('catalog:courses'),
                                   {
            'languages': 'no',
            'categories': 'TestCategory'
        })
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        self.assertContains(response, 'CourseEnPublished', count=0)
        self.assertContains(response, 'CourseDePublished', count=1)
        self.assertNotContains(response, COURSE_NOT_AVAILABLE, html=True)

    def test_leave_course(self):
        self.__create_courses()
        course_slug = self.course_de_published.slug
        response = self.client.post(reverse('catalog:courses'), {
                                    'course_slug': course_slug, 'action': 'leave'}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        participant = Participant.objects.filter(
            user=self.user, course=self.course_en_published).first()
        self.assertIsNone(participant)
        self.__assert_one_message(
            response, 'You were successfully removed from the course CourseDePublished')

    def test_enter_course(self):
        self.__create_courses()
        course_slug = self.course_en_published.slug
        response = self.client.post(reverse('catalog:courses'),  {
                                    'course_slug': course_slug, 'action': 'enter'}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        participant = Participant.objects.get(user=self.user, course=self.course_de_published)
        self.assertIsNotNone(participant)
        self.__assert_one_message(
            response, 'You were successfully added to the course CourseEnPublished')

    def test_leave_course_of_no_participant(self):
        self.__create_courses()
        course_slug = self.course_en_published.slug
        response = self.client.post(reverse('catalog:courses'), {
                                    'course_slug': course_slug, 'action': 'leave'}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/courses.html')
        participant = Participant.objects.filter(
            user=self.user, course=self.course_en_published).first()
        self.assertIsNone(participant)
        self.__assert_one_message(
            response, 'You are\'t a member of the course CourseEnPublished')

    def test_enter_course_of_participant(self):
        self.__create_courses()
        course_slug = self.course_de_published.slug
        response = self.client.post(reverse('catalog:courses'),  {
                                    'course_slug': course_slug, 'action': 'enter'}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        participant = Participant.objects.get(user=self.user, course=self.course_de_published)
        self.assertIsNotNone(participant)
        self.__assert_one_message(
            response, 'You are already a member of the course CourseDePublished')

    def test_invalid_action(self):
        self.__create_courses()
        course_slug = self.course_de_published.slug
        response = self.client.post(reverse('catalog:courses'),  {
                                    'course_slug': course_slug, 'action': 'foo'}, follow=True)
        self.assertEquals(response.status_code, 500)

    def test_authentication(self):
        response = self.client.get(reverse('catalog:courses'))
        self.assertEquals(response.status_code, 200)
        response = self.client.post(reverse('catalog:courses'))
        self.assertEquals(response.status_code, 302)

    def __assert_one_message(self, response, expected_message):
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(
            str(messages[0]), expected_message)


class CourseDetailViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()

    def test_course_detail_get_unauthorized(self):
        course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        response = self.client.get(reverse('catalog:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/course_detail.html')
        self.assertContains(response, 'CourseA', count=1)
        self.assertNotContains(
            response, '<input type="submit" class="btn btn-primary" value="Enter"/>')

    def test_course_detail_get(self):
        login_client_user(self)

        cat = Category.objects.create(type="CSS")
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        prereq = Prerequisite.objects.create(skill_level='1', category=cat)
        course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        course.categories.add(cat)
        course.prerequisites.add(prereq)

        response = self.client.get(reverse('catalog:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/course_detail.html')
        self.assertContains(response, 'Novice - CSS', count=1)
        self.assertContains(response, 'CSS', count=2)
        self.assertContains(
            response, '<input type="submit" class="btn btn-primary" value="Enter"/>', count=1)

        Participant.objects.create(course=course, user=self.user, rating=0)
        response = self.client.get(reverse('catalog:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'catalog/course_detail.html')
        self.assertContains(response, '<p>You are a member of this course</p>', count=1)

    def test_course_detail_post(self):
        login_client_user(self)
        course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        lesson = Lesson.objects.create(name='test', course=course)
        text = Text.objects.create(lesson=lesson, name='Text', content='Foo')

        response = self.client.post(reverse('catalog:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug, follow=True)
        self.assertRedirects(response, reverse('desk:text_detail', kwargs={
            'task_slug': text.slug}))

        participant = Participant.objects.get(user=self.user, course=course)
        self.assertIsNotNone(participant)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'You were successfully added to the course CourseA')

    def test_course_detail_post_already_participant(self):
        login_client_user(self)
        course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        lesson = Lesson.objects.create(name='test', course=course)
        text = Text.objects.create(lesson=lesson, name='Text', content='Foo')
        Participant.objects.create(course=course, user=self.user, rating=0)

        response = self.client.post(reverse('catalog:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug, follow=True)
        self.assertRedirects(response, reverse('desk:text_detail', kwargs={
            'task_slug': text.slug}))

        # assert messages
        response.redirect_chain
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'You are already a member of the course CourseA')
