from django.apps import apps
from django.test import TestCase

from chat.apps import ChatConfig


class ChatConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(ChatConfig.name, 'chat')
        self.assertEqual(apps.get_app_config('chat').name, 'chat')
