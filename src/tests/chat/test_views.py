from django.test import TestCase
from django.urls import reverse

from chat.models import Chat, ChatStatus, Message
from course.models import Course, Participant, Instructor
from ..testing_utilities import populate_test_db, populate_test_db_2, login_client_user, \
    login_client_user_2

COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'


def set_up_infrastructure(self):
    self.user_a = populate_test_db()
    self.user_b = populate_test_db_2()

    self.course_a = Course.objects.create(
        name='CourseA',
        short_description='ShortDescription',
        long_description='LongDescription',
        course_language='en',
        effort=1,
        published=True
    )

    self.course_b = Course.objects.create(
        name='CourseB',
        short_description='ShortDescription',
        long_description='LongDescription',
        course_language='en',
        effort=1,
        published=True
    )

    Participant.objects.create(course=self.course_a, user=self.user_a, rating=0)
    Participant.objects.create(course=self.course_b, user=self.user_b, rating=0)

    self.chat_a = Chat.objects.create(
        subject="ChatA",
        course=self.course_a
    )

    self.chat_b_1 = Chat.objects.create(
        subject="ChatB1",
        course=self.course_b
    )

    self.chat_b_2 = Chat.objects.create(
        subject="ChatB2",
        course=self.course_b
    )

    Message.objects.create(chat=self.chat_a, content='Foo', author=self.user_a)
    Message.objects.create(chat=self.chat_b_1, content='Foo', author=self.user_b)

    ChatStatus.objects.create(user=self.user_b, chat=self.chat_b_1, isRead=True)
    ChatStatus.objects.create(user=self.user_b, chat=self.chat_b_2, isRead=False)


class ConversationsViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)

    def test_conversations(self):
        login_client_user(self)
        response = self.client.get(reverse('chat:conversations'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/conversations.html')
        self.assertContains(response, 'ChatA', count=1)
        self.assertContains(response, 'CourseA', count=1)
        self.assertContains(response, 'ChatB', count=0)
        self.assertContains(response, 'CourseB', count=0)

        login_client_user_2(self)
        response = self.client.get(reverse('chat:conversations'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/conversations.html')
        self.assertContains(response, 'ChatA', count=0)
        self.assertContains(response, 'CourseA', count=0)
        self.assertContains(response, 'ChatB1', count=1)
        self.assertContains(response, 'ChatB2', count=1)
        self.assertContains(response, 'CourseB', count=2)

    def test_can_create_conversation(self):
        login_client_user(self)
        Instructor.objects.create(user=self.user_a, course=self.course_a)
        response = self.client.get(reverse('chat:conversations'))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'Create a new conversation', count=1)

    def test_use_can_not_create_conversation(self):
        login_client_user(self)
        response = self.client.get(reverse('chat:conversations'))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'You can only create a conversation, '
                                      'if you are an instructor of a course.', count=1)


class MessageViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)

    def test_messages(self):
        login_client_user(self)
        response = self.client.get(reverse('chat:messages', kwargs={
            'chat_slug': self.chat_a.slug}), chat_slug=self.chat_a.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/messages.html')
        self.assertContains(response, 'Foo', count=1)
        self.assertContains(response, 'ChatA', count=1)

        status = ChatStatus.objects.filter(chat=self.chat_a, user=self.user_a).first()
        self.assertTrue(status.isRead)

    def test_can_delete_conversation(self):
        login_client_user(self)
        Instructor.objects.create(user=self.user_a, course=self.course_a)
        response = self.client.get(reverse('chat:messages', kwargs={
            'chat_slug': self.chat_a.slug}), chat_slug=self.chat_a.slug)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'Delete this conversation', count=1)

    def test_can_not_delete_conversation(self):
        login_client_user_2(self)
        response = self.client.get(reverse('chat:messages', kwargs={
            'chat_slug': self.chat_b_1.slug}), chat_slug=self.chat_b_1.slug)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'Delete this conversation', count=0)


class ConversationCreateViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)

    def test_get(self):
        login_client_user_2(self)
        response = self.client.get(reverse('chat:conversation_create'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/conversation_create.html')

    def test_post(self):
        login_client_user_2(self)
        Instructor.objects.create(user=self.user_b, course=self.course_b)
        response = self.client.post(reverse('chat:conversation_create'),
                                    {'subject': 'Foo',
                                     'courses': 'CourseB',
                                     'message': 'This is the message',
                                     'markdown-image-upload': ''}, follow=True)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/conversations.html')


class MessageCreateViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)

    def test_get(self):
        login_client_user_2(self)
        response = self.client.get(reverse('chat:message_create', kwargs={
            'chat_slug': self.chat_a.slug}), chat_slug=self.chat_a.slug)
        self.assertEquals(response.status_code, 302)

    def test_post(self):
        login_client_user_2(self)
        response = self.client.post(reverse('chat:message_create', kwargs={
            'chat_slug': self.chat_b_1.slug
        }),
            {
            'message': 'House of Cards'
        }, follow=True)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/messages.html')
        self.assertContains(response, 'House of Cards', count=1)


class ConversationDeleteViewTest(TestCase):

    def setUp(self):
        set_up_infrastructure(self)

    def test_get(self):
        login_client_user_2(self)
        response = self.client.get(reverse('chat:conversation_delete', kwargs={
            'chat_slug': self.chat_b_1.slug}), chat_slug=self.chat_b_1.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/conversation_delete.html')

    def test_post(self):
        login_client_user_2(self)
        Instructor.objects.create(user=self.user_b, course=self.course_b)
        response = self.client.post(reverse('chat:conversation_delete', kwargs={
            'chat_slug': self.chat_b_1.slug}), chat_slug=self.chat_b_1.slug, follow=True)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'chat/conversations.html')
