from django.apps import apps
from django.test import TestCase

from course.apps import CourseConfig


class CourseConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(CourseConfig.name, 'course')
        self.assertEqual(apps.get_app_config('course').name, 'course')
