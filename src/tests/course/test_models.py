from django.db import IntegrityError
from course.models import Course, Instructor, Participant, Lesson
from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class CourseTest(TestCase):

    def test_course(self):
        actual_course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )

        self.assertEqual(actual_course.name, 'CourseA')
        self.assertEqual(actual_course.short_description, 'ShortDescription')
        self.assertEqual(actual_course.long_description, 'LongDescription')
        self.assertEqual(actual_course.course_language, 'en')
        self.assertEqual(actual_course.effort, 1)
        self.assertEqual(actual_course.published, True)
        self.assertEqual(str(actual_course), 'CourseA')
        self.assertEqual(actual_course.slug, 'coursea')

    def test_duplicated_slugs(self):
        first_course = Course.objects.create(
            name='Course',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        second_course = Course.objects.create(
            name='Course',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.assertEqual(first_course.slug, 'course')
        self.assertEqual(second_course.slug, 'course-1')

    def test_update_course(self):
        actual_course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        actual_course.description = 'foo'
        actual_course.save()


class InstructorTest(TestCase):

    def setUp(self):
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.user = User.objects.create_user('Max', 'max@gmx.de', 'secret')
        self.user.save()

    def test_instructor(self):
        actual_instructor = Instructor.objects.create(user=self.user, course=self.course)

        self.assertEqual(actual_instructor.course, self.course)
        self.assertEqual(actual_instructor.user, self.user)

    def test_unique(self):
        Instructor.objects.create(user=self.user, course=self.course)
        self.assertRaises(IntegrityError, lambda: Instructor.objects.create(
            user=self.user, course=self.course))


class ParticipantTest(TestCase):

    def setUp(self):
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.user = User.objects.create_user('Max', 'max@gmx.de', 'secret')
        self.user.save()

    def test_participant(self):
        actual_participant = Participant.objects.create(
            user=self.user, course=self.course, rating=5)
        self.assertEqual(actual_participant.course, self.course)
        self.assertEqual(actual_participant.user, self.user)
        self.assertEqual(actual_participant.rating, 5)

    def test_unique(self):
        Participant.objects.create(user=self.user, course=self.course, rating=5)
        self.assertRaises(IntegrityError, lambda: Participant.objects.create(
            user=self.user, course=self.course, rating=5))


class LessonTest(TestCase):

    def setUp(self):
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )

    def test_lesson(self):
        actual_lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)

        self.assertEqual(actual_lesson.name, 'Name')
        self.assertEqual(actual_lesson.position, 0)
        self.assertEqual(actual_lesson.course, self.course)
        self.assertEqual(str(actual_lesson), 'Name')

    def test_duplicated_slugs(self):
        first_lesson = Lesson.objects.create(
            name='Lesson',  position=1, course=self.course)
        second_lesson = Lesson.objects.create(
            name='Lesson', position=2, course=self.course)
        self.assertEqual(first_lesson.slug, 'lesson')
        self.assertEqual(second_lesson.slug, 'lesson-1')
