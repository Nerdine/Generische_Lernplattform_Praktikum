from django.test import TestCase
from django.urls import reverse

from course.models import Course, Participant, Lesson
from task.models import Text, UserSuccess
from ..testing_utilities import populate_test_db, login_client_user

COURSE_TAG = '<div class="course">'
COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'


class CourseContinueView(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        login_client_user(self)
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )

        self.participant = Participant.objects.create(user=self.user, course=self.course, rating=0)

        self.lesson_a = Lesson.objects.create(
            name='LessonA',
            position=1,
            course=self.course
        )
        self.lesson_b = Lesson.objects.create(
            name='LessonB',
            position=2,
            course=self.course
        )

        self.text_a = Text.objects.create(name='TextA', position=1, lesson=self.lesson_a,
                                          content='TextA')
        self.text_b = Text.objects.create(name='TextB', position=2, lesson=self.lesson_a,
                                          content='TextB')
        self.text_c = Text.objects.create(name='TextC', position=1, lesson=self.lesson_b,
                                          content='TextC')

    def test_course_start(self):
        response = self.client.get(reverse(
            'course:continue',
            kwargs={'course_slug': self.course.slug}
        ), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        self.assertContains(response, 'TextA')

    def test_first_task_of_lesson_complete(self):
        UserSuccess.objects.create(taskbase=self.text_a, user=self.user, successful=True)
        response = self.client.get(reverse(
            'course:continue',
            kwargs={'course_slug': self.course.slug}
        ), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        self.assertContains(response, 'TextB')

    def test_lesson_complete(self):
        UserSuccess.objects.create(taskbase=self.text_b, user=self.user, successful=True)
        response = self.client.get(reverse(
            'course:continue',
            kwargs={'course_slug': self.course.slug}
        ), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        self.assertContains(response, 'TextC')

    def test_course_complete(self):
        UserSuccess.objects.create(taskbase=self.text_c, user=self.user, successful=True)
        response = self.client.get(reverse(
            'course:continue',
            kwargs={'course_slug': self.course.slug}
        ), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        self.assertContains(response, 'TextA')
