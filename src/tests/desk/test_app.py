from django.apps import apps
from django.test import TestCase

from desk.apps import DeskConfig


class DeskConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(DeskConfig.name, 'desk')
        self.assertEqual(apps.get_app_config('desk').name, 'desk')
