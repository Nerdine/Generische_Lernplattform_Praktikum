import tempfile

from django.test import TestCase
from django.urls import reverse

from course.models import Course, Participant, Lesson
from prerequisite.models import Category, Prerequisite
from task.models import Text, UserSuccess, Quiz, Question, Answer, UserAnswerSuccess, \
    ProgrammingExercise, Video, Exercise
from ..testing_utilities import populate_test_db, populate_test_db_2, login_client_user

COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'


def set_up_infrastructure(self):
    self.user = populate_test_db()

    self.course = Course.objects.create(
        name='CourseA',
        short_description='ShortDescription',
        long_description='LongDescription',
        course_language='en',
        effort=1,
        published=True
    )
    self.lesson = Lesson.objects.create(
        name='Name', position=0, course=self.course)


class CoursesViewTest(TestCase):

    def setUp(self):
        self.user_a = populate_test_db()
        self.user_b = populate_test_db_2()

    def test_without_course(self):
        login_client_user(self)
        response = self.client.get(reverse('desk:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/courses.html')
        self.assertContains(response, COURSE_NOT_AVAILABLE, count=1)

    def __create_courses(self):
        login_client_user(self)
        self.course_en_published_user_a = Course.objects.create(
            name='CoursePublishedUserA',
            short_description='ShortPublishedUserA',
            long_description='LongPublishedUserA',
            course_language='en',
            effort=1,
            published=True
        )
        Participant.objects.create(course=self.course_en_published_user_a, user=self.user_a,
                                   rating=0)

        self.course_en_published_user_b = self.course_en_published = Course.objects.create(
            name='CoursePublishedUserB',
            short_description='ShortPublishedUserB',
            long_description='LongPublishedUserB',
            course_language='en',
            effort=1,
            published=True
        )
        Participant.objects.create(course=self.course_en_published_user_b, user=self.user_b,
                                   rating=0)

        self.course_en_not_published_user_a = Course.objects.create(
            name='CourseNotPublishedUserA',
            short_description='ShortNotPublishedUserA',
            long_description='LongNotPublishedUserA',
            course_language='en',
            effort=2,
            published=False
        )
        Participant.objects.create(
            course=self.course_en_not_published_user_a, user=self.user_a, rating=0)

    def test_my_desk(self):
        self.__create_courses()
        response = self.client.get(reverse('desk:courses'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/courses.html')
        self.assertContains(response, 'CoursePublishedUserA', count=1)
        self.assertNotContains(response, 'CoursePublishedUserB')
        self.assertNotContains(response, 'CourseNotPublishedUserA')
        self.assertNotContains(response, COURSE_NOT_AVAILABLE, html=True)

    def test_authentication(self):
        response = self.client.get(reverse('desk:courses'))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse('desk:courses'))
        self.assertEquals(response.status_code, 302)


class TaskNavigationTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()

        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)
        self.participant = Participant.objects.create(user=self.user, course=self.course, rating=0)
        login_client_user(self)

    def test_redirect_to_previous_task(self):
        # create task without other task
        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='Content')

        # Create exactly one task before (position) the other
        Text.objects.create(name='123456789', position=41,
                            lesson=self.lesson, content='Content')

        response = self.client.post(reverse('desk:previous',
                                            kwargs={'task_slug': actual_text.slug}),
                                    task_slug=actual_text.slug, course_slug=self.course.slug,
                                    follow=True)
        self.assertEquals(response.status_code, 200)

        self.assertContains(response, '123456789', html=True)

    def test_next_button(self):
        # create task without other task
        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='Content')

        response = self.client.post(reverse('desk:next', kwargs={'task_slug': actual_text.slug}),
                                    task_slug=actual_text.slug, course_slug=self.course.slug,
                                    follow=True)
        self.assertEquals(response.status_code, 200)

        self.assertIsNotNone(
            UserSuccess.objects.filter(user=self.user, taskbase=actual_text).first())

    def test_redirect_to_next_task(self):
        # create task without other task
        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='Content')

        # Create exactly one task after (position) the other
        Text.objects.create(name='123456789', position=43,
                            lesson=self.lesson, content='Content')

        response = self.client.post(reverse('desk:next', kwargs={'task_slug': actual_text.slug}),
                                    task_slug=actual_text.slug, course_slug=self.course.slug,
                                    follow=True)
        self.assertEquals(response.status_code, 200)

        self.assertIsNotNone(
            UserSuccess.objects.filter(user=self.user, taskbase=actual_text).first())

        self.assertContains(response, '123456789', html=True)


class ExerciseTaskPassViewTest(TestCase):
    # TODO: Wenn wir zufrieden mit dieser Abgabe Strategie sind.
    pass


class QuizTaskPassViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()

        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)
        self.participant = Participant.objects.create(user=self.user, course=self.course, rating=0)
        login_client_user(self)

    def test_post_single_answer_quiz(self):
        actual_quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        question = Question.objects.create(
            text="My Question", position=1, type='0', quiz=actual_quiz)
        answer_1 = Answer.objects.create(text="right", position=1, is_correct=True,
                                         question=question)
        answer_2 = Answer.objects.create(text="wrong", position=2, is_correct=False,
                                         question=question)

        question_name = 'question-' + str(question.id)

        response = self.client.post(reverse('desk:quiz_pass', kwargs={
            'task_slug': actual_quiz.slug}), {question_name: answer_1.id}, follow=True)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/quiz_detail.html')

        self.assertTrue(UserAnswerSuccess.objects.get(user=self.user, quiz=actual_quiz,
                                                      answer=answer_1).is_correct_answered)
        self.assertTrue(UserAnswerSuccess.objects.get(user=self.user, quiz=actual_quiz,
                                                      answer=answer_2).is_correct_answered)


class TaskViewTest(TestCase):
    def setUp(self):
        set_up_infrastructure(self)
        self.participant = Participant.objects.create(user=self.user, course=self.course, rating=0)

    def test_authentication(self):
        response = self.client.get(reverse('desk:text_detail', kwargs={'task_slug': 'name'}))
        self.assertEquals(response.status_code, 302)

    def test_participant_authentication(self):
        login_client_user(self)

        # deletes the participant => the user has no access rights to the task
        self.participant.delete()

        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='Content')

        response = self.client.get(reverse('desk:text_detail', kwargs={
            'task_slug': actual_text.slug}), task_slug=actual_text.slug)

        self.assertEquals(response.status_code, 302)

    def test_exercise(self):
        login_client_user(self)
        actual_exercise = Exercise.objects.create(name='Name', position=42, lesson=self.lesson,
                                                  task='Task', solution='Solution')
        response = self.client.get(reverse('desk:exercise_detail', kwargs={
            'task_slug': actual_exercise.slug}), task_slug=actual_exercise.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/task_detail.html')
        self.assertTemplateUsed(response, 'desk/tasks/exercise_detail.html')
        self.assertContains(response, 'Task', count=1)

    def test_programming_exercise(self):
        login_client_user(self)
        actual_exercise = ProgrammingExercise.objects.\
            create(name='Name', position=42, lesson=self.lesson,
                   task='Task')

        response = self.client.get(reverse('desk:programmingexercise_detail', kwargs={
            'task_slug': actual_exercise.slug}), task_slug=actual_exercise.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/task_detail.html')
        self.assertTemplateUsed(
            response, 'desk/tasks/programming_exercise_detail.html')
        self.assertContains(response, 'Task', count=1)

    def test_text(self):
        login_client_user(self)
        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='Content')

        response = self.client.get(reverse('desk:text_detail', kwargs={
            'task_slug': actual_text.slug}), task_slug=actual_text.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/task_detail.html')
        self.assertTemplateUsed(response, 'desk/tasks/text_detail.html')
        self.assertContains(response, 'Content', count=1)

    def test_text_markdown(self):
        login_client_user(self)
        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='**Bold**')

        response = self.client.get(reverse('desk:text_detail', kwargs={
            'task_slug': actual_text.slug}), task_slug=actual_text.slug)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, '<strong>Bold</strong>', count=1, html=True)

    def test_video(self):
        login_client_user(self)
        actual_video = Video.objects.create(name='Name', position=42, lesson=self.lesson,
                                            upload='http://google.de')

        response = self.client.get(reverse('desk:video_detail', kwargs={
            'task_slug': actual_video.slug}), task_slug=actual_video.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/task_detail.html')
        self.assertTemplateUsed(response, 'desk/tasks/video_detail.html')

    def test_quiz(self):
        login_client_user(self)
        actual_quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        question = Question.objects.create(
            text="My Question", position=99, type='1', quiz=actual_quiz)
        Answer.objects.create(text="My Answer", position=13, is_correct=True, question=question)

        response = self.client.get(reverse('desk:quiz_detail', kwargs={
            'task_slug': actual_quiz.slug}), task_slug=actual_quiz.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/tasks/task_detail.html')
        self.assertTemplateUsed(response, 'desk/tasks/quiz_detail.html')
        self.assertContains(response, 'QuizName', count=2)
        self.assertContains(response, 'My Question', count=1)
        self.assertContains(response, 'My Answer', count=1)


class CourseDetailViewTest(TestCase):
    def setUp(self):
        self.user = populate_test_db()

    def test_authentication(self):
        response = self.client.get(
            reverse('desk:course_detail', kwargs={'course_slug': 'test'})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('desk:course_detail', kwargs={'course_slug': 'test'})
        )
        self.assertEquals(response.status_code, 302)

    def test_course_detail_get(self):
        login_client_user(self)

        cat = Category.objects.create(type="CSS")
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        prereq = Prerequisite.objects.create(skill_level='1', category=cat)
        course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescriptionA',
            long_description='LongDescriptionA',
            course_language='en',
            effort=1,
            published=True
        )
        course.categories.add(cat)
        course.prerequisites.add(prereq)

        response = self.client.get(reverse('desk:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug)
        self.assertEquals(response.status_code, 302)
        Participant.objects.create(course=course, user=self.user, rating=0)
        response = self.client.get(reverse('desk:course_detail', kwargs={
            'course_slug': course.slug}), course_slug=course.slug)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'desk/course_detail.html')
        self.assertContains(response, '<p>You are a member of this course</p>', count=1)
        self.assertContains(response, 'Novice - CSS', count=1)
        self.assertContains(response, 'CSS', count=2)
