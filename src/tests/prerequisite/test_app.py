from django.apps import apps
from django.test import TestCase

from prerequisite.apps import PrerequisiteConfig


class PrerequisiteConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(PrerequisiteConfig.name, 'prerequisite')
        self.assertEqual(apps.get_app_config('prerequisite').name, 'prerequisite')
