from django.db import IntegrityError
from django.test import TestCase
from prerequisite.models import Prerequisite, Category
import tempfile


class TestCategoryModel(TestCase):

    def test_category_string_representation(self):
        cat = Category.objects.create(type='CSS')
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        self.assertEqual(str(cat), cat.type)

    def test_duplicated_slugs(self):
        first_cat = Category.objects.create(
            type='Cat',
        )
        first_cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        second_cat = Category.objects.create(
            type='Cat'
        )
        second_cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        self.assertEqual(first_cat.slug, 'cat')
        self.assertEqual(second_cat.slug, 'cat-1')

    def test_update_category(self):
        cat = Category.objects.create(type='CSS')
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        cat.type = 'foo'
        cat.save()


class TestPrerequisiteModel(TestCase):

    def test_prerequisite_string_representation(self):
        cat = Category.objects.create(type='CSS')
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        prereq = Prerequisite.objects.create(skill_level='1', category=cat)
        self.assertEqual(str(prereq), 'Novice - CSS')

    def test_unique_constraint(self):
        cat = Category.objects.create(
            type='Cat',
        )
        cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name

        first_prereq = Prerequisite.objects.create(
            skill_level='1', category=cat)
        self.assertEqual(first_prereq.slug, 'cat-1')

        data = dict(skill_level='1', category=cat)
        second_prereq = Prerequisite(**data)
        with self.assertRaises(Exception) as raised:
            second_prereq.save()
        self.assertEqual(IntegrityError, type(raised.exception))
