from django.test import TestCase
from django.urls import reverse
import tempfile

from course.models import Course
from prerequisite.models import Category, Prerequisite
from ..testing_utilities import populate_test_db, login_client_user, \
    populate_test_db_2, login_client_user_2
from PIL import Image

CATEGORY_TAG = '<li class="list-group-item category-list-group">'
CATEGORY_NOT_AVAILABLE = '<p>No categories available.</p>'


# needed for POST data
def _create_image():
    with tempfile.NamedTemporaryFile(suffix='.png', delete=False) as f:
        image = Image.new('RGB', (100, 100), 'white')
        image.save(f, 'PNG')

    return open(f.name, mode='rb')


class CategoryCreateViewTest(TestCase):
    def setUp(self):
        self.user = populate_test_db()
        self.user2 = populate_test_db_2()
        self.image = _create_image()

    def test_authentication(self):
        login_client_user(self)
        response = self.client.get(
            reverse('prerequisite:category_create')
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('prerequisite:category_create')
        )
        self.assertEquals(response.status_code, 302)

    def test_category_create_get(self):
        login_client_user_2(self)
        response = self.client.get(
            reverse('prerequisite:category_create')
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_create.html')
        self.assertContains(response, 'Create category', count=1)

    def test_category_create_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse('prerequisite:category_create'),
            {
                'type': 'CSS',
                'picture': self.image
            },
            follow=True
        )

        self.assertRedirects(
            response, reverse('prerequisite:category_overview')
        )
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        # name of category is counted twice: once in messages and once in the category list
        self.assertContains(response, 'CSS', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "'CSS' created successfully")

        # assert database
        category = Category.objects.filter(type="CSS").first()
        self.assertNotEqual(category, None)
        self.assertEqual(Prerequisite.objects.filter(category=category).count(), 3)

    def test_category_create_duplicate_type_post(self):
        login_client_user_2(self)
        Category.objects.create(type='CSS')
        response = self.client.post(
            reverse('prerequisite:category_create'),
            {
                'type': 'CSS',
                'picture': self.image
            },
            follow=True
        )

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_create.html')
        # name of category is counted once in the input field
        self.assertContains(response, 'CSS', count=1)
        # Validation error
        self.assertContains(response, 'A category with that name already exists.', count=1)


class CategoryUpdateViewTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()
        self.user2 = populate_test_db_2()
        self.image = _create_image()
        self.cat = Category.objects.create(type='CSS')
        self.cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        Prerequisite.objects.create(skill_level='1', category=self.cat)

    def test_authentication(self):
        login_client_user(self)
        response = self.client.get(reverse(
            'prerequisite:category_update',
            kwargs={'category_slug': 'css'}
        ))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse(
            'prerequisite:category_update',
            kwargs={'category_slug': 'css'}
        ))
        self.assertEquals(response.status_code, 302)

    def test_category_update_get(self):
        login_client_user_2(self)
        response = self.client.get(reverse(
            'prerequisite:category_update',
            kwargs={'category_slug': 'css'}
        ))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_update.html')
        self.assertContains(response, 'Update category', count=1)

    def test_category_update_change_type_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse(
                'prerequisite:category_update',
                kwargs={'category_slug': 'css'}
            ),
            {
                'type': 'CSSChange',
                'picture': self.image
            },
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('prerequisite:category_overview')
        )
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        # name of category is counted twice: once in messages and once in the category list
        self.assertContains(response, 'CSSChange', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "'CSSChange' updated successfully")

    def test_category_update_change_type_to_duplicate_post(self):
        login_client_user_2(self)
        Category.objects.create(type='CSSChange')
        response = self.client.post(
            reverse(
                'prerequisite:category_update',
                kwargs={'category_slug': 'css'}
            ),
            {
                'type': 'CSSChange',
                'picture': self.image
            },
            follow=True
        )
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_update.html')
        # name of category is counted once in the input field
        self.assertContains(response, 'CSSChange', count=1)
        # Validation error
        self.assertContains(response, 'A category with that name already exists.', count=1)

    def test_category_update_change_image_post(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse(
                'prerequisite:category_update',
                kwargs={'category_slug': 'css'}
            ),
            {
                'type': 'CSS',
                'picture': self.image
            },
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('prerequisite:category_overview')
        )
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        # name of category is counted twice: once in messages and once in the category list
        self.assertContains(response, 'CSS', count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "'CSS' updated successfully")


class CategoryDeleteViewTest(TestCase):

    def setUp(self):
        populate_test_db()
        populate_test_db_2()
        self.cat = Category.objects.create(type='CSS')
        Prerequisite.objects.create(skill_level='1', category=self.cat)
        self.cat.picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.category1 = Category.objects.create(
            type='UndeletableCauseOfCategoryUsage',
        )
        Prerequisite.objects.create(
            category=self.category1,
            skill_level='1',
        )
        self.category1.courses.set([self.course])

        self.category2 = Category.objects.create(
            type='UndeletableCauseOfPrerequisiteUsage',
        )
        self.prerequisite = Prerequisite.objects.create(
            category=self.category2,
            skill_level='1',
        )
        self.prerequisite.courses.set([self.course])

    def test_authentication(self):
        login_client_user(self)
        response = self.client.get(
            reverse('prerequisite:category_delete', kwargs={'category_slug': self.cat.slug})
        )
        self.assertEquals(response.status_code, 302)
        response = self.client.post(
            reverse('prerequisite:category_delete', kwargs={'category_slug': self.cat.slug})
        )
        self.assertEquals(response.status_code, 302)

    def test_category_delete_get(self):
        login_client_user_2(self)
        response = self.client.get(reverse(
            'prerequisite:category_delete',
            kwargs={'category_slug': self.cat.slug}
        ))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_delete.html')
        self.assertContains(
            response,
            'Are you sure you want to delete "CSS"?',
            count=1
        )

    def test_category_delete_post_success(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse(
                'prerequisite:category_delete',
                kwargs={'category_slug': self.cat.slug}
            ),
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('prerequisite:category_overview')
        )
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        # name of category is counted once in messages
        self.assertContains(response, 'CSS', count=1)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "'CSS' deleted successfully")

    def test_category_delete_post_failure_category_in_use_as_category(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse(
                'prerequisite:category_delete',
                kwargs={'category_slug': self.category1.slug}
            ),
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('prerequisite:category_overview')
        )
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        # name of category is counted once in messages and once in the category list
        self.assertContains(response, self.category1.type, count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "'" + self.category1.type +
                         "' cannot be deleted. It is used for a course.")

    def test_category_delete_post_failure_category_in_use_as_prerequisite(self):
        login_client_user_2(self)
        response = self.client.post(
            reverse(
                'prerequisite:category_delete',
                kwargs={'category_slug': self.category2.slug}
            ),
            follow=True
        )

        self.assertRedirects(
            response,
            reverse('prerequisite:category_overview')
        )
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        # name of category is counted once in messages and once in the category list
        self.assertContains(response, self.category2.type, count=2)

        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), "'" + self.category2.type +
                         "' cannot be deleted. It is used as a prerequisite for a course.")


class CategoryOverviewViewTest(TestCase):
    def setUp(self):
        populate_test_db()
        populate_test_db_2()

    def test_authentication(self):
        login_client_user(self)
        response = self.client.get(reverse('prerequisite:category_overview'))
        self.assertEquals(response.status_code, 302)
        response = self.client.post(reverse('prerequisite:category_overview'))
        self.assertEquals(response.status_code, 302)

    def test_category_overview_without_categories(self):
        login_client_user_2(self)
        response = self.client.get(reverse('prerequisite:category_overview'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        self.assertNotContains(response, CATEGORY_TAG)
        self.assertContains(response, CATEGORY_NOT_AVAILABLE, count=1)

    def test_category_overview_with_two_categories(self):
        login_client_user_2(self)
        picture = tempfile.NamedTemporaryFile(suffix='.jpg').name
        Category.objects.create(type='CSS', picture=picture)
        Category.objects.create(type='CSS2', picture=picture)
        response = self.client.get(reverse('prerequisite:category_overview'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'prerequisite/category_overview.html')
        self.assertContains(response, CATEGORY_TAG, count=2)
        self.assertNotContains(response, CATEGORY_NOT_AVAILABLE, html=True)
