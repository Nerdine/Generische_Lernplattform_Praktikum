from django.test import TestCase

from course.models import Course, Lesson
from task.models import Exercise


class ExerciseTest(TestCase):

    def setUp(self):

        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name="Name", position=0, course=self.course)
        self.exercise = Exercise.objects.create(
            name="ExerciseName", position=42,
            lesson=self.lesson, task="TaskText",
            solution=open('src/tests/test_data/plain_text_file_solution.txt').read())

        self.exercise2 = Exercise.objects.create(
            name="ExerciseName", position=42,
            lesson=self.lesson, task="TaskText", solution="solution")

    def test_exercise(self):
        actual_exercise = Exercise.objects.get(id=self.exercise.id)
        actual_exercise_2 = Exercise.objects.get(id=self.exercise2.id)
        self.assertEqual(actual_exercise.position, 42)
        self.assertEqual(actual_exercise.lesson.id, self.lesson.id)
        self.assertEqual(self.lesson.tasks.all().first().id, actual_exercise.id)
        self.assertEqual(actual_exercise.solution, open(
            'src/tests/test_data/plain_text_file_solution.txt').read())
        self.assertEqual(actual_exercise_2.solution, self.exercise2.solution)

    def test_exercise_str(self):
        self.assertEqual(str(self.exercise), 'ExerciseName')
