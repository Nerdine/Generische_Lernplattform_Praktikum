from django.test import TestCase

from course.models import Course, Lesson
from task.managers import CourseProgressManager
from task.models import Text, UserSuccess
from ..testing_utilities import populate_test_db


class CourseProgressManagerTest(TestCase):

    def setUp(self):
        self.user = populate_test_db()

        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)
        task_a = Text.objects.create(name='Name', position=0, lesson=self.lesson,
                                     content='Content')
        Text.objects.create(name='Name', position=1, lesson=self.lesson, content='Content')
        UserSuccess.objects.create(user=self.user, taskbase=task_a, successful=True)

    def test(self):
        actual_progress = CourseProgressManager().get_course_progress(self.course, self.user)
        self.assertEqual(1 / 2, actual_progress)
