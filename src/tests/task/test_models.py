from django.test import TestCase

from course.models import Course, Lesson
from task.models import Exercise, Text, Video, TaskBase


class TaskBaseTest(TestCase):

    def setUp(self):
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)

    def test_properties(self):
        actual_task_base = TaskBase.objects.create(name='Name', position=42, lesson=self.lesson)
        self.assertEqual(actual_task_base.name, 'Name')
        self.assertEqual(actual_task_base.position, 42)
        self.assertEqual(actual_task_base.lesson, self.lesson)
        self.assertEqual(actual_task_base.slug, "name")

    def test_duplicated_slug(self):
        TaskBase.objects.create(name='Name', position=42, lesson=self.lesson)
        actual_task_base = TaskBase.objects.create(name='Name', position=43, lesson=self.lesson)
        self.assertEqual(actual_task_base.slug, "name-1")

    def test_update_with_slug(self):
        actual_task_base = TaskBase.objects.create(name='Name', position=43, lesson=self.lesson)
        actual_task_base.name = 'Foo'
        actual_task_base.save()
        self.assertEqual(actual_task_base.slug, "name")


class TaskTest(TestCase):

    def setUp(self):
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name='Name', position=0, course=self.course)

    def test_exercise(self):
        actual_exercise = Exercise.objects.create(name='Name', position=42, lesson=self.lesson,
                                                  task='Exercise', solution='Solution')

        self.assertEqual(actual_exercise.name, 'Name')
        self.assertEqual(actual_exercise.position, 42)
        self.assertEqual(actual_exercise.lesson, self.lesson)
        self.assertEqual(str(actual_exercise), 'Name')
        self.assertEqual(actual_exercise.slug, "name")

        self.assertEqual(actual_exercise.task, 'Exercise')
        self.assertEqual(actual_exercise.solution, 'Solution')

    def test_text(self):
        actual_text = Text.objects.create(
            name='Name', position=42, lesson=self.lesson, content='Content')

        self.assertEqual(actual_text.name, 'Name')
        self.assertEqual(actual_text.position, 42)
        self.assertEqual(actual_text.lesson, self.lesson)
        self.assertEqual(str(actual_text), 'Name')
        self.assertEqual(actual_text.slug, "name")

        self.assertEqual(actual_text.content, 'Content')

    def test_video(self):
        actual_video = Video.objects.create(name='Name', position=42, lesson=self.lesson,
                                            upload='http://google.de')

        self.assertEqual(actual_video.name, 'Name')
        self.assertEqual(actual_video.position, 42)
        self.assertEqual(actual_video.lesson, self.lesson)
        self.assertEqual(str(actual_video), 'Name')
        self.assertEqual(actual_video.slug, "name")

        self.assertEqual(actual_video.upload, 'http://google.de')
