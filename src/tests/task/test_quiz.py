from django.test import TestCase

from course.models import Course, Lesson
from task.models import Quiz, Question, Answer


class QuizTests(TestCase):

    def setUp(self):
        self.course = Course.objects.create(
            name='CourseA',
            short_description='ShortDescription',
            long_description='LongDescription',
            course_language='en',
            effort=1,
            published=True
        )
        self.lesson = Lesson.objects.create(
            name="Name", position=0, course=self.course)
        self.quiz = Quiz.objects.create(name="QuizName", position=42, lesson=self.lesson)
        self.question = Question.objects.create(
            text="My Question", position=99, type='1', quiz=self.quiz)
        self.answer = Answer.objects.create(text="My Answer", position=13,
                                            is_correct=True, question=self.question)

    def test_quiz(self):
        actual_quiz = Quiz.objects.get(id=self.quiz.id)

        self.assertEqual(actual_quiz.position, 42)
        self.assertEqual(actual_quiz.lesson.id, self.lesson.id)
        self.assertEqual(self.lesson.tasks.all().first().id, actual_quiz.id)
        self.assertEqual(actual_quiz.get_questions().first(), self.question)

    def test_quiz_str(self):
        self.assertEqual(str(self.quiz), 'QuizName')

    def test_question(self):
        actual_question = Question.objects.get(id=self.question.id)

        self.assertEqual(actual_question.text, "My Question")
        self.assertEqual(actual_question.position, 99)
        self.assertEqual(actual_question.type, '0')
        # automatisch auf 0 (single-answer) gesetzt,
        # wenn nur eine korrekte Antwort mit der Frage verknüpft wurde
        self.assertEqual(actual_question.quiz.id, self.quiz.id)
        self.assertEqual(actual_question.get_answers().first(), self.answer)
        self.assertEqual(self.quiz.get_questions().first().id, actual_question.id)

    def test_question_str(self):
        self.assertEqual(str(self.question), 'My Question')

    def test_answer(self):
        actual_answer = Answer.objects.get(id=self.answer.id)

        self.assertEqual(actual_answer.text, "My Answer")
        self.assertEqual(actual_answer.position, 13)
        self.assertEqual(actual_answer.is_correct, True)
        self.assertEqual(actual_answer.question.id, self.question.id)
        self.assertEqual(self.question.get_answers().first().id, actual_answer.id)

    def test_answer_str(self):
        self.assertEqual(str(self.answer), 'My Answer')
