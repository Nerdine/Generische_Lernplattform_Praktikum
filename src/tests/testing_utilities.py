from django.contrib.auth import get_user_model

TEST_USER_DATA = {'email': 'lennon@thbeatles.com', 'name': 'john', 'password': '<password>'}
TEST_USER_DATA2 = {'email': 'lennon2@thbeatles.com', 'name': 'john2', 'password': '<password>'}
TEST_USER_DATA3 = {'email': 'lennon22@thbeatles.com', 'name': 'john22', 'password': '<password>'}

User = get_user_model()


def populate_test_db():
    """
    Adds test user to an empty test database
    """
    user = User.objects.create_user(
        TEST_USER_DATA['name'],
        TEST_USER_DATA['email'],
        TEST_USER_DATA['password'])
    user.can_be_instructor = False
    user.save()
    return user


def login_client_user(self):
    self.client.login(
        username=TEST_USER_DATA['name'],
        password=TEST_USER_DATA['password'])
    return self


def get_user_data():
    return TEST_USER_DATA


def populate_test_db_2():
    """
    Adds test user to an empty test database
    """
    user = User.objects.create_user(
        TEST_USER_DATA2['name'],
        TEST_USER_DATA2['email'],
        TEST_USER_DATA2['password'])
    user.can_be_instructor = True
    user.save()
    return user


def login_client_user_2(self):
    self.client.login(
        username=TEST_USER_DATA2['name'],
        password=TEST_USER_DATA2['password'])
    return self


def populate_instructor_test_db():
    """
    Adds test user to an empty test database
    """
    user = User.objects.create_user(
        TEST_USER_DATA3['name'],
        TEST_USER_DATA3['email'],
        TEST_USER_DATA3['password'])
    user.can_be_instructor = True
    user.save()
    return user
