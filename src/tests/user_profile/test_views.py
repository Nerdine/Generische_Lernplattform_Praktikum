from django.test import TestCase
from django.urls import reverse
from course.models import Course, Instructor
from authentication.models import Event, Specialty
from ..testing_utilities import populate_test_db, populate_test_db_2, login_client_user
from django.contrib.auth import get_user_model
from django.core import mail
import re
import datetime

COURSE_NOT_AVAILABLE = '<p>No courses available.</p>'

User = get_user_model()


class UserProfilePageTest(TestCase):
    def setUp(self):
        self.user_a = populate_test_db()
        self.user_b = populate_test_db_2()

    def __create_courses(self):
        login_client_user(self)
        self.course_en_published_user_a = Course.objects.create(
            name='CoursePublishedUserA',
            short_description='ShortPublishedUserA',
            long_description='LongPublishedUserA',
            course_language='en',
            effort=1,
            published=True
        )
        Instructor.objects.create(course=self.course_en_published_user_a, user=self.user_a)

        self.course_en_published_user_b = self.course_en_published = Course.objects.create(
            name='CoursePublishedUserB',
            short_description='ShortPublishedUserB',
            long_description='LongPublishedUserB',
            course_language='en',
            effort=1,
            published=True
        )
        Instructor.objects.create(course=self.course_en_published_user_b, user=self.user_b)

        self.course_en_not_published_user_a = Course.objects.create(
            name='CourseNotPublishedUserA',
            short_description='ShortNotPublishedUserA',
            long_description='LongNotPublishedUserA',
            course_language='en',
            effort=2,
            published=False
        )
        Instructor.objects.create(
            course=self.course_en_not_published_user_a, user=self.user_a)

    def test_user_profile_page(self):
        login_client_user(self)
        response = self.client.get(reverse('profile:profile'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/user_profile_page.html')
        self.assertContains(response, "Username", count=1)

    def test_user_profile_page_without_auth(self):
        response = self.client.get(reverse('profile:profile'))
        self.assertEquals(response.status_code, 302)


class EditUserProfileTest(TestCase):
    def setUp(self):
        populate_test_db()

    def test_edit_user_page(self):
        login_client_user(self)
        response = self.client.get(reverse('profile:edit_profile'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/edit_user_profile.html')
        self.assertContains(response, "/profile/edit_profile", count=1)

    def test_edit_user_page_without_auth(self):
        response = self.client.get(reverse('profile:edit_profile'))
        self.assertEquals(response.status_code, 302)

    def test_edit_name(self):
        login_client_user(self)
        self.assertEquals(len(User.objects.filter(first_name="First")), 0)
        response1 = self.client.post(reverse('profile:edit_profile'),
                                     {'first_name': "First", 'email': "first@last.de",
                                      'last_name': "Last"}, follow=True)
        self.assertEquals(response1.status_code, 200)
        self.assertEquals(len(User.objects.filter(first_name="First")), 1)
        # assert messages
        messages = list(response1.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We changed your data successfully!')


class EditUserPasswordTest(TestCase):
    def setUp(self):
        populate_test_db()

    def test_edit_password_page(self):
        login_client_user(self)
        response = self.client.get(reverse('profile:edit_password'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/edit_user_password.html')
        self.assertContains(response, "Old password", count=1)

    def test_edit_user_page_without_auth(self):
        response = self.client.get(reverse('profile:edit_password'))
        self.assertEquals(response.status_code, 302)

    def test_edit_password_wrong_1(self):
        login_client_user(self)
        old_pw = User.objects.first().password
        response = self.client.post(reverse('profile:edit_password'),
                                    {'new_password1': "richtig",
                                     'new_password2': "falsch",
                                     'old_password': '<password>'}, follow=True)
        self.assertTemplateUsed(response, 'user_profile/edit_user_password.html')
        self.assertContains(response, "Old password", count=1)
        self.assertEqual(old_pw, User.objects.first().password)

    def test_edit_password_wrong_2(self):
        login_client_user(self)
        old_pw = User.objects.first().password
        response = self.client.post(reverse('profile:edit_password'),
                                    {'new_password1': "richtig",
                                     'new_password2': "richtig",
                                     'old_password': 'falsch'}, follow=True)
        self.assertTemplateUsed(response, 'user_profile/edit_user_password.html')
        self.assertContains(response, "Old password", count=1)
        self.assertEqual(old_pw, User.objects.first().password)

    def test_edit_password_correct(self):
        login_client_user(self)
        old_pw = User.objects.first().password
        response = self.client.post(reverse('profile:edit_password'),
                                    {'new_password1': "richtigtollespw",
                                     'new_password2': "richtigtollespw",
                                     'old_password': '<password>'}, follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/user_profile_page.html')
        self.assertNotEqual(old_pw, User.objects.first().password)
        # assert messages
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We changed your data successfully!')


class EditUserEmailTest(TestCase):
    def setUp(self):
        populate_test_db()

    def test_edit_email_page(self):
        login_client_user(self)
        response = self.client.get(reverse('profile:edit_email'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/edit_user_email.html')
        self.assertContains(response, "Email address", count=1)

    def test_edit_user_page_without_auth(self):
        response = self.client.get(reverse('profile:edit_email'))
        self.assertEquals(response.status_code, 302)

    def test_edit_email(self):
        login_client_user(self)
        response1 = self.client.post(reverse('profile:edit_email'),
                                     {'email': "first@last.de"}, follow=True)
        self.assertEquals(response1.status_code, 200)
        # assert messages
        messages = list(response1.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We sent you an E-mail. Please verify your E-mail!')
        self.assertNotEqual("first@last.de", User.objects.first().email)
        # assert mail
        self.assertEqual(len(mail.outbox), 1)
        url_match = re.search(r"https?://[^/]*/.*verification\S*", mail.outbox[0].body)
        self.assertIsNotNone(url_match, "No URL found in sent mail")
        path = url_match.group(0)
        # set the new password
        self.client.get(path)
        # assert messages
        self.assertEqual("first@last.de", User.objects.first().email)


class EditBiographyTest(TestCase):
    def setUp(self):
        populate_test_db()

    def test_edit_biography_without_auth(self):
        response = self.client.get(reverse('profile:edit_biography'))
        self.assertEquals(response.status_code, 302)

    def test_edit_biography_page(self):
        login_client_user(self)
        response = self.client.get(reverse('profile:edit_biography'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/edit_user_biography.html')
        self.assertContains(response, "Edit Biography", count=1)

    def test_edit_biography(self):
        login_client_user(self)
        response1 = self.client.post(reverse('profile:edit_biography'),
                                     {'position': "Boss",
                                      'institution': "LPF-AG",
                                      'start_date': "2016-01-01",
                                      'end_date': "2017-01-01"}, follow=True)
        self.assertEquals(response1.status_code, 200)
        messages = list(response1.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We saved your event successfully!')
        self.assertEqual("Boss", Event.objects.first().position)
        self.assertEqual("LPF-AG", Event.objects.first().institution)
        self.assertEqual(datetime.date(2016, 1, 1), Event.objects.first().start_date)
        self.assertEqual(datetime.date(2017, 1, 1), Event.objects.first().end_date)

        slug = Event.objects.first().slug
        response2 = self.client.post(reverse('profile:edit_biography') + '?event=' + slug,
                                     {'position': "Boss",
                                      'institution': "LPF-AG",
                                      'start_date': "2016-02-02",
                                      'end_date': "2017-02-02"}, follow=True)
        self.assertEquals(response2.status_code, 200)
        messages = list(response2.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We saved your event successfully!')
        self.assertEqual("Boss", Event.objects.first().position)
        self.assertEqual("LPF-AG", Event.objects.first().institution)
        self.assertEqual(datetime.date(2016, 2, 2), Event.objects.first().start_date)
        self.assertEqual(datetime.date(2017, 2, 2), Event.objects.first().end_date)

        slug = Event.objects.first().slug
        response3 = self.client.get(reverse('profile:delete_biography') + '?event=' + slug,
                                    follow=True)
        messages = list(response3.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Deleted event successfully!')
        self.assertIsNone(Event.objects.first())

    def test_edit_biography_wrong_dates(self):
        login_client_user(self)
        response1 = self.client.post(reverse('profile:edit_biography'),
                                     {'position': "Boss",
                                      'institution': "LPF-AG",
                                      'end_date': "2016-01-01",
                                      'start_date': "2017-01-01"}, follow=True)
        self.assertEquals(response1.status_code, 200)
        messages = list(response1.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Your dates are incorrect!')


class EditSpecialtiesTest(TestCase):
    def setUp(self):
        populate_test_db()

    def test_edit_specialties_without_auth(self):
        response = self.client.get(reverse('profile:edit_specialties'))
        self.assertEquals(response.status_code, 302)

    def test_edit_specialties_page(self):
        login_client_user(self)
        response = self.client.get(reverse('profile:edit_specialties'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user_profile/edit_user_specialties.html')
        self.assertContains(response, "Edit Specialties", count=1)

    def test_edit_specialties(self):
        login_client_user(self)
        response1 = self.client.post(reverse('profile:edit_specialties'),
                                     {'title': "Python",
                                      'knowledge_level': 3}, follow=True)
        self.assertEquals(response1.status_code, 200)
        messages = list(response1.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We saved your specialty successfully!')
        self.assertEqual("Python", Specialty.objects.first().title)
        self.assertEqual("3", Specialty.objects.first().knowledge_level)

        slug = Specialty.objects.first().slug
        response2 = self.client.post(reverse('profile:edit_specialties') + '?specialty=' + slug,
                                     {'title': "Python",
                                      'knowledge_level': 2}, follow=True)
        self.assertEquals(response2.status_code, 200)
        messages = list(response2.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'We saved your specialty successfully!')
        self.assertEqual("Python", Specialty.objects.first().title)
        self.assertEqual("2", Specialty.objects.first().knowledge_level)

        slug = Specialty.objects.first().slug
        response3 = self.client.get(reverse('profile:delete_specialties') + '?specialty=' + slug,
                                    follow=True)
        messages = list(response3.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Deleted specialty successfully!')
        self.assertIsNone(Specialty.objects.first())
