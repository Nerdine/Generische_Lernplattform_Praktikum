from django.forms import EmailField, ValidationError, DateField, TextInput
from course.forms import BaseForm
from authentication.models import User, Event, Specialty
from django.utils.translation import ugettext as _


class UniqueEmailField(EmailField):
    def validate(self, value):
        super(EmailField, self).validate(value)
        if value is '':
            raise ValidationError(_("Email is empty!"))
        if User.check_email_already_registered(value):
            raise ValidationError(_("Email already exists!"))


class EditUserForm(BaseForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'has_email_notifications_activated']


class EditEmailForm(BaseForm):
    email = UniqueEmailField(required=False, label=_("Email address"))

    class Meta:
        model = User
        fields = ['email']


class EditBiographyForm(BaseForm):
    start_date = DateField(
         widget=TextInput(
            attrs={'type': 'date'}
         )
    )
    end_date = DateField(
         widget=TextInput(
            attrs={'type': 'date'}
         )
    )

    class Meta:
        model = Event
        fields = ['position', 'institution', 'start_date', 'end_date']


class EditSpecialtyForm(BaseForm):
    class Meta:
        model = Specialty
        fields = ['title', 'knowledge_level']
