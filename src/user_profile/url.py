from django.urls import path

from . import views

app_name = 'user_profile'
urlpatterns = [
    path('profile', views.ProfileView.as_view(), name='profile'),
    path('edit_profile', views.EditProfileView.as_view(), name='edit_profile'),
    path('edit_password', views.EditPasswordView.as_view(), name='edit_password'),
    path('edit_email', views.EditEmailView.as_view(), name='edit_email'),
    path('edit_biography', views.EditBiographyView.as_view(), name='edit_biography'),
    path('delete_biography', views.DeleteBiographyView.as_view(), name='delete_biography'),
    path('edit_specialties', views.EditSpecialtiesView.as_view(), name='edit_specialties'),
    path('delete_specialties', views.DeleteSpecialtiesView.as_view(), name='delete_specialties'),
    path(
        'edit_email_verification',
        views.EmailChangeDoneView.as_view(),
        name='edit_email_verification'
    ),
]
