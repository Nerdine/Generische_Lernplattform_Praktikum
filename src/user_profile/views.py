from celery import shared_task
from django.contrib.auth import get_user_model, views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView
from course.models import Instructor, Course
from user_profile.forms import EditUserForm, EditEmailForm, EditBiographyForm, EditSpecialtyForm
from django.contrib.auth.forms import PasswordChangeForm
from django.urls import reverse_lazy
from django.contrib.auth import login
from django.views.generic.edit import FormView
from django.views import View
from django.contrib import messages
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
import uuid
from datetime import timedelta
from django.utils import timezone
from django.shortcuts import redirect

from authentication.models import Event, Specialty
from django.http import HttpResponseRedirect

User = get_user_model()

# Create your views here.


class ProfileView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'user_profile/user_profile_page.html'

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        user = self.request.user
        recent_course = Course.objects.first()

        context['user'] = user
        context['events'] = user.events.all()
        context['specialties'] = user.specialties.order_by('-knowledge_level').all()
        related_instructors = Instructor.objects.filter(user=user.id).all()

        courses = list()
        for related in related_instructors:
            courses.append(Course.objects.filter(id=related.course_id).first())
        context['courses'] = courses
        context['recent_course'] = recent_course
        is_instructor = False
        if Instructor.objects.filter(user_id=self.request.user.id).first() is not None:
            is_instructor = True
        context['is_instructor'] = is_instructor
        return context


class EditProfileView(LoginRequiredMixin, FormView):
    model = User
    form_class = EditUserForm
    template_name = 'user_profile/edit_user_profile.html'
    success_url = reverse_lazy('user_profile:profile')

    def get_initial(self):
        initial = super(FormView, self).get_initial()
        data = self.request.user
        initial['first_name'] = data.first_name
        initial['last_name'] = data.last_name
        initial['has_email_notifications_activated'] = data.has_email_notifications_activated
        return initial

    def form_valid(self, form):
        response = super(EditProfileView, self).form_valid(form)
        new_data = self.request.user
        new_data.first_name = form.cleaned_data['first_name']
        new_data.last_name = form.cleaned_data['last_name']
        new_data.has_email_notifications_activated = \
            form.cleaned_data['has_email_notifications_activated']
        new_data.save()
        messages.success(self.request, _("We changed your data successfully!"))
        return response


class EditPasswordView(LoginRequiredMixin, views.PasswordChangeView):
    form_class = PasswordChangeForm
    template_name = 'user_profile/edit_user_password.html'
    success_url = reverse_lazy('user_profile:profile')

    def form_valid(self, form):
        response = super(EditPasswordView, self).form_valid(form)
        messages.success(self.request, _("We changed your data successfully!"))
        return response


class EditEmailView(LoginRequiredMixin, FormView):
    model = User
    form_class = EditEmailForm
    template_name = 'user_profile/edit_user_email.html'
    success_url = reverse_lazy('user_profile:profile')

    def get_initial(self):
        initial = super(FormView, self).get_initial()
        user = self.request.user
        initial['email'] = user.email
        return initial

    def form_valid(self, form):
        response = super(EditEmailView, self).form_valid(form)
        email = form.cleaned_data['email']
        user = self.request.user
        token = str(uuid.uuid4())
        user.verifyEmailToken = token
        user.verifyEmailTokenExpireDate = timezone.now() + timedelta(
            hours=2)
        user.save()
        email_verification_link = self.request.build_absolute_uri(
            reverse_lazy('user_profile:edit_email_verification')
            + '?token=' + token + '&email=' + email)

        send_email_async(email_verification_link, [email])
        messages.success(self.request, _("We sent you an E-mail. Please verify your E-mail!"))
        return response


@shared_task()
def send_email_async(email_verification_link, adress):
        send_mail(
            _('Verify Account'),
            email_verification_link,
            'admin@authentication.com',
            adress,
            fail_silently=False,
        )


class EmailChangeDoneView(View):
    def get(self, request):
        if not request.GET.get('token') is None:
            user = User.get_user_by_token(request.GET.get('token'))
            email = request.GET.get('email')
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            user.verifyEmailToken = None
            user.email = email
            user.save()
            messages.add_message(request, messages.SUCCESS,
                                 _('E-mail was activated successfully!'))
            return redirect(reverse_lazy('user_profile:profile'))
        return redirect(reverse_lazy('home'))


class EditBiographyView(LoginRequiredMixin, CreateView):
    form_class = EditBiographyForm
    template_name = 'user_profile/edit_user_biography.html'
    success_url = reverse_lazy('user_profile:edit_biography')
    slug = ''

    def get_context_data(self, **kwargs):
        context = super(EditBiographyView, self).get_context_data(**kwargs)
        user = self.request.user
        context['user'] = user
        context['events'] = user.events.all()
        return context

    def get(self, request):
        if "event" in request.GET:
            slug_tmp = request.GET["event"]
            user = self.request.user
            event_obj = Event.objects.filter(slug=slug_tmp).first()
            if user == event_obj.user:
                self.slug = slug_tmp
        return super().get(request)

    def post(self, request):
        if "event" in request.GET:
            slug_tmp = request.GET["event"]
            user = self.request.user
            event_obj = Event.objects.filter(slug=slug_tmp).first()
            if user == event_obj.user:
                self.slug = slug_tmp
        return super().post(request)

    def get_initial(self):
        initial = super(EditBiographyView, self).get_initial()
        if self.slug != "":
            old = Event.objects.filter(slug=self.slug).first()
            initial['start_date'] = old.start_date
            initial['end_date'] = old.end_date
            initial['institution'] = old.institution
            initial['position'] = old.position
        return initial

    def form_valid(self, form):
        self.object = form.save(False)
        if self.object.start_date > self.object.end_date:
            messages.error(self.request, _("Your dates are incorrect!"))
            if self.slug:
                return HttpResponseRedirect(self.success_url + "?event=" + self.slug)
            else:
                return HttpResponseRedirect(self.success_url)

        if self.slug != "":
            old = Event.objects.filter(slug=self.slug).first()
            old.delete()

        user = self.request.user
        self.object.user = user
        self.object.save()

        messages.success(self.request, _("We saved your event successfully!"))
        return HttpResponseRedirect(self.success_url)


class EditSpecialtiesView(LoginRequiredMixin, CreateView):
    form_class = EditSpecialtyForm
    template_name = 'user_profile/edit_user_specialties.html'
    success_url = reverse_lazy('user_profile:edit_specialties')
    slug = ''

    def get_context_data(self, **kwargs):
        context = super(EditSpecialtiesView, self).get_context_data(**kwargs)
        user = self.request.user
        context['user'] = user
        context['specialties'] = user.specialties.order_by('-knowledge_level').all()
        return context

    def get(self, request):
        if "specialty" in request.GET:
            slug_tmp = request.GET["specialty"]
            user = self.request.user
            specialty_obj = Specialty.objects.filter(slug=slug_tmp).first()
            if user == specialty_obj.user:
                self.slug = slug_tmp
        return super().get(request)

    def post(self, request):
        if "specialty" in request.GET:
            slug_tmp = request.GET["specialty"]
            user = self.request.user
            specialty_obj = Specialty.objects.filter(slug=slug_tmp).first()
            if user == specialty_obj.user:
                self.slug = slug_tmp
        return super().post(request)

    def get_initial(self):
        initial = super(EditSpecialtiesView, self).get_initial()
        if self.slug != "":
            old = Specialty.objects.filter(slug=self.slug).first()
            initial['title'] = old.title
            initial['knowledge_level'] = old.knowledge_level
        return initial

    def form_valid(self, form):
        self.object = form.save(False)
        user = self.request.user
        self.object.user = user
        self.object.save()

        if self.slug != "":
            old = Specialty.objects.filter(slug=self.slug).first()
            old.delete()

        messages.success(self.request, _("We saved your specialty successfully!"))
        return HttpResponseRedirect(self.success_url)


class DeleteSpecialtiesView(View):
    def get(self, request):
        if not request.GET.get('specialty') is None:
            specialty_slug = request.GET.get('specialty')
            specialty_obj = Specialty.objects.filter(slug=specialty_slug).first()
            user = self.request.user
            if user == specialty_obj.user:
                specialty_obj.delete()
            messages.success(request, _('Deleted specialty successfully!'))
            return redirect(reverse_lazy('user_profile:edit_specialties'))
        return redirect(reverse_lazy('home'))


class DeleteBiographyView(View):
    def get(self, request):
        if not request.GET.get('event') is None:
            event_slug = request.GET.get('event')
            event_obj = Event.objects.filter(slug=event_slug).first()
            user = self.request.user
            if user == event_obj.user:
                event_obj.delete()
            messages.success(request, _('Deleted event successfully!'))
            return redirect(reverse_lazy('user_profile:edit_biography'))
        return redirect(reverse_lazy('home'))
