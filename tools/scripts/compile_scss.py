"""
Script to compile all .scss files in the folder src/static/scss/ to .css files in src/static/css/
"""
import sass

print("Start compiling")
print("Compiling...")
sass.compile(dirname=('src/static/scss/', 'src/static/css/'))
print("Finish compiling")
